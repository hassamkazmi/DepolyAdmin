import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { STATUSES } from "../../redux/getReducer/getHorseSlice";
import { MdDelete } from "react-icons/md";
import { BiEdit } from "react-icons/bi";
import ScrollContainer from "react-indiana-drag-scroll";
import swal from "sweetalert";
import Moment from "react-moment";
import { fetchTrainer } from "../../redux/getReducer/getTrainerSlice";
import { fetchHorseKind } from "../../redux/getReducer/getHorseKind";
import { Modal } from "react-bootstrap";
import HorsePopup from "../../Components/Popup/HorsePopup";
import Lottie from "lottie-react";
import HorseAnimation from "../../assets/horselottie.json";
import axios from "axios";
import { BsEyeFill } from "react-icons/bs";
import Pagination from "./Pagination";
import { BiFilter } from "react-icons/bi";
import { fetchcolor } from "../../redux/getReducer/getColor";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { fetchBreederList } from "../../redux/getDropDownReducer/getBreederList";
import CSVHorse from "../../Components/CSVUploadPopup/HorsePopup";
import Select from "react-select";
import { fetchNationalityList } from "../../redux/getDropDownReducer/getNationalityList";
import { fetchOwner } from "../../redux/getReducer/getOwnerSlice";
import DefaulImg from "../../assets/default.jpg";
import Notfound from "../../Notfound";

const Horse = () => {
  //--------------------------useState----------------------//

  const [ShowCalender, setShowCalender] = useState(false);
  const [SearchAge, setSearchAge] = useState("");
  const [SearchCode, setSearchCode] = useState("");
  const [NationalityId, setNationalityId] = useState();
  const [SearchTitle, setSearchTitle] = useState("");
  const [TotalCount, setTotalCount] = useState();
  const [SearchKindHorse, setKindHorse] = useState();
  const [SearchBreeder, setBreeder] = useState();
  const [SearchActiveTrainer, setActiveTrainer] = useState();
  const [SearchData, setSearchData] = useState([]);
  const [SearchColorID, setColor] = useState("");
  const [loading, setLoading] = useState(false);
  const [SearchActiveOwner, setActiveOwner] = useState();

  //--------------------------modal ----------------------//

  const [show, setShow] = useState(false);
  const [modaldata, setmodaldata] = useState();
  const handleClose = () => setShow(false);
  const handleShow = async (data) => {
    setmodaldata(data);
    await setShow(true);
  };
  //--------------------------import csv----------------------//

  const [showCSV, setShowCSV] = useState(false);
  const [modaldataCSV, setmodaldataCSV] = useState();
  const handleCloseCSV = () => setShowCSV(false);
  const handleShowCSV = async (data) => {
    setmodaldataCSV(data);
    await setShowCSV(true);
  };

  const dispatch = useDispatch();
  const history = useNavigate();
  //--------------------------get data from redux----------------------//

  const { status } = useSelector((state) => state.horse);
  const { data: NationalityList } = useSelector(
    (state) => state.NationalityList
  );

  const { data: color } = useSelector((state) => state.color);
  const { data: BreederList } = useSelector((state) => state.BreederList);
  const { data: HorseKind } = useSelector((state) => state.HorseKind);
  const { data: owner } = useSelector((state) => state.owner);
  const { data: trainer } = useSelector((state) => state.trainer);

  //--------------------------data for dropdown----------------------//

  let AllNationality =
    NationalityList === undefined ? (
      <></>
    ) : (
      NationalityList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );

  let AllColor =
    color === undefined ? (
      <></>
    ) : (
      color.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );
  let AllBreeder =
    BreederList === undefined ? (
      <></>
    ) : (
      BreederList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );
  let horsekindoptions =
    HorseKind === undefined ? (
      <></>
    ) : (
      HorseKind.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );
  let owneroption =
    owner === undefined ? (
      <></>
    ) : (
      owner.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );
  let traineroption =
    trainer === undefined ? (
      <></>
    ) : (
      trainer.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );
  const [currentPage, setCurrentPage] = useState(1);
  const [TotalPages, setTotalPages] = useState("");

  const [postsPerPage] = useState(8);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const GetSearch = async () => {
    setLoading(true);
    const response = await axios.get(
      `${window.env.API_URL}SearchHorse?Foal=${SearchCode}&NameEn=${SearchTitle}&RemarksEn=${SearchAge}&NationalityID=${data}&ColorID=${Color}&Breeder=${Breeder}&KindHorse=${Kind}&ActiveOwner=${owners}&ActiveTrainer=${Trainers}&page=${currentPage}`
    );
    setSearchData(response.data.data);
    setLoading(false);
    const totalcount = response.data.totalcount;
    const TotalPagesData = response.data.totalPages;
    setTotalCount(totalcount);
    setTotalPages(TotalPagesData);
  };
  //--------------------------variiable for searching----------------------//

  const data = NationalityId ? NationalityId.id : "";
  const Color = SearchColorID ? SearchColorID.id : "";
  const Breeder = SearchBreeder ? SearchBreeder.id : "";
  const Kind = SearchKindHorse ? SearchKindHorse.id : "";
  const owners = SearchActiveOwner ? SearchActiveOwner.id : "";
  const Trainers = SearchActiveTrainer ? SearchActiveTrainer.id : "";

  useEffect(() => {
    dispatch(fetchcolor({ SearchTitle, SearchCode, SearchAge }));
    dispatch(fetchBreederList());
    dispatch(fetchNationalityList());
    dispatch(fetchOwner({ SearchTitle, SearchCode, SearchAge }));
    dispatch(fetchTrainer({ SearchTitle, SearchCode, SearchAge }));
    dispatch(fetchHorseKind({ SearchTitle, SearchCode, SearchAge }));
  }, [SearchAge, SearchCode, SearchTitle, dispatch]);

  useEffect(() => {
    GetSearch();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, currentPage]);
  //--------------------------delete----------------------//

  const handleRemove = async (Id) => {
    try {
      swal({
        title: "Are you sure?",
        text: "do you want to delete this data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then(async (willDelete) => {
        if (willDelete) {
          await axios.delete(`${window.env.API_URL}/softdeletehorse/${Id}`);
          swal("Your data has been deleted Successfully!", {
            icon: "success",
          });
          GetSearch();
        } else {
          swal("Your data is safe!");
        }
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

  if (status === STATUSES.ERROR) {
    return (
      <h2
        style={{
          margin: "100px",
        }}
      >
        Something went wrong!
      </h2>
    );
  }

  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Horse Listings</h4>

              <div>
                <Link to="/horseform">
                  <button>Add Horse</button>
                </Link>
                <OverlayTrigger
                  overlay={<Tooltip id={`tooltip-top`}>Filter</Tooltip>}
                >
                  <span className="addmore">
                    <BiFilter
                      className="calendericon"
                      onClick={() => setShowCalender(!ShowCalender)}
                    />
                  </span>
                </OverlayTrigger>

                <p onClick={() => handleShowCSV()} className="importcsv">
                  Import JSON
                </p>
              </div>
            </div>
            <div>
              {ShowCalender ? (
                <>
                  <div className="userfilter">
                    <div className="filtertextform d-block">
                      <input
                        type="text"
                        className="form-control"
                        onChange={(e) => setSearchTitle(e.target.value)}
                        placeholder="Enter Name"
                      />

                      <input
                        type="text"
                        className="form-control"
                        onChange={(e) => setSearchAge(e.target.value)}
                        placeholder="Enter Remarks"
                      />
                      <input
                        onKeyPress={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        className="form-control"
                        onChange={(e) => setSearchCode(e.target.value)}
                        placeholder="Enter Foal"
                      />
                    </div>
                    <div className="filtertextform">
                      <div className="searchDropdown">
                        <Select
                          placeholder="Breeder"
                          defaultValue={SearchBreeder}
                          onChange={setBreeder}
                          options={AllBreeder}
                          isClearable={true}
                          isSearchable={true}
                        />
                      </div>
                      <div className="searchDropdown">
                        <Select
                          placeholder="Nationality"
                          defaultValue={NationalityId}
                          onChange={setNationalityId}
                          options={AllNationality}
                          isClearable={true}
                          isSearchable={true}
                        />
                      </div>
                      <div className="searchDropdown">
                        <Select
                          placeholder="Color"
                          defaultValue={SearchColorID}
                          onChange={setColor}
                          options={AllColor}
                          isClearable={true}
                          isSearchable={true}
                        />
                      </div>
                    </div>

                    <div className="filtertextform  ">
                      <div className="searchDropdown">
                        <Select
                          placeholder=" Horse Kind"
                          defaultValue={SearchKindHorse}
                          onChange={setKindHorse}
                          options={horsekindoptions}
                          isClearable={true}
                          isSearchable={true}
                        />
                      </div>
                      <div className="searchDropdown">
                        <Select
                          placeholder=" Owner"
                          defaultValue={SearchActiveOwner}
                          onChange={setActiveOwner}
                          options={owneroption}
                          isClearable={true}
                          isSearchable={true}
                        />
                      </div>
                      <div className="searchDropdown">
                        <Select
                          placeholder=" Trainer"
                          defaultValue={SearchActiveTrainer}
                          onChange={setActiveTrainer}
                          options={traineroption}
                          isClearable={true}
                          isSearchable={true}
                        />
                      </div>
                    </div>
                  </div>
                  <button className="filterbtn" onClick={GetSearch}>
                    Apply Filter
                  </button>
                </>
              ) : (
                <></>
              )}
            </div>
            <>
              <div className="div_maintb">
                <ScrollContainer className="scroll-container">
                  <table id="customers">
                    <thead>
                      <tr>
                        <th>Actions</th>
                        <th>Name</th>
                        <th>Name Arabic</th>
                        <th>Age</th>
                        <th>Sex</th>
                        <th>Color</th>
                        <th>Purchase Price</th>
                        <th>Breeder</th>

                        <th>Remarks</th>
                        <th>Remarks Arabic</th>

                        <th>Rds</th>
                        <th>Foal</th>
                        <th>Purchase Price</th>
                        {/* <th>Stars</th> */}
                        <th>Horse Status</th>
                        {/* <th>Height</th> */}
                        <th>Kind Of Horse</th>
                        <th>Nationality</th>
                        {/* <th>Created in</th> */}
                        <th>Active Trainer</th>
                        <th>Owner</th>
                        <th>Dam </th>
                        <th>Sire </th>
                        <th>G sire</th>
                        <th>Short Code</th>

                        <th>Image</th>
                      </tr>
                    </thead>

                    {loading ? (
                      <Lottie
                        animationData={HorseAnimation}
                        loop={true}
                        className="TableLottie"
                      />
                    ) : !SearchData ? (
                      <Notfound />
                    ) : (
                      SearchData.map((item) => {
                        return (
                          <>
                            <tbody>
                              <tr className="tr_table_class">
                                <td
                                  className="table_delete_btn1"
                                  // style={{ textAlign: "center" }}
                                >
                                  <BiEdit
                                    onClick={() =>
                                      history("/edithorse", {
                                        state: {
                                          horseid: item,
                                        },
                                      })
                                    }
                                  />
                                  <MdDelete
                                    style={{
                                      fontSize: "22px",
                                    }}
                                    onClick={() => handleRemove(item._id)}
                                  />
                                  <BsEyeFill onClick={() => handleShow(item)} />
                                </td>
                                <td>{item.NameEn}</td>
                                <td>{item.NameAr}</td>
                                <td>
                                  <Moment fromNow ago>
                                    {item.DOB}
                                  </Moment>
                                </td>

                                <td>
                                  {item.SexModelData === null ? (
                                    <>N/A</>
                                  ) : (
                                    <>{item.SexModelData.NameEn}</>
                                  )}
                                </td>

                                <td>
                                  {item.ColorIDData === null ? (
                                    <>N/A</>
                                  ) : (
                                    item.ColorIDData.NameEn
                                  )}{" "}
                                </td>

                                <td>{item.PurchasePrice}</td>
                                <td>
                                  {item.BreederData === null ? (
                                    <>No Data</>
                                  ) : (
                                    <>{item.BreederData.NameEn}</>
                                  )}
                                </td>
                                <td className="cell">
                                  {item.RemarksEn ? item.RemarksEn : <> - </>}
                                </td>
                                <td className="cell">
                                  {item.RemarksAr ? item.RemarksAr : <> - </>}
                                </td>

                                <td>
                                  {item.Rds === true ? <>Yes</> : <>No</>}
                                </td>
                                <td>{item.Foal}</td>
                                <td>{item.PurchasePrice}</td>

                                <td>
                                  {item.HorseStatus === true ? (
                                    <>running</>
                                  ) : (
                                    <>not running</>
                                  )}
                                </td>

                                <td>
                                  {" "}
                                  {item.KindHorseData ? (
                                    item.KindHorseData.NameEn
                                  ) : (
                                    <>-</>
                                  )}
                                </td>
                                <td>
                                  {item.NationalityData ? (
                                    item.NationalityData.NameEn
                                  ) : (
                                    <>-</>
                                  )}
                                </td>

                                <td>
                                  {item.ActiveTrainerData ? (
                                    item.ActiveTrainerData.NameEn
                                  ) : (
                                    <>-</>
                                  )}
                                </td>
                                <td>
                                  {item.ActiveOwnerData ? (
                                    item.ActiveOwnerData.NameEn
                                  ) : (
                                    <>-</>
                                  )}
                                </td>
                                <td>
                                  {item.DamData ? item.DamData.NameEn : <>-</>}
                                </td>
                                <td>
                                  {item.SireData ? (
                                    item.SireData.NameEn
                                  ) : (
                                    <>-</>
                                  )}
                                </td>
                                <td>
                                  {item.GSireData ? (
                                    item.GSireData.NameEn
                                  ) : (
                                    <>-</>
                                  )}
                                </td>
                                <td>
                                  {item.shortCode ? item.shortCode : <>-</>}
                                </td>

                                <td>
                                  <img
                                    src={
                                      item.HorseImage
                                        ? item.HorseImage
                                        : DefaulImg
                                    }
                                    alt=""
                                    style={{
                                      width: "30px",
                                      height: "30px",
                                    }}
                                  ></img>
                                </td>
                              </tr>
                            </tbody>
                          </>
                        );
                      })
                    )}
                  </table>
                </ScrollContainer>
              </div>
            </>
          </div>
          <Pagination
            postsPerPage={postsPerPage}
            totalPosts={TotalCount}
            paginate={paginate}
            currentPage={currentPage}
            TotalPages={TotalPages}
          />
        </div>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Horse </h2>
        </Modal.Header>
        <Modal.Body>
          <HorsePopup data={modaldata} />
        </Modal.Body>

        <Modal.Footer>
          <button onClick={handleClose} className="modalClosebtn">
            Close
          </button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showCSV}
        onHide={handleCloseCSV}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2 style={{ fontFamily: "inter" }}>Horse JSON</h2>
        </Modal.Header>
        <Modal.Body>
          <CSVHorse data={modaldataCSV} />
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
};
export default Horse;
