import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { STATUSES } from "../../redux/getReducer/getOwnerSlice";
import OwnerPopup from "../../Components/Popup/OwnerPopup";
import { Modal } from "react-bootstrap";
import { MdDelete } from "react-icons/md";
import ScrollContainer from "react-indiana-drag-scroll";
import Moment from "react-moment";
import { BiEdit } from "react-icons/bi";
import Lottie from "lottie-react";
import HorseAnimation from "../../assets/horselottie.json";
import axios from "axios";
import swal from "sweetalert";
import { BsEyeFill } from "react-icons/bs";
import { fetchNationalityList } from "../../redux/getDropDownReducer/getNationalityList";
import Pagination from "./Pagination";
import { BiFilter } from "react-icons/bi";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import OwnerUploadPopup from "../../Components/CSVUploadPopup/OwnerPopup";
import Defaultimg from "../../assets/default.jpg";
import Select from "react-select";
import Notfound from "../../Notfound";

const Owner = () => {
  //---------------------------state-------------------------//
  const [ShowCalender, setShowCalender] = useState(false);

  const [SearchCode, setSearchCode] = useState("");
  const [SearchTitle, setSearchTitle] = useState("");
  const [SearchData, setSearchData] = useState([]);
  const [TotalCount, setTotalCount] = useState();
  const [SearchNationalityId, setNationalityId] = useState();
  const [TotalPages, setTotalPages] = useState("");

  const navigate = useNavigate();
  const [show, setShow] = useState(false);
  const [modaldata, setmodaldata] = useState();
  const handleClose = () => setShow(false);
  const handleShow = async (data) => {
    setmodaldata(data);
    await setShow(true);
  };
  //---------------------------modal-------------------------//

  const [showCSV, setShowCSV] = useState(false);
  const [modaldataCSV, setmodaldataCSV] = useState();
  const handleCloseCSV = () => setShowCSV(false);
  const handleShowCSV = async (data) => {
    setmodaldataCSV(data);
    await setShowCSV(true);
  };

  const dispatch = useDispatch();

  const { status } = useSelector((state) => state.owner);
  const { data: NationalityList } = useSelector(
    (state) => state.NationalityList
  );

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(8);
  const [loading, setLoading] = useState(false);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const Nation = SearchNationalityId ? SearchNationalityId.id : "";
  //---------------------------search-------------------------//

  const GetSearch = async () => {
    setLoading(true);
    const response = await axios.get(
      `${window.env.API_URL}SearchOwner?ShortEn=${SearchCode}&NameEn=${SearchTitle}&NationalityID=${Nation}&page=${currentPage}`
    );
    setSearchData(response.data.data);
    setLoading(false);
    const totalcount = response.data.totalcount;
    setTotalCount(totalcount);
    const TotalPagesData = response.data.totalPages;
    setTotalPages(TotalPagesData);
  };

  useEffect(() => {
    GetSearch();
    dispatch(fetchNationalityList());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, currentPage]);
  //---------------------------dropdown options-------------------------//

  let AllNationality =
    NationalityList === undefined ? (
      <></>
    ) : (
      NationalityList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
            </div>
          ),
        };
      })
    );
  //---------------------------delete-------------------------//

  const handleRemove = async (Id) => {
    try {
      swal({
        title: "Are you sure?",
        text: "do you want to delete this data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then(async (willDelete) => {
        if (willDelete) {
          await axios.delete(`${window.env.API_URL}/softdeleteowner/${Id}`);
          swal("Your data has been deleted Successfully!", {
            icon: "success",
          });
          GetSearch();
        } else {
          swal("Your data is safe!");
        }
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

  if (status === STATUSES.ERROR) {
    return <div>Something went wrong</div>;
  }

  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Owner Listings</h4>

              <div>
                <Link to="/ownerform">
                  <button>Add Owner</button>
                </Link>
                <OverlayTrigger
                  overlay={<Tooltip id={`tooltip-top`}>Filter</Tooltip>}
                >
                  <span className="addmore">
                    <BiFilter
                      className="calendericon"
                      onClick={() => setShowCalender(!ShowCalender)}
                    />
                  </span>
                </OverlayTrigger>
                <p onClick={() => handleShowCSV()} className="importcsv">
                  Import JSON
                </p>
                {/* <CSVLink
                  data={owner}
                  separator={";"}
                  filename={"MKS Owner.csv"}
                  className="csvclass"
                >
                  Export CSV
                </CSVLink> */}
              </div>
            </div>
            <div>
              {ShowCalender ? (
                <>
                  <div className="userfilter">
                    <div className="filtertextform d-flex">
                      <div className="searchDropdown searchDropdown1">
                        <Select
                          placeholder="Nationality"
                          className="mt--5 h-3 "
                          defaultValue={SearchNationalityId}
                          onChange={setNationalityId}
                          isSearchable={true}
                          options={AllNationality}
                          isClearable={true}
                        />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        onChange={(e) => setSearchTitle(e.target.value)}
                        placeholder="Enter Name"
                      />
                      <input
                        type="text"
                        className="form-control"
                        onChange={(e) => setSearchCode(e.target.value)}
                        placeholder="Enter Short Name"
                      />
                    </div>
                  </div>
                  <button className="filterbtn" onClick={GetSearch}>
                    Apply Filter
                  </button>
                </>
              ) : (
                <></>
              )}
            </div>
            <>
              <div className="div_maintb">
                <ScrollContainer className="scroll-container">
                  <table>
                    <thead>
                      <tr>
                        <th style={{ textAlign: "center" }}>Action</th>
                        <th>Owner Name</th>
                        <th>Owner Name Arabic</th>
                        <th>Title</th>
                        <th>Title Arabic</th>
                        <th>Short Name</th>
                        <th>Short Name Arabic</th>
                        <th>Registration Date</th>
                        <th>Nationality</th>
                        {/* <th>Silk Color</th> */}
                        <th>Image</th>
                      </tr>
                    </thead>
                    {loading ? (
                      <Lottie
                        animationData={HorseAnimation}
                        loop={true}
                        className="TableLottie"
                      />
                    ) : SearchData.length === 0 ? (
                      <Notfound />
                    ) : (
                      <tbody>
                        {SearchData.map((item, index) => {
                          return (
                            <>
                              <tr className="tr_table_class">
                                <td
                                  className="table_delete_btn1"
                                // style={{ textAlign: "center" }}
                                >
                                  <BiEdit
                                    onClick={() =>
                                      navigate("/editowner", {
                                        state: {
                                          ownerid: item,
                                        },
                                      })
                                    }
                                  />
                                  <MdDelete
                                    onClick={() => handleRemove(item._id)}
                                  />
                                  <BsEyeFill onClick={() => handleShow(item)} />
                                </td>
                                <td>{item.NameEn}</td>
                                <td>{item.NameAr}</td>
                                <td>{item.TitleEn}</td>
                                <td>
                                  {item.TitleAr === "" ? (
                                    <>N/A</>
                                  ) : (
                                    item.TitleAr
                                  )}{" "}
                                </td>
                                <td>{item.ShortEn}</td>
                                <td>
                                  {item.ShortAr === "" ? (
                                    <>N/A</>
                                  ) : (
                                    item.ShortAr
                                  )}{" "}
                                </td>
                                <td>
                                  {" "}
                                  <Moment format="YYYY/MM/DD">
                                    {item.RegistrationDate}
                                  </Moment>
                                </td>

                                <td>
                                  {item.OwnerDataNationalityData === null ? (
                                    <>N/A</>
                                  ) : (
                                    item.OwnerDataNationalityData.NameEn
                                  )}
                                </td>
                                {/* <td>
                          <img src={item.OwnerIDData === undefined ? <></> : item.OwnerIDData.OwnerSilkColor} alt='' />
                         </td> */}
                                <td className="imageRow">
                                  <img
                                    src={item.image ? item.image : Defaultimg}
                                    alt=""
                                  />
                                </td>
                              </tr>
                            </>
                          );
                        })}
                      </tbody>
                    )}
                  </table>
                </ScrollContainer>
              </div>
            </>
          </div>
          <Pagination
            postsPerPage={postsPerPage}
            totalPosts={TotalCount}
            paginate={paginate}
            currentPage={currentPage}
            TotalPages={TotalPages}
          />
        </div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Owner </h2>
        </Modal.Header>
        <Modal.Body>
          <OwnerPopup data={modaldata} />
        </Modal.Body>
        <Modal.Footer>
          <button onClick={handleClose} className="modalClosebtn">
            Close
          </button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showCSV}
        onHide={handleCloseCSV}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2 style={{ fontFamily: "inter" }}>Owner JSON</h2>
        </Modal.Header>
        <Modal.Body>
          <OwnerUploadPopup data={modaldataCSV} />
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
};

export default Owner;
