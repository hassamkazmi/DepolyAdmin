import React, { useEffect, Fragment, useState } from "react";
import { fetchpointTable, STATUSES } from "../../redux/getReducer/getPointTable";
import { useDispatch, useSelector } from "react-redux";
import { MdDelete } from "react-icons/md";
import { Link, useNavigate } from "react-router-dom";
import swal from "sweetalert";
import ScrollContainer from "react-indiana-drag-scroll";
import Lottie from "lottie-react";
import HorseAnimation from "../../assets/horselottie.json";
import axios from "axios";
import { BiEdit } from "react-icons/bi";
import { Modal } from "react-bootstrap";
import PointTablePopup from "../../Components/Popup/PointTablePopup";
import { BsEyeFill } from "react-icons/bs"
import Pagination from "./Pagination";
import { BiFilter } from 'react-icons/bi';
import { CSVLink } from "react-csv";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import Notfound from "../../Notfound";




const BonusPoint = (data) => {
  return (
    <>
      <table className="Prizeclass">
        <thead className="Prizeclassthead">
          <tr>
            <th>Bonus Point</th>
            <th>Point </th>
            <th>Rank </th>
            <th>Type </th>
            <th>shortCode </th>
          </tr>
        </thead>
        <tbody className="Prizeclasstbody">
          {
            data.data.PointSystemidDataOfCompetition ? data.data.PointSystemidDataOfCompetition.map((item) => {
              return (
                <tr>
                  <td>{item.BonusPoint}</td>
                  <td>{item.Point}</td>
                  <td>{item.Rank}</td>
                  <td>{item.Type}</td>
                  <td>{item.shortCode}</td>
                </tr>
              )
            }) : <></>
          }
          <tr>
            {/* <td>{data.data.FirstPrice}</td>
            <td>{data.data.SecondPrice}</td>
            <td>{data.data.ThirdPrice}</td>
            <td>{data.data.FourthPrice}</td>
            <td>{data.data.FifthPrice}</td>
            <td>{data.data.SixthPrice}</td> */}
          </tr>
        </tbody>
      </table>
    </>
  );
};

const Pointstable = () => {
  //---------------------------state-------------------------//


  const [ShowCalender, setShowCalender] = useState(false)
  const [SearchAge, setSearchAge] = useState('');
  const [SearchCode, setSearchCode] = useState('');
  const [SearchTitle, setSearchTitle] = useState('');
  const [TotalCount, setTotalCount] = useState()
  const [TotalPages, setTotalPages] = useState('');
  //---------------------------modal-------------------------//

  const [modaldata, setmodaldata] = useState();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = async (data) => {
    setmodaldata(data);
    await setShow(true);
  };






  const dispatch = useDispatch();
  const history = useNavigate();
  const { data: pointTable, status } = useSelector((state) => state.pointTable);

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(8)


  const paginate = pageNumber => setCurrentPage(pageNumber);

  //---------------------------api-------------------------//

  const Data1 = async () => {
    const res = await axios.get(`${window.env.API_URL}PointTableSystemget?Group_Name=${SearchTitle}&Rank=${SearchAge}&page=${currentPage}`);
    const totalcount = res.data.totalcount;
    setTotalCount(totalcount);
    const TotalPagesData = res.data.totalPages;
    setTotalPages(TotalPagesData)
  };
  //---------------------------search-------------------------//

  const GetSearch = async () => {
    dispatch(fetchpointTable({ SearchTitle, SearchCode, SearchAge, }));
  };
  useEffect(() => {
    dispatch(fetchpointTable({ SearchTitle, SearchCode, SearchAge, currentPage }));
    // Data1()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, currentPage]);
  //---------------------------delete-------------------------//

  const handleRemove = async (Id) => {
    try {
      await axios.delete(`${window.env.API_URL}/softdeletePointTableSystem/${Id}`);
      swal({
        title: "Success!",
        text: "Data has been Deleted successfully ",
        icon: "success",
        button: "OK",
      });
      history("/viewcompetitionPoint");
      dispatch(fetchpointTable({ SearchTitle, SearchCode, SearchAge }));
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
    history("/viewcompetitionPoint");
  };



  if (status === STATUSES.ERROR) {
    return (
      <h2
        style={{
          margin: "100px",
        }}
      >
        Something went wrong!
      </h2>
    );
  }

  return (
    <Fragment>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Point Table Listings</h4>

              <div>
                <h6
                  style={{
                    marginRight: "100px",
                    alignItems: "center",
                    color: "rgba(0, 0, 0, 0.6)",
                  }}
                >

                </h6>

                <Link to="/addcompetitionPoint">
                  <button>Add Point Table</button>
                </Link>
                <OverlayTrigger
                  overlay={<Tooltip id={`tooltip-top`}>Filter</Tooltip>}
                >
                  <span
                    className="addmore"
                  >
                    <BiFilter
                      className="calendericon"
                      onClick={() => setShowCalender(!ShowCalender)}
                    />
                  </span>
                </OverlayTrigger>
                <CSVLink data={pointTable} separator={";"} filename={"MKS Point Table.csv"} className='csvclass'>
                  Export CSV
                </CSVLink>
              </div>
            </div>
            <div>

              {
                ShowCalender ?
                  <span className="transitionclass">
                    <div className="userfilter">

                      <div className="filtertextform forflex">

                        <input
                          type="text"
                          className="form-control"
                          onChange={(e) => setSearchTitle(e.target.value)}
                          placeholder="Enter Group Name"
                        />
                        <input
                          onKeyPress={(event) => {
                            if (!/[0-9]/.test(event.key)) {
                              event.preventDefault();
                            }
                          }}
                          className="form-control"
                          onChange={(e) => setSearchAge(e.target.value)}
                          placeholder="Enter Rank"
                        />
                        <input
                          type="text"
                          className="form-control"
                          onChange={(e) => setSearchCode(e.target.value)}
                          placeholder="Enter Short Code"
                        />
                      </div>

                    </div>
                    <button className="filterbtn" onClick={GetSearch}>
                      Apply Filter
                    </button>
                  </span>
                  : <></>
              }
            </div>
            <>
              <div className="div_maintb">
                <ScrollContainer>
                  <table>
                    <thead>
                      <tr>
                        <th>Action</th>

                        <th>Point Group Name</th>

                        <th>Length</th>
                        <th>Point System </th>

                        <th>Short Code</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        !pointTable.length ? <Notfound /> :
                          status === STATUSES.LOADING ?

                            <Lottie animationData={HorseAnimation} loop={true} className="TableLottie" />

                            :
                            pointTable.map((item, index) => {
                              return (
                                <>
                                  <tr className="tr_table_class">
                                    <td className="table_delete_btn1">
                                      <BiEdit

                                        onClick={() =>
                                          history("/editpoint", {
                                            state: {
                                              pointid: item,
                                            },
                                          })
                                        }
                                      />
                                      <MdDelete
                                        style={{
                                          fontSize: "22px",
                                        }}
                                        onClick={() => handleRemove(item._id)}
                                      />
                                      <BsEyeFill
                                        onClick={() => handleShow(item)}
                                      />
                                    </td>
                                    <td>{item.Group_NameDataOfCompetition ? item.Group_NameDataOfCompetition.NameEn : <></>}</td>

                                    <td>{item.Length}</td>

                                    <td>
                                      <button
                                        className="Approvedbtn resultbtn"
                                        onClick={() => handleShow(item)}
                                      >
                                        Click
                                      </button>
                                    </td>

                                    <td>{item.shortCode} </td>


                                  </tr>
                                </>
                              );
                            })}
                    </tbody>
                  </table>
                </ScrollContainer>
              </div>
            </>
          </div>
          <span className="plusIconStyle"></span>
          <Pagination
            postsPerPage={postsPerPage}
            totalPosts={TotalCount}
            paginate={paginate}
            currentPage={currentPage}
            TotalPages={TotalPages}

          />
        </div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Point Table  </h2>
        </Modal.Header>
        <Modal.Body>
          <BonusPoint data={modaldata} />
        </Modal.Body>
        <Modal.Footer>
          <button onClick={handleClose} className="modalClosebtn">
            Close
          </button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

export default Pointstable;
