import React, { useEffect } from "react";
import "react-toastify/dist/ReactToastify.css";
import { fetchjockey } from "../../../redux/getReducer/getJockeySlice";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchHorseForRace } from "../../../redux/getDropDownReducer/getHorseRace";
import { fetchequipment } from "../../../redux/getReducer/getEquipment";
import { toast } from "react-toastify";
import Form from "react-bootstrap/Form";
import Select from "react-select";
import swal from "sweetalert";
import axios from "axios";
import { fetchcolor } from "../../../redux/getReducer/getColor";
import { ImCross } from "react-icons/im";
import { TiTick } from "react-icons/ti";

function FormData({ onValChange, formObject, onFormSubmit }) {

    const { data: jockey } = useSelector((state) => state.jockey);
  const { data: HorseForRace } = useSelector((state) => state.HorseForRace);
  const { data: equipment } = useSelector((state) => state.equipment);
  const { data: color } = useSelector((state) => state.color);

  const [HorseData, SetHorseData] = useState("");
  const [raceoriginal, setraceoriginal] = useState([]);
  const [Gate, setGate] = useState("");
  const [EquipmentData, SetEquipmentData] = useState("");
  const [JockeyData, SetJockeyData] = useState("");
  const [CapColor, SetCapColor] = useState("");
  const [HorseStatus, SetHorseStatus] = useState(true);
  const [items, setitems] = useState([]);
  const [SearchAge, setSearchAge] = useState("");
  const [SearchCode, setSearchCode] = useState("");
  const [SearchTitle, setSearchTitle] = useState("");
  const [SearchNameEn, setSearchNameEn] = useState("");
  const [SearchRating, setSearchRating] = useState("");

  let horseoptions = HorseForRace.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
      Ownername: item.ActiveOwnerData.NameEn,
      rating: item.STARS,
    };
  });
  const gateoption = [
    { id: "0", value: "1", label: "1" },
    { id: "1", value: "2", label: "2" },
    { id: "2", value: "3", label: "3" },
    { id: "3", value: "4", label: "4" },
    { id: "4", value: "5", label: "5" },
    { id: "5", value: "6", label: "6" },
    { id: "6", value: "7", label: "7" },
    { id: "7", value: "8", label: "8" },
    { id: "8", value: "9", label: "9" },
    { id: "9", value: "10", label: "10" },
  ];


    return (
      <div className="row mb-4">
        <div className="mb-3">
            <select  onChange={onValChange} name="HorseData">
                {
                    gateoption.map((item) => {
                        return(
                            <option value={formObject.label}>
                                {item.label}
                            </option>
                        )
                    })
                }
            </select>

        {/* <Select
                      id="Horseinput"
                      placeholder="Select Horse"
                      className="dropdown multidropdown"
                      onChange={onValChange}
                      options={gateoption}
                      isSearchable={true}
                      name="HorseData"
                      defaultValue={formObject.HorseData}

                    /> */}
          
        </div>
        <div className="mb-3">
          <input
            type="email"
            className="form-control"
            placeholder="Email"
            onChange={onValChange}
            value={formObject.email}
            name="email"
          />
        </div>
        <div className="mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Profile"
            onChange={onValChange}
            value={formObject.profile}
            name="profile"
          />
        </div>
        <div className="d-grid">
          <input
            type="submit"
            onClick={onFormSubmit}
            className="btn btn-success"
          />
        </div>
      </div>
    );
  }
  export default FormData;