import React, { useEffect } from "react";
import "react-toastify/dist/ReactToastify.css";
import { fetchjockey } from "../../../redux/getReducer/getJockeySlice";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchracecourse } from "../../../redux/getReducer/getRaceCourseSlice";

import { fetchMeeting } from "../../../redux/getReducer/getMeeting";
import { fetchRaceName } from "../../../redux/getReducer/getRaceName";
import { fetchHorseKind } from "../../../redux/getReducer/getHorseKind";
import { fetchTrackLength } from "../../../redux/getReducer/getTracklength";
import { fetchgroundtype } from "../../../redux/getReducer/getGroundType";
import { fetchpointTable } from "../../../redux/getReducer/getPointTable";
import { fetchTrackConditionDropdown } from "../../../redux/getDropDownReducer/getTrackConditionDropDown";
import Select from "react-select";
import swal from "sweetalert";
import "react-calendar/dist/Calendar.css";
import "react-clock/dist/Clock.css";
import axios from "axios";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import { AiOutlineReload } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import TextInputValidation from "../../../utils/TextInputValidation";
import Racename from "../Racenameform";
import MeetingTypePopUp from "../MeetingType";
import RaceTypePopup from "../Racetypeform";
import TrackLengthPopup from "../Tracklengthform";
import GroundTypePopup from "../GroundType";
import RaceKindPopup from "../RaceKind";
import RaceCoursePopup from "../RaceCourseForm";
import HorseKindPopup from "../Horsekindform";
import JockeyPopup from "../JockeyForm";
import SponsorPopup from "../SponsorForm";
import TrackConditionPopup from "../TrackCondition";
import { fetchcurrency } from "../../../redux/getReducer/getCurrency";
import CurrencyPopup from '../Currency';
import { fetchRaceKindDropdown } from "../../../redux/getDropDownReducer/getRaceKind";
import { fetchRaceTypeDropdown } from "../../../redux/getDropDownReducer/getRaceType";
import { fetchSponsorDropdown } from "../../../redux/getDropDownReducer/getSponsor";

const WeatherTypes = [
  {
    id: "1",
    value: "Sunny",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Sunny</p>
        <p>مشمس</p>
      </div>
    ),
  },
  {
    id: "2",
    value: "Cloudy",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Cloudy</p>
        <p>غائم</p>
      </div>
    ),
  },
];

// const ResultStatus = [
//   { id: "1", value: "Announced", label: "Announced" },
//   { id: "2", value: "Awaited", label: "Awaited" },
//   { id: "3", value: "Cancelled", label: "Cancelled" },

// ];

// const WeatherTypesAr = [
//   { id: "1", value: "مشمس", label: "مشمس" },
//   { id: "2", value: "غائم", label: "غائم" },
// ];

const RaceStatuss = [
  {
    id: "1",
    value: "Cancelled",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Cancel</p>
        <p>يلغي</p>
      </div>
    ),
  },
  {
    id: "2",
    value: "Due",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Due</p>
        <p>بسبب</p>
      </div>
    ),
  },
  {
    id: "2",
    value: "Live",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Live</p>
        <p>يعيش</p>
      </div>
    ),
  },
  {
    id: "2",
    value: "Completed",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>End</p>
        <p>نهاية</p>
      </div>
    ),
  },
];

// const RaceStatussAr = [
//   { id: "1", value: "يلغي", label: "يلغي" },
//   { id: "2", value: "بسبب", label: "بسبب" },
//   { id: "2", value: "يعيش", label: "يعيش" },
//   { id: "2", value: "نهاية", label: "نهاية" },
// ];

const RaceForm = () => {
  const [ErrorMeetingType, setErrorMeetingType] = useState("");
  const [ErrorRaceNameEn, setErrorRaceNameEn] = useState("");
  const [ErrorMeetingCode, setErrorMeetingCode] = useState("");


  const [ErrorRaceKind, setErrorRaceKind] = useState("");
  const [ErrorCondition, setErrorCondition] = useState("");
  const [ErrorDescriptionEn, setErrorDescriptionEn] = useState("");
  const [ErrorDescriptionAr, setErrorDescriptionAr] = useState("");

  const [ErrorWeatherType, setErrorWeatherType] = useState("");
  const [ErrorRaceStatus, setErrorRaceStatus] = useState("");
  const [ErrorRaceCourse, setErrorRaceCourse] = useState("");

  const [ErrorWeatherDegree, setErrorWeatherDegree] = useState("");
  const [ErrorHorseKind, setErrorHorseKind] = useState("");
  const [ErrorSponsor, setErrorSponsor] = useState("");
  const [ErrorTrackLength, setErrorTrackLength] = useState("");


  const [ErrorStartTime, setErrorStartTime] = useState("");
  const [ErrorEndTime, setErrorEndTime] = useState("");
  const [ErrorRaceTyp, setErrorRaceType] = useState("");

  const [ErrorFirstPrice, setErrorFirstPrice] = useState("");

  const [isLoading, setisLoading] = useState(false);

  const SearchTitle = "";
  const SearchCode = "";
  const SearchAge = "";
  //end
  const { data: racecourse } = useSelector((state) => state.racecourse);
  const { data: HorseKind } = useSelector((state) => state.HorseKind);
  const { data: sponsordropdown } = useSelector((state) => state.sponsordropdown);
  const { data: meeting } = useSelector((state) => state.meeting);
  const { data: racetypedropdown } = useSelector((state) => state.racetypedropdown);
  const { data: RaceName } = useSelector((state) => state.RaceName);
  const { data: trackLength } = useSelector((state) => state.trackLength);
  const { data: racekinddropdown } = useSelector((state) => state.racekinddropdown);
  const { data: TrackConditiondropdown } = useSelector((state) => state.TrackConditiondropdown);
  const { data: currency } = useSelector((state) => state.currency);


  const history = useNavigate();
  const dispatch = useDispatch();

  let trackconditionTable =
    TrackConditiondropdown === undefined ? (
      <></>
    ) : (
      TrackConditiondropdown.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );


  let currencyoption =
    currency === undefined ? (
      <></>
    ) : (
      currency.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let racecourses =
    racecourse === undefined ? (
      <></>
    ) : (
      racecourse.map(function (item) {
        return {
          id: item._id,
          value: item.TrackNameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.TrackNameEn}</p>
              <p>{item.TrackNameAr}</p>
            </div>
          ),
        };
      })
    );

  let horsekindoptions =
    HorseKind === undefined ? (
      <></>
    ) : (
      HorseKind.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );



  let Racenameoptions =
    RaceName === undefined ? (
      <></>
    ) : (
      RaceName.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  // let RacenameoptionsAr =
  //   RaceName === undefined ? (
  //     <></>
  //   ) : (
  //     RaceName.map(function (item) {
  //       return {
  //         id: item._id,
  //         value: item._id,
  //         label: item.NameAr,
  //       };
  //     })
  //   );



  let SponsorForTheRace =
    sponsordropdown === undefined ? (
      <></>
    ) : (
      sponsordropdown.map(function (item) {
        return {
          id: item._id,
          value: item.TitleEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <span
                style={{
                  display: "flex",
                }}
              >
                <img src={item.image} height="30px" width="30px" alt="" />
                <p>{item.TitleEn}</p>
              </span>
              <span
                style={{
                  display: "flex",
                }}
              >
                <img src={item.image} height="30px" width="30px" alt="" />
                <p>{item.TitleAr}</p>
              </span>
            </div>
          ),
        };
      })
    );

  // let SponsorForTheRaceAr =
  //   sponsor === undefined ? (
  //     <></>
  //   ) : (
  //     sponsor.map(function (item) {
  //       return {
  //         id: item._id,
  //         value: item.TitleAr,
  //         label: item.TitleAr,
  //       };
  //     })
  //   );

  let MeetingTypes =
    meeting === undefined ? (
      <></>
    ) : (
      meeting.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
    
  // let MeetingTypesAr =
  //   meeting === undefined ? (
  //     <></>
  //   ) : (
  //     meeting.map(function (item) {
  //       return {
  //         id: item._id,
  //         value: item._id,
  //         label: item.NameAr,
  //       };
  //     })
  //   );

  let RaceTypes =
    racetypedropdown === undefined ? (
      <></>
    ) : (
      racetypedropdown.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let TrackLenght =
    trackLength === undefined ? (
      <></>
    ) : (
      trackLength.map(function (item) {
        return {
          id: item._id,
          value: item._id,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: "5px"
              }}
            >
                <p>{item.TrackLength} ({item.GroundTypeModelData ? item.GroundTypeModelData.NameEn : <></>})</p>
              <p>{item.TrackLength} ({item.GroundTypeModelData ? item.GroundTypeModelData.NameAr : <></>})</p>
            </div>
          ),
        };
      })
    );

  let OptionRaceKind =
    racekinddropdown === undefined ? (
      <></>
    ) : (
      racekinddropdown.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );





  //  Modal functionalities Here
  const [showName, setShowName] = useState(false);
  const [showType, setShowType] = useState(false);
  const [showRaceType, setShowRaceType] = useState(false);
  const [showTrackLength, setShowTrackLength] = useState(false);
  const [showGroundType, setShowGroundType] = useState(false);
  const [showRaceKind, setShowRaceKind] = useState(false);
  const [showRaceCourse, setShowRaceCourse] = useState(false);
  const [showJockey, setShowJockey] = useState(false);
  const [showSponsor, setShowSponsor] = useState(false);
  const [showCondition, setshowCondition] = useState(false);
  const [showhorseKind, setshowhorseKind] = useState(false);
  const [showcurrency, setshowcurrency] = useState(false);

  const handleCloseName = () => setShowName(false);
  const handleCloseType = () => setShowType(false);
  const handleCloseRaceType = () => setShowRaceType(false);
  const handleCloseTrackLength = () => setShowTrackLength(false);
  const handleCloseGroundType = () => setShowGroundType(false);
  const handleCloseRaceKind = () => setShowRaceKind(false);
  const handleCloseRaceCourse = () => setShowRaceCourse(false);
  const handleCloseJockey = () => setShowJockey(false);
  const handleCloseSponsor = () => setShowSponsor(false);
  const handleCloseTrackCondition = () => setshowCondition(false);
  const handleCloseHorseKind = () => setshowhorseKind(false);
  const handleCloseCurrency = () => setshowcurrency(false);

  const handleShowName = async () => {
    await setShowName(true);
  };
  const handleShowHorseKind = async () => {
    await setshowhorseKind(true);
  };
  const handleShowType = async () => {
    await setShowType(true);
  };
  const handleShowTrackCondition = async () => {
    await setshowCondition(true);
  };

  const handleShowRaceType = async () => {
    await setShowRaceType(true);
  };
  const handleShowTrackLength = async () => {
    await setShowTrackLength(true);
  };



  const handleShowRaceKind = async () => {
    await setShowRaceKind(true);
  };

  const handleShowRaceCourse = async () => {
    await setShowRaceCourse(true);
  };



  const handleShowSponsor = async () => {
    await setShowSponsor(true);
  };

  const handleShowCurrency = async () => {
    await setshowcurrency(true);
  };

  // Modal functionalities End Here




  const FetchNew = () => {
    dispatch(fetchracecourse({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchjockey({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchSponsorDropdown());
    dispatch(fetchMeeting({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchRaceTypeDropdown());
    dispatch(fetchRaceName({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchTrackLength({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchRaceKindDropdown());
    dispatch(fetchgroundtype({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchpointTable({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchTrackConditionDropdown());
    dispatch(fetchHorseKind({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchcurrency({ SearchCode, SearchTitle }));

  };

  const [MeetingType, setMeetingType] = useState("");
  const [RaceNameEn, setRaceNameEn] = useState("");
  const [MeetingCode, setMeetingCode] = useState("");
  const [HorsesKind, sethorsesKind] = useState('');
  const [CurrencyData, setCurrencyData] = useState('');
  const [RaceWeight, setRaceWeight] = useState('0');


  const [RaceKind, setRaceKind] = useState("");
  const [DescriptionEn, setDescriptionEn] = useState("");
  const [DescriptionAr, setDescriptionAr] = useState("");
  const [WeatherType, setWeatherType] = useState("");
  const [RaceStatus, setRaceStatus] = useState("");
  const [RaceCourse, setRaceCourse] = useState("");

  const [Sponsor, setSponsor] = useState("");
  const [TrackLength, setTrackLength] = useState("");


  const [RaceTyp, setRaceType] = useState("");
  const [Day, setDay] = useState("");
  const [StartTime, setStartTime] = useState("");

  const [TrackCondition, setTrackCondition] = useState("");
  const [RaceNumber, setRaceNumber] = useState("");
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [TotalPrize, setTotalPrize] = useState("");
  const [FirstPrice, setFirstPrice] = useState("");
  const [SecondPrice, setSecondPrice] = useState("");
  const [ThirdPrice, setThirdPrice] = useState("");
  const [FourthPrice, setFourthPrice] = useState("");
  const [FifthPrice, setFifthPrice] = useState("");
  const [SixthPrice, setSixthPrice] = useState("");
  const [showCalculated, setshowCalculated] = useState(false);
  const [Dstate, setDstate] = useState({
    WeatherDegree: 24,
  });



  useEffect(() => {
    dispatch(fetchracecourse({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchjockey({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchSponsorDropdown());
    dispatch(fetchMeeting({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchRaceTypeDropdown());
    dispatch(fetchRaceName({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchTrackLength({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchRaceKindDropdown());
    dispatch(fetchgroundtype({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchpointTable({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchHorseKind({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchTrackConditionDropdown());
    dispatch(fetchcurrency({ SearchCode, SearchTitle }));


  }, [dispatch]);



  //   console.log(Day,"Day")

  //   var dateObj = Day === '' ? new Date() : Day
  // var month = dateObj.getUTCMonth() + 1;
  // var day = dateObj.getUTCDate();
  // var year = dateObj.getUTCFullYear();

  // console.log(dateObj,"dateObj")

  // console.log(day,'year')

  const submit = async (event) => {
    event.preventDefault();
    setisLoading(true);
    let Timing = new Date(`${Day} ${StartTime}`);
    let Endtiming = new Date(Timing.getTime() + 1000);

    let UTCTime = Timing.toISOString();
    let UTCDate = Endtiming.toISOString();
    try {
      const formData = new FormData();
      formData.append("RaceName", RaceNameEn.id);
      formData.append("MeetingType", MeetingType.id);
      formData.append("MeetingCode", MeetingCode);
      formData.append("RaceNumber", RaceNumber);
      formData.append("RaceType", RaceTyp.id);
      formData.append("HorseKindinRace", HorsesKind.id);
      formData.append("RaceKind", RaceKind.id);
      formData.append("DescriptionEn", DescriptionEn);
      formData.append("DescriptionAr", DescriptionAr);
      formData.append("Currency", CurrencyData.id);
      formData.append("RaceWeight", RaceWeight);
      formData.append("Day", Day);
      formData.append(
        "WeatherType",
        WeatherType.value === undefined ? "Sunny" : WeatherType.value
      );
      formData.append(
        "RaceStatus",
        RaceStatus.value === undefined ? "Live" : RaceStatus.value
      );
      formData.append("RaceCourse", RaceCourse.id);
      formData.append("WeatherIcon", "WeatherIcon");
      formData.append("StartTime", StartTime);
      formData.append("Timing", UTCTime);
      formData.append("Endtiming", UTCDate);

      formData.append("totalPrize", TotalPrize);
      formData.append("PrizeNumber", isSubscribed === false ? 5 : 6);
      formData.append("Sponsor", Sponsor.id);
      formData.append("WeatherDegree", Dstate.WeatherDegree);
      formData.append("TrackCondition", TrackCondition.id
      );
      formData.append("TrackLength", TrackLength.id);

      const response = await axios.post(
        `${window.env.API_URL}/createrace`,
        formData
      );
      setisLoading(false);
      swal({
        title: "Success",
        text: "Data has been added successfully ",
        icon: "success",
        button: "OK",
      });
      const RaceId = response.data.data._id;
      history("/publishrace", {
        state: {
          RaceId: RaceId,
        },
      });
      setisLoading(false);
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
      setisLoading(false);
    }
  };


  const data1 = JSON.stringify(
    TextInputValidation("en", DescriptionEn, "Race Description")
  );

  const obj = JSON.parse(data1);

  const data2 = JSON.stringify(
    TextInputValidation("ar", DescriptionAr, "Race Description Arabic")
  );

  const objAr = JSON.parse(data2);



  const handleChange = (event) => {
    if (event.target.checked) {
      console.log("✅ Checkbox is checked", isSubscribed);
    } else {
      console.log("⛔️ Checkbox is NOT checked", isSubscribed);
    }
    setIsSubscribed((current) => !current);
  };

  const calculatePrize = () => {
    setshowCalculated(true);
    if (!isSubscribed) {
      let data1 = 60 / 100;
      let data2 = 20 / 100;
      let data3 = 11 / 100;
      let data4 = 6 / 100;
      let data5 = 3 / 100;
      setFirstPrice(data1 * TotalPrize);
      setSecondPrice(data2 * TotalPrize);
      setThirdPrice(data3 * TotalPrize);
      setFourthPrice(data4 * TotalPrize);
      setSixthPrice("0");
      setFifthPrice(data5 * TotalPrize);
    } else {
      let data1 = 60 / 100;
      let data2 = 20 / 100;
      let data3 = 10 / 100;
      let data4 = 5 / 100;
      let data5 = 3 / 100;
      let data6 = 2 / 100;
      setFirstPrice(data1 * TotalPrize);
      setSecondPrice(data2 * TotalPrize);
      setThirdPrice(data3 * TotalPrize);
      setFourthPrice(data4 * TotalPrize);
      setSixthPrice(data6 * TotalPrize);
      setFifthPrice(data5 * TotalPrize);
    }
  };


  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Add Race</div>
            <div className="form">
              <form onSubmit={submit}>


                <div className="row mainrow">
                  <input type='date' data-placeholder="Race Date" onChange={(e) => setDay(e.target.value)}
                    value={Day} className='dateforrace' required aria-required="true" />
                  {/* <DatePicker
                    onChange={setDay}
                    value={Day}
                    monthPlaceholder="Date "
                    dayPlaceholder="&"
                    minDate={today}
                    maxDate={new Date("02-29-2023")}
                    yearPlaceholder="Time"
                    onBlur={() =>
                      Day === ""
                        ? setErrorDate("Date is required ")
                        : setErrorDate("Date is Validated")
                    }
                  /> */}

                  <span style={{ top: "270px" }} className={Day === "" ? "error" : "success"}>
                  </span>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <input
                      type="time"
                      onChange={(e) => setStartTime(e.target.value)}
                      value={StartTime}
                      onBlur={() =>
                        StartTime === ""
                          ? setErrorStartTime("Start Time is required ")
                          : setErrorStartTime("Start Time is Validated")
                      }
                    />
                    {/* <TimePicker
                      onChange={setStartTime}
                      value={StartTime}
                      minutePlaceholder={"Start Time"}
                      secondPlaceholder={""}
                      onBlur={() =>
                        StartTime === ""
                          ? setErrorStartTime("Start Time is required ")
                          : setErrorStartTime("Start Time is Validated")
                      }
                    /> */}
                    <span className="spanForm">|</span>
                    <span className={StartTime === "" ? "error" : "success"}>
                      {ErrorStartTime}
                    </span>
                  </div>

                  <div className="col-sm">
                    <input
                       onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                      placeholder="Race Number"
                      onChange={(e) => setRaceNumber(e.target.value)}
                      value={RaceNumber}
                      onBlur={() =>
                        RaceNumber === ""
                          ? setErrorEndTime("Race Number is required ")
                          : setErrorEndTime("Race Number is Validated")
                      }
                    />
                    {/* <TimePicker
                      onChange={setEndTime}
                      value={EndTime}
                      minutePlaceholder={"End Time"}
                      secondPlaceholder={""}
                      onBlur={() =>
                        EndTime === ""
                          ? setErrorEndTime("End Time is required ")
                          : setErrorEndTime("End Time is Validated")
                      }
                    /> */}
                    <span className="spanForm"></span>

                    <span className={RaceNumber === "" ? "error" : "success"}>
                      {ErrorEndTime}
                    </span>
                  </div>
                </div>
                <div className="row  mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Meeting Code"
                      className="mb-3"
                      onChange={(e) => setMeetingCode(e.target.value)}
                      value={MeetingCode}
                      onBlur={() =>
                        MeetingCode === ""
                          ? setErrorMeetingCode("Meeting Code is required ")
                          : setErrorMeetingCode("Meeting Code is Validated")
                      }
                    >
                      <Form.Control type="text" placeholder="Meeting Code" />
                    </FloatingLabel>
                    {/* <span className="spanForm"> |</span> */}
                    <span className={MeetingCode === "" ? "error" : "success"}>
                      {ErrorMeetingCode}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="رمز الاجتماع"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                    >
                      <Form.Control type="text" placeholder="رمز الاجتماع" />
                    </FloatingLabel>
                  </div> */}
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Select Horse Kind</div>}
                      defaultValue={HorsesKind}
                      onChange={sethorsesKind}
                      options={horsekindoptions}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        HorsesKind === ""
                          ? setErrorHorseKind("Horse Kind is required ")
                          : setErrorHorseKind("Horse Kind is Validated")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowHorseKind}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    <span className={HorsesKind === "" ? "error" : "success"}>
                      {ErrorHorseKind}
                    </span>
                  </div>


                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Race Name</div>}
                      defaultValue={RaceNameEn}
                      onChange={setRaceNameEn}
                      options={Racenameoptions}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        RaceNameEn === ""
                          ? setErrorRaceNameEn("Race Name is required ")
                          : setErrorRaceNameEn("Race Name is Validated")
                      }
                    />{" "}
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowName}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    <span className={RaceNameEn === "" ? "error" : "success"}>
                      {ErrorRaceNameEn}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>اسم العرق</div>}
                      className="selectdir"
                      defaultValue={RaceNameEn}
                      onChange={setRaceNameEn}
                      options={RacenameoptionsAr}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Sponsor</div>}
                      defaultValue={Sponsor}
                      onChange={setSponsor}
                      options={SponsorForTheRace}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        Sponsor === ""
                          ? setErrorSponsor("Sponsor  is required ")
                          : setErrorSponsor("Sponsor  is Validated")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowSponsor}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                    <span className={Sponsor === "" ? "error" : "success"}>
                      {ErrorSponsor}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>نوع السباق</div>}
                      defaultValue={Sponsor}
                      onChange={setSponsor}
                      options={SponsorForTheRaceAr}
                      className="selectdir"
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>RaceKind</div>}
                      defaultValue={RaceKind}
                      onChange={setRaceKind}
                      options={OptionRaceKind}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        RaceKind === ""
                          ? setErrorRaceKind("Race Kind is required ")
                          : setErrorRaceKind("")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowRaceKind}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    <span className={RaceKind === "" ? "error" : "success"}>
                      {ErrorRaceKind}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>نوع السباق</div>}
                      defaultValue={RaceKind}
                      className="selectdir"
                      onChange={setRaceKind}
                      options={OptionRaceKindAr}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Select Race Type</div>}
                      defaultValue={RaceTyp}
                      onChange={setRaceType}
                      options={RaceTypes}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        RaceTyp === ""
                          ? setErrorRaceType("Race Type is required ")
                          : setErrorRaceType("Race Type is Validated")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowRaceType}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    <span className={RaceTyp === "" ? "error" : "success"}>
                      {ErrorRaceTyp}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      className="selectdir"
                      placeholder={
                        <div style={{ direction: "rtl" }}>
                          اكتب للبحث عن الجنسية
                        </div>
                      }
                      defaultValue={RaceTyp}
                      onChange={setRaceType}
                      options={RaceTypesAr}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Select Track Length</div>}
                      defaultValue={TrackLength}
                      onChange={setTrackLength}
                      options={TrackLenght}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        TrackLength === ""
                          ? setErrorTrackLength("Track Length is required ")
                          : setErrorTrackLength("")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowTrackLength}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    <span className={TrackLength === "" ? "error" : "success"}>
                      {ErrorTrackLength}
                    </span>
                  </div>
                  {/* 
                  <div className="col-sm">
                    <Select
                      className="selectdir"
                      placeholder={
                        <div style={{ direction: "rtl" }}>
                          اكتب للبحث عن الجنسية
                        </div>
                      }
                      defaultValue={TrackLength}
                      onChange={setTrackLength}
                      options={TrackLenght}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>
                <div className="row  mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Description"
                      className="mb-3"
                      onChange={(e) => setDescriptionEn(e.target.value)}
                      value={DescriptionEn}
                      onBlur={() => setErrorDescriptionEn(obj)}
                    >
                      <Form.Control type="text" placeholder="Description" />
                    </FloatingLabel>
                    <span className="spanForm"> |</span>
                    <span
                      className={
                        ErrorDescriptionEn.status ? "success" : "error"
                      }
                    >
                      {ErrorDescriptionEn.message}
                    </span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label=" وصف"
                      className="mb-3 floatingInputAr"
                      onChange={(e) => setDescriptionAr(e.target.value)}
                      value={DescriptionAr}
                      style={{ direction: "rtl" }}
                      onBlur={() => setErrorDescriptionAr(objAr)}
                    >
                      <Form.Control type="text" placeholder=" وصف" />
                    </FloatingLabel>
                    <span
                      className={
                        ErrorDescriptionAr.status ? "successAr" : "errorAr"
                      }
                    >
                      {ErrorDescriptionAr.message}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Meeting Type</div>}
                      defaultValue={MeetingType}
                      onChange={setMeetingType}
                      options={MeetingTypes}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        MeetingType === ""
                          ? setErrorMeetingType("Meeting Type is required ")
                          : setErrorMeetingType("Meeting Type is Validated")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowType}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                    <span className={MeetingType === "" ? "error" : "success"}>
                      {ErrorMeetingType}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>نوع الاجتماع</div>}
                      defaultValue={MeetingType}
                      className="selectdir"
                      options={MeetingTypesAr}
                      onChange={setMeetingType}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>





                <div className="row  mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Weather Degree"
                      className="mb-3"
                      onChange={(e) =>
                        setDstate({ ...Dstate, WeatherDegree: e.target.value })
                      }
                      onBlur={() =>
                        Dstate.WeatherDegree === ""
                          ? setErrorWeatherDegree("Weather Degree is required ")
                          : setErrorWeatherDegree("Weather Degree is Validated")
                      }
                    >
                      <Form.Control
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        placeholder="Weather Degree"
                        value={Dstate.WeatherDegree}
                      />
                    </FloatingLabel>
                    {/* <span className="spanForm"> |</span> */}
                    <span
                      className={Dstate.WeatherDegree === "" ? "error" : "success"}
                    >
                      {ErrorWeatherDegree}
                    </span>
                  </div>


                </div>







                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>WeatherType</div>}
                      defaultValue={WeatherTypes[0]}
                      onChange={setWeatherType}
                      options={WeatherTypes}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        WeatherType === ""
                          ? setErrorWeatherType("Weather Type is required ")
                          : setErrorWeatherType("Weather Type is Validated")
                      }
                    />{" "}
                    {/* <span className="spanForm"> |</span> */}
                    <span className={WeatherType === "" ? "error" : "success"}>
                      {ErrorWeatherType}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>طقس</div>}
                      className="selectdir"
                      options={WeatherTypesAr}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Race Course</div>}
                      defaultValue={RaceCourse}
                      onChange={setRaceCourse}
                      options={racecourses}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        RaceCourse === ""
                          ? setErrorRaceCourse("Race Course is required ")
                          : setErrorRaceCourse("Race Course is Validated")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowRaceCourse}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    <span className={RaceCourse === "" ? "error" : "success"}>
                      {ErrorRaceCourse}
                    </span>
                  </div>
                  {/* 
                  <div className="col-sm">
                    <Select
                      placeholder={<div>دورة السباق</div>}
                      className="selectdir"
                      options={racecoursesAr}
                      defaultValue={RaceCourse}
                      onChange={setRaceCourse}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Race Status</div>}
                      defaultValue={RaceStatuss[2]}
                      onChange={setRaceStatus}
                      options={RaceStatuss}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        RaceStatus === ""
                          ? setErrorRaceStatus("Race Status is required ")
                          : setErrorRaceStatus("")
                      }
                    />
                    {/* <span className="spanForm"> |</span> */}
                    <span className={RaceStatus === "" ? "error" : "success"}>
                      {ErrorRaceStatus}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>حالة السباق</div>}
                      className="selectdir"
                      options={RaceStatussAr}
                      defaultValue={RaceStatus}
                      onChange={setRaceStatus}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Track Condition</div>}
                      defaultValue={{ label: "GS", id: "08829866-24d9-4573-9950-eab0962ee25c" }}
                      onChange={setTrackCondition}
                      options={trackconditionTable}
                      isClearable={true}
                      isSearchable={true}
                      onBlur={() =>
                        TrackCondition === ""
                          ? setErrorCondition("Track Condition is required ")
                          : setErrorCondition("Track Condition is Validated")
                      }
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowTrackCondition}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                    {/* <span className="spanForm"> |</span> */}
                    <span
                      className={TrackCondition === "" ? "error" : "success"}
                    >
                      {ErrorCondition}
                    </span>
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>حالة السباق</div>}
                      className="selectdir"
                      options={RaceStatussAr}
                      defaultValue={RaceStatus}
                      onChange={setRaceStatus}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>




                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Currency</div>}
                      defaultValue={{ label: "AED", id: "edb74f0d-12a1-4c2a-8965-e13f4924f9f3" }}
                      onChange={setCurrencyData}
                      options={currencyoption}
                      isClearable={true}
                      isSearchable={true}

                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowCurrency}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                    {/* <span className={Sponsor === "" ? "error" : "success"}>
                      {ErrorSponsor}
                    </span> */}
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>نوع السباق</div>}
                      defaultValue={Sponsor}
                      onChange={setSponsor}
                      options={SponsorForTheRaceAr}
                      className="selectdir"
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Enter Total Prize"
                      className="mb-3"
                      onChange={(e) => setTotalPrize(e.target.value)}
                      value={TotalPrize}
                      min="0"
                      onBlur={() =>
                        TotalPrize === ""
                          ? setErrorFirstPrice("Total Prize is required ")
                          : setErrorFirstPrice("Total Prize is Validated")
                      }
                    >
                      <Form.Control
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onPaste={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        min="0"
                        placeholder="Enter Total Prize"
                      />
                    </FloatingLabel>
                    <span className={TotalPrize === "" ? "error" : "success"}>
                      {ErrorFirstPrice}
                    </span>
                  </div>
                </div>


                <div
                  className="ButtonSection"
                  style={{ justifyContent: "space-between" }}
                >
                  <div>
                    <div className="ViewCalulatedPrize">
                      <div className="ViewCalulatedPrize1122">
                        <span className="ViewCalulatedPrize111">
                          <input
                            type="checkbox"
                            id="vehicle1"
                            name="checked"
                            value={isSubscribed}
                            onChange={handleChange}
                          />
                          <label for="vehicle1">Six Position</label>
                          <br />
                        </span>
                        <span
                          onClick={calculatePrize}
                          className="ViewCalulatedPrize1"
                        >
                          Calculate Prize
                        </span>
                      </div>

                      {showCalculated ?

                        <table className="calcualtedPrizeTable">
                          <tr>
                            <th>First Prize</th>
                            <th>Second Prize</th>
                            <th>Third Prize</th>
                            <th>Fourth Prize</th>
                            <th>Fifth Prize</th>
                            <th>Six Prize</th>
                          </tr>
                          <tr>
                            <td>{FirstPrice}</td>
                            <td>{SecondPrice}</td>
                            <td>{ThirdPrice}</td>
                            <td>{FourthPrice}</td>
                            <td>{FifthPrice}</td>
                            <td>{SixthPrice}</td>
                          </tr>
                        </table> : <></>}
                    </div>
                  </div>

                  <button
                    type="submit"
                    className="SubmitButton"
                    disabled={isLoading}
                  >
                    Save & Add Horses
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      {/*  ------------Modal Popup ------------------ */}

      <Modal
        show={showName}
        onHide={handleCloseName}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Race Name</h2>
        </Modal.Header>
        <Modal.Body>
          <Racename />
        </Modal.Body>
      </Modal>
      <Modal
        show={showType}
        onHide={handleCloseType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Meeting Type</h2>
        </Modal.Header>
        <Modal.Body>
          <MeetingTypePopUp />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceType}
        onHide={handleCloseRaceType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Race Type</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceTypePopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showTrackLength}
        onHide={handleCloseTrackLength}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Track Length</h2>
        </Modal.Header>
        <Modal.Body>
          <TrackLengthPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceKind}
        onHide={handleCloseRaceKind}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Race Kind</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceKindPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceCourse}
        onHide={handleCloseRaceCourse}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Create Race Course</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceCoursePopup />
        </Modal.Body>
      </Modal>

      <Modal
        show={showJockey}
        onHide={handleCloseJockey}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Create Jockey</h2>
        </Modal.Header>
        <Modal.Body>
          <JockeyPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showSponsor}
        onHide={handleCloseSponsor}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Sponsor</h2>
        </Modal.Header>
        <Modal.Body>
          <SponsorPopup />
        </Modal.Body>
      </Modal>

      <Modal
        show={showcurrency}
        onHide={handleCloseCurrency}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Currency</h2>
        </Modal.Header>
        <Modal.Body>
          <CurrencyPopup />
        </Modal.Body>
      </Modal>


      <Modal
        show={showGroundType}
        onHide={handleCloseGroundType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Ground Type</h2>
        </Modal.Header>
        <Modal.Body>
          <GroundTypePopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showCondition}
        onHide={handleCloseTrackCondition}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Track Condition</h2>
        </Modal.Header>
        <Modal.Body>
          <TrackConditionPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showhorseKind}
        onHide={handleCloseHorseKind}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Horse Kind</h2>
        </Modal.Header>
        <Modal.Body>
          <HorseKindPopup />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default RaceForm;
