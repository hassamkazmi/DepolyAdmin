import React, { useEffect, useRef } from "react";
import "react-toastify/dist/ReactToastify.css";
import { fetchjockey } from "../../../redux/getReducer/getJockeySlice";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchHorseForRace } from "../../../redux/getDropDownReducer/getHorseRace";
import { toast } from "react-toastify";
import Form from "react-bootstrap/Form";
import Select from "react-select";
import swal from "sweetalert";
import axios from "axios";
import { ImCross } from "react-icons/im";
import { TiTick } from "react-icons/ti";
import { fetchColorDropdown } from "../../../redux/getDropDownReducer/getColor";
import { fetchequipmentdropdown } from "../../../redux/getDropDownReducer/getEquipment";
import { Modal, OverlayTrigger, Tooltip } from "react-bootstrap";
import { AiOutlineReload } from "react-icons/ai";
import HorsePopup from "../../PostTable/HorsePopupForm"
import JockeyPopup from "../../PostTable/JockeyPopupForm";
import ColorPopup from "../../PostTable/Color";
import EquipmentPopup from "../../PostTable/Equipment";
import { fetchsilkcolordropdown } from "../../../redux/getDropDownReducer/getOwnerSilkColor";

const AddHorse = () => {
  //------------------------------get data from redux-----------------------------------------//
  const { data: jockey } = useSelector((state) => state.jockey);
  const { data: HorseForRace } = useSelector((state) => state.HorseForRace);
  const { data: equipmentdropdown } = useSelector(
    (state) => state.equipmentdropdown
  );
  const { data: colordropdown } = useSelector((state) => state.colordropdown);
  const { data: silkcolordropdown } = useSelector((state) => state.silkcolordropdown);

  //------------------------------state-----------------------------------------//
  const [HorseData, SetHorseData] = useState("");

  const [Gate, setGate] = useState("");
  const [EquipmentData, SetEquipmentData] = useState("");
  const [JockeyData, SetJockeyData] = useState("");
  const [CapColor, SetCapColor] = useState([]);
  const [HorseStatus, SetHorseStatus] = useState(true);
  const [items, setitems] = useState([]);

  const [HorseNo, setHorseNo] = useState(1);
  const [RaceWeight, setRaceWeight] = useState("");
  const [silkid, setsilkid] = useState("");
  const [silkColor, setsilkColor] = useState("");



  //------------------------------state for popup -----------------------------------------//
  const [showHorse, setShowHorse] = useState(false)
  const [showJockey, setshowJockey] = useState(false)
  const [showColor, setshowColor] = useState(false)
  const [showEquipment, setshowEquipment] = useState(false)






  //------------------------------function Popup -----------------------------------------//

  const handleshowHorse = async () => {
    await setShowHorse(true)
  }
  const handleshowJockey = async () => {
    await setshowJockey(true)
  }
  const handleshowColor = async () => {
    await setshowColor(true)
  }
  const handleshowEquipment = async () => {
    await setshowEquipment(true)
  }



  const handleCloseHorse = () => setShowHorse(false)

  const handleCloseColor = () => setshowColor(false)

  const handleCloseJockey = () => setshowJockey(false)

  const handleCloseEquipment = () => setshowEquipment(false)

  let formRef = useRef();
  const [StoreData, setStoreData] = useState([]);
  //------------------------------drag Scroller -----------------------------------------//
  const slider = document.querySelector(".horizontal-scroll-wrapper");
  let isDown = false;
  let startX;
  let scrollLeft;
  if (slider) {
    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });

    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3;
      slider.scrollLeft = scrollLeft - walk;

    });
  }

  const history = useNavigate();
  const { state } = useLocation();
  const { RaceId } = state;
  //------------------------------option for dropdown-----------------------------------------//
  let MapGate = [];
  for (var i = 1; i < 100; i++) {
    MapGate.push({ id: i, value: i, label: i });
  }
  const G1 = StoreData.map((item) => item.GateNo);

  let Gateoptions = MapGate.map(function (item) {
    return {
      id: item._id,
      value: item.value,
      label: item.value,
      isdisabled:
        G1[0] !== item.value &&
          G1[1] !== item.value &&
          G1[2] !== item.value &&
          G1[3] !== item.value &&
          G1[4] !== item.value &&
          G1[5] !== item.value &&
          G1[6] !== item.value &&
          G1[7] !== item.value &&
          G1[8] !== item.value &&
          G1[9] !== item.value &&
          G1[10] !== item.value &&
          G1[11] !== item.value &&
          G1[12] !== item.value &&
          G1[13] !== item.value &&
          G1[14] !== item.value &&
          G1[15] !== item.value &&
          G1[16] !== item.value &&
          G1[17] !== item.value &&
          G1[18] !== item.value &&
          G1[19] !== item.value &&
          G1[20] !== item.value
          ? false
          : true,
    };
  });
  const A1 = StoreData.map((item) => item.HorseId);

  let horseoptions = HorseForRace.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      ActiveOwnerid:item.ActiveOwner === null ? <></> : item.ActiveOwner,
      label: item.NameEn,
      Ownername:
        item.ActiveOwnerData === null ? <>NAN</> : item.ActiveOwnerData.NameEn,
      rating: item.STARS,
      ownerSilk:  item.ActiveOwnerData === null ? <>NAN</> : item.ActiveOwnerData.OwnerIDData,
      isdisabled:
        A1[0] !== item._id &&
          A1[1] !== item._id &&
          A1[2] !== item._id &&
          A1[3] !== item._id &&
          A1[4] !== item._id &&
          A1[5] !== item._id &&
          A1[6] !== item._id &&
          A1[7] !== item._id &&
          A1[8] !== item._id &&
          A1[9] !== item._id &&
          A1[10] !== item._id &&
          A1[11] !== item._id &&
          A1[12] !== item._id &&
          A1[13] !== item._id &&
          A1[14] !== item._id &&
          A1[15] !== item._id &&
          A1[16] !== item._id &&
          A1[17] !== item._id &&
          A1[18] !== item._id &&
          A1[19] !== item._id &&
          A1[20] !== item._id &&
          A1[21] !== item._id
          ? false
          : true,
    };
  });

  // let coloroption = colordropdown.map(function (item) {
  //   return {
  //     id: item._id,
  //     value: item.NameEn,
  //     label: item.NameEn,
  //   };
  // });

  const J1 = StoreData.map((item) => item.JockeyDataId);

  let AllJockey = jockey.map(function (item) {
    return {
      id: item._id,
      value: item._id,
      label: item.NameEn,
      weight: item.JockeyAllowance,
      minweight: item.MiniumumJockeyWeight,
      isdisabled:
        J1[0] !== item._id &&
          J1[1] !== item._id &&
          J1[2] !== item._id &&
          J1[3] !== item._id &&
          J1[4] !== item._id &&
          J1[5] !== item._id &&
          J1[6] !== item._id &&
          J1[7] !== item._id &&
          J1[8] !== item._id &&
          J1[9] !== item._id &&
          J1[10] !== item._id &&
          J1[11] !== item._id &&
          J1[12] !== item._id &&
          J1[13] !== item._id &&
          J1[14] !== item._id &&
          J1[15] !== item._id &&
          J1[16] !== item._id &&
          J1[17] !== item._id &&
          J1[18] !== item._id &&
          J1[19] !== item._id &&
          J1[20] !== item._id
          ? false
          : true,
    };
  });

  let AllEquipment = equipmentdropdown.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
    };
  });

  const dispatch = useDispatch();

  const HorseLength = HorseForRace.length;
  const ItemLength = items.length;
  const [Value1, setValue1] = useState("");

  const handleChangeInput = (value) => {
    setValue1(value);
  };
  const DataABC = JSON.parse(localStorage.getItem("mapping"));
  const fetchall = () => {
    dispatch(fetchHorseForRace({ RaceId, Value1 }));
    dispatch(fetchjockey());
    dispatch(fetchequipmentdropdown());



  }

  const AOwnerid = HorseData.ActiveOwnerid === '' ? <></> : HorseData.ActiveOwnerid

  useEffect(() => {
    formRef.current?.reset();
    dispatch(fetchHorseForRace({ RaceId, Value1 }));
    dispatch(fetchjockey());
    dispatch(fetchsilkcolordropdown({AOwnerid}));
    setsilkColor(HorseData.ownerSilk)
    dispatch(fetchequipmentdropdown());
    localStorage.setItem("lists", JSON.stringify(items));
    localStorage.setItem("mapping", JSON.stringify(StoreData));
  }, [dispatch, items, StoreData, Value1, RaceId,AOwnerid]);
  //------------------------------Remove Item-----------------------------------------//
  const removesingleItem = (id) => {
    const updateItems = StoreData.filter((elem, ind) => {
      return ind !== id;
    });
    setStoreData(updateItems);
  };
  //------------------------------Save Item-----------------------------------------//


  let AllsilkColor = silkcolordropdown.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      urlimg:item.OwnerSilkColor,
      label: (
        <img src={item.OwnerSilkColor} height="30px" width="30px" alt="" />

      ),
    };
  });


  const saveItem = (e) => {
    e.preventDefault();

    let HorseEntry = {
      GateNo: Gate === '' ? 100 : Gate.value,
      HorseNo: HorseNo,
      HorseModelId: HorseData.id,
      Equipment: EquipmentData.id,
      JockeyOnRace: JockeyData.id,
      JockeyWeight: JockeyData.weight,
      Rating: HorseData.rating,
      HorseRunningStatus: HorseStatus,
      CapColor: CapColor.id === '' ? null : CapColor.id,
      JockeyRaceWeight: RaceWeight === "" ? undefined : RaceWeight,
    };
    let MappingData = {
      GateNo: Gate.value,
      HorseNo: HorseNo,
      HorseName: HorseData.label,
      EquipmentName: EquipmentData.label,
      OwnerOnRace: HorseData.Ownername,
      JockeyOnRaceName: JockeyData.label,
      JockeyWeight: JockeyData.weight,
      MiniumumJockeyWeight: JockeyData.minweight,
      RaceWeight: RaceWeight,
      Rating: HorseData.rating,
      HorseRunningStatus: HorseStatus,
      CapColor: CapColor.urlimg,
      HorseId: HorseData.id,
      JockeyDataId: JockeyData.id,
    };

    if (HorseLength === ItemLength) {
      toast("No Horse ");
    }
      else if(JockeyData === '' && HorseStatus === true ){
        toast("Select Jockey Value ");
      }
     else if (
      HorseData === "" ||
      HorseNo === "" ||
      RaceWeight === ""
    ) {
      toast("Select Values ");
    } else {
      setitems([...items, HorseEntry]);
      setStoreData([...StoreData, MappingData]);
      setHorseNo(HorseNo + 1);

      formRef.current?.reset();
    }
    SetHorseStatus(true)
    setGate("");
    SetHorseData(" ");
    SetCapColor("");
    SetJockeyData("");
    SetEquipmentData("");
    setRaceWeight("");
  };
  //------------------------------remove all-----------------------------------------//
  const Remove = () => {
    setitems([]);
    setHorseNo(1);
    setStoreData([]);
  };

  const submit = async (event) => {
    event.preventDefault();
    if (ItemLength === 0) {
      toast("Please Add and Save Horse ");
    } else {
      try {
        await axios.post(`${window.env.API_URL}addracehorses/${RaceId}`, {
          HorseEntry: items,
        });
        localStorage.removeItem("lists");
        localStorage.removeItem("mapping");
        history("/fullpublishrace", {
          state: {
            RaceId: RaceId,
          },
        });

        swal({
          title: "Success",
          text: "Data has been added successfully ",
          icon: "success",
          button: "OK",
        });
      } catch (error) {
        const err = error.response.data.message;
        swal({
          title: "Error!",
          text: err,
          icon: "error",
          button: "OK",
        });
      }
    }
  };
  //------------------------------fetch Apis-----------------------------------------//
  const fetch = () => {
    dispatch(fetchHorseForRace({ RaceId, Value1 }));
    dispatch(fetchjockey());
    dispatch(fetchequipmentdropdown());
    dispatch(fetchsilkcolordropdown({AOwnerid}));

  };

  

  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Add Horse</h4>
              <OverlayTrigger
                overlay={<Tooltip id={`tooltip-top`}>Fetch New</Tooltip>}
              >
                <span className="fetchNewVerdict" onClick={fetch}>
                  <AiOutlineReload />
                </span>
              </OverlayTrigger>{" "}
            </div>

            <div className="horizontal-scroll-wrapper squares">
              <div className="myselecthorse">
                <div className="myselecthorsedata">
                  <span>Horse #</span>
                  <span>Gate #</span>
                  <span>Horse</span>
                  <span>Owner Name</span>
                  <span>Jockey</span>

                  <span> Weight</span>

                  <span>Min Weight</span>
                  <span>Race Weight</span>
                  <span>Rating</span>
                  <span>Cap Color</span>
                  <span>Equipment</span>
                  <span>Horse Status</span>
                  <span>Action</span>
                </div>
              </div>
              <div className="myselectdata">
                <hr />
                {!DataABC ? (
                  <></>
                ) : (
                  DataABC.map((item, i) => {
                    return (
                      <div
                        className="myselectiondataaddHorse myselectiondataaddHorse2 mynew"
                        key={i}
                      >
                        <span className="inputAddhorse3">
                          <input  onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }} value={item.HorseNo} min="1" />
                        </span>
                        <span className="inputAddhorse4">
                          <span>
                            <input  onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }} value={item.GateNo} />
                          </span>
                        </span>
                        <span className="InputAddhorse5">
                          <input
                            placeholder="Horse Name"
                            value={item.HorseName}
                            readOnly
                          />
                        </span>
                        <span className="InputAddhorse5">
                          <input
                            placeholder="Owner"
                            value={item.OwnerOnRace}
                            readOnly
                            className="ownerinput"
                          />
                        </span>

                        <span className="InputAddhorse5">
                          <input
                            placeholder="Jockey Name"
                            value={item.JockeyOnRaceName}
                            readOnly
                            className="inputAddhorse"
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            placeholder="Weight"
                            value={item.JockeyWeight}
                            readOnly
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            placeholder="Min Weight"
                            value={item.MiniumumJockeyWeight}
                            readOnly
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            placeholder="Race Weight"
                            value={item.RaceWeight}
                            readOnly
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            type="text"
                            value={item.Rating}
                            readOnly
                            placeholder="Rating"
                          />
                        </span>
                        <span className="inputAddhorse4">
                        <img src={item.CapColor} height="30px" width="30px" alt="" />
                        </span>
                        <span className="inputAddhorse4">
                          <input
                            type="text"
                            value={item.EquipmentName}
                            readOnly
                            placeholder="Equipment"
                          />
                        </span>

                        <Form.Check
                          type="switch"
                          id="custom-switch"
                          // onChange={() => SetHorseStatus(!HorseStatus)}
                          // value={HorseStatus}
                          defaultChecked={item.HorseRunningStatus}
                        />
                        <button
                          className="removebtnhorse"
                          onClick={() => removesingleItem(i)}
                        >
                          <ImCross />
                        </button>
                      </div>
                    );
                  })
                )}
                <form
                  ref={formRef}
                  onSubmit={saveItem}
                  className="myselectiondataaddHorse  myselectiondataaddHorse1 mynew"
                >
                  <span className="inputAddhorse3">
                    <input
                       onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                      onChange={(event) => {
                        const value = Number(event.target.value);
                        setHorseNo(value);
                      }}
                      value={HorseNo}
                      min="1"
                      name="horseNo"
                    />
                  </span>
                  <span className="inputAddhorse4">
                    <Select
                      placeholder="Gate #"
                      className="dropdown multidropdown w-5"
                      onChange={setGate}
                      options={Gateoptions}
                      isSearchable={true}
                      isOptionDisabled={(option) => option.isdisabled}
                      hideSelectedOptions={true}
                      value={Gate}
                    />
                  </span>
                  <span className="InputAddhorse5">

                    <Select
                      id="selectNow"
                      placeholder="Horse"
                      className="dropdown multidropdown"
                      onChange={SetHorseData}
                      options={horseoptions}
                      isOptionDisabled={(option) => option.isdisabled}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      onInputChange={handleChangeInput}
                      value={HorseData}
                      name="HorseValue"
                    />
                    <span className="spanForm spanForm1">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore addmore1" onClick={handleshowHorse}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore addmore1" onClick={fetchall}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </span>
                  <span className="InputAddhorse5">
                    <input
                      placeholder="Owner"
                      value={HorseData.Ownername}
                      className="ownerinput"
                    />
                  </span>
                  <span className="InputAddhorse5">
                    <Select
                      placeholder="Jockey"
                      className="dropdown multidropdown"
                      onChange={SetJockeyData}
                      options={AllJockey}
                      isOptionDisabled={(option) => option.isdisabled}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      value={JockeyData}
                    />
                    <span className="spanForm spanForm1">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore addmore1" onClick={handleshowJockey}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore addmore1" onClick={fetchall}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      placeholder="Weight"
                      value={JockeyData.weight}
                      readOnly
                    />
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      placeholder="Min Weight"
                      value={JockeyData.minweight}
                      readOnly
                    />
                  </span>
                  <span className="inputAddhorse3">
                    <input
                    onKeyPress={(event) => {
                      if (!/^\d*\.?\d*$/
                      .test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                      placeholder="Race Weight"
                      value={RaceWeight}
                      onChange={(e) => setRaceWeight(e.target.value)}
                    />
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      type="text"
                      value={HorseData.rating}
                      readOnly
                      placeholder="Rating"
                    />
                  </span>
                  <span className="inputAddhorse4">
                    <Select
                      placeholder="Color"
                      className="dropdown multidropdown"
                      onChange={SetCapColor}
                      options={AllsilkColor}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      value={CapColor}
                    />
                    <span className="spanForm spanForm1">
                      
                    </span>
                  </span>
                  <span className="inputAddhorse4">
                    <Select
                      placeholder="Equipment"
                      className="dropdown multidropdown"
                      onChange={SetEquipmentData}
                      options={AllEquipment}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      value={EquipmentData}
                    />
                    <span className="spanForm spanForm1">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore addmore1" onClick={handleshowEquipment}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore addmore1" onClick={fetchall}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </span>
                  <Form.Check
                    type="switch"
                    id="custom-switch"
                    onChange={() => SetHorseStatus(!HorseStatus)}
                    value={HorseStatus}
                    checked={HorseStatus}
                  />
                  <button className="savebtnhorse" onClick={saveItem}>
                    <TiTick />
                  </button>
                </form>
              </div>
            </div>
            <div className="sbmtbtndiv">
              <div className="RaceButtonDiv">
                <button className="updateButton" onClick={Remove}>
                  Remove
                </button>

                <button className="SubmitButton" type="submit" onClick={submit}>
                  Add Horses
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={showHorse} onHide={handleCloseHorse}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Horse</Modal.Title>
        </Modal.Header>
        <Modal.Body><HorsePopup /> </Modal.Body>

      </Modal>


      <Modal show={showJockey} onHide={handleCloseJockey}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Jockey</Modal.Title>
        </Modal.Header>
        <Modal.Body><JockeyPopup /> </Modal.Body>

      </Modal>
      <Modal show={showColor} onHide={handleCloseColor}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Color</Modal.Title>
        </Modal.Header>
        <Modal.Body><ColorPopup /> </Modal.Body>

      </Modal>
      <Modal show={showEquipment} onHide={handleCloseEquipment}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Equipment</Modal.Title>
        </Modal.Header>
        <Modal.Body><EquipmentPopup /> </Modal.Body>

      </Modal>


    </>
  );
};

export default AddHorse;
