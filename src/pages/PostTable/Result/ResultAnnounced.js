import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { remove } from "../../../redux/postReducer/postRace";

import "../../../Components/CSS/Table.css";
import ScrollContainer from "react-indiana-drag-scroll";
import "../../../Components/CSS/race.css";
import { Modal } from "react-bootstrap";

import { MdDelete } from "react-icons/md";
import swal from "sweetalert";

import {
  fetchraceresult,
  STATUSES,
} from "../../../redux/getReducer/getRaceResultAnnounced";
import Lottie from "lottie-react";
import HorseAnimation from "../../../assets/horselottie.json";

import Notfound from "../../../Notfound";

const Prize = (data) => {

  return (
    <>
      <h4 style={{
        textAlign: 'center',
        justifyContent: 'center',
        textDecoration: 'underline'
      }}>  {data.data.RaceResultData === undefined ||
        data.data.RaceResultData.length === 0 ? (
        <></>
      ) : (
        <>
         <p> Total Race Time : {data.data.RaceResultData[0].RaceTime}</p>
         <p>Best Turn Out Horse : {data.data.RaceResultData[0].BestTurnOutData ? data.data.RaceResultData[0].BestTurnOutData.NameEn : <></>}</p>
         <p>Best Turn Out Prize : {data.data.RaceResultData[0].BestTurnPrice ? data.data.RaceResultData[0].BestTurnPrice : <></>}</p>
        </>
      )}</h4>
      <table className="Prizeclass">

        <thead className="Prizeclassthead Prizeclass">
          <tr>
            <th>Rank </th>
            <th>Horse </th>
            <th>Distance</th>
            <th>Cumulative Distance </th>
            <th>Final Position </th>
            <th>Prize Win </th>
            <th>BeatenBy </th>
            <th>Rating </th>
            <th>Remarks </th>


          </tr>
        </thead>
        <tbody className="Prizeclasstbody Prizeclass">

          {data.data.RaceResultData === undefined ||
            data.data.RaceResultData.length === 0 ? (
            <></>
          ) : (
            data.data.RaceResultData.map((item) => {
              return (
                <>
                  <tr>


                  </tr>
                  <tr>
                    <td>
                      {item.FinalPositionDataHorse === null ? (
                        <></>
                      ) : (
                        item.FinalPositionDataHorse.Rank
                      )}
                    </td>
                    <td>
                      {item.HorseIDData === null ? (
                        <></>
                      ) : (
                        item.HorseIDData.NameEn
                      )}
                    </td>
                    <td>
                      {item.Distance === null ? (
                        <></>
                      ) : (
                        item.Distance
                      )}
                    </td>
                    <td>
                      {item.CumulativeDistance === null ? (
                        <></>
                      ) : (
                        item.CumulativeDistance
                      )}
                    </td>
                    <td>
                      {item.FinalPositionDataHorse === null ? (
                        <></>
                      ) : (
                        item.FinalPositionDataHorse.NameEn
                      )}
                    </td>
                    <td>
                      {item.PrizeWin === null ? (
                        <></>
                      ) : (
                        item.PrizeWin
                      )}
                    </td>
                    <td>
                      {item.BeatenByData === null ? (
                        <></>
                      ) : (
                        item.BeatenByData.NameEn
                      )}
                    </td>
                    <td>
                      {item.Rating === null ? (
                        <></>
                      ) : (
                        item.Rating
                      )}
                    </td>
                    <td>
                      {item.RaceTime === null ? (
                        <></>
                      ) : (
                        item.Remarks
                      )}
                    </td>
                  </tr>
                </>
              );
            })
          )}

        </tbody>
      </table>
    </>
  );
};
const Races = () => {
  const dispatch = useDispatch();

  const { data: raceresult, status } = useSelector((state) => state.raceresult);

  const [show, setShow] = useState(false);
  const [modaldata, setmodaldata] = useState();
  const handleClose = () => setShow(false);
  const handleShow = async (data) => {
    setmodaldata(data);
    await setShow(true);
  };

  const handleRemove = async (Id) => {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this imaginary file!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Poof! Your imaginary file has been deleted!", {
          icon: "success",
        });
        dispatch(remove(Id));
        dispatch(fetchraceresult());
      } else {
        swal("Your Data is safe!");
      }
    });
  };

  useEffect(() => {
    dispatch(fetchraceresult());
  }, [dispatch]);

  if (status === STATUSES.ERROR) {
    return (
      <h2
        style={{
          margin: "auto",
        }}
      >
        No Result Found
      </h2>
    );
  }
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Result Announced</h4>
              <div>

              </div>
            </div>

            <div className="div_maintb">
              <ScrollContainer className="scroll-container">
                <table className="Sc">
                  <thead
                    style={{
                      marginTop: "30px",
                    }}
                  >
                    <tr className="trtabletd">
                      <th>Action</th>
                      <th>Race Name</th>
                      <th>Winner</th>
                    </tr>
                  </thead>
                  {raceresult.length === 0 ? (


                    <Notfound />



                  ) : (
                    <>
                      {status === STATUSES.LOADING ? (
                        <Lottie
                          animationData={HorseAnimation}
                          loop={true}
                          className="TableLottie"
                        />
                      ) : (
                        <tbody
                          key={raceresult._id}
                          style={{
                            marginTop: "20px",
                          }}
                        >
                          {
                            raceresult.map((item) => {
                              return (
                                <tr>
                                  <td className="table_delete_btn1">

                                    <MdDelete
                                      style={{
                                        fontSize: "22px",
                                      }}
                                      onClick={() => handleRemove(item._id)}
                                    />
                                    {/* <BsEyeFill /> */}
                                  </td>

                                  <td>{item.RaceNameModelData === null || item.RaceNameModelData === undefined ? <>N/A</> : item.RaceNameModelData.NameEn} </td>
                                  <td>
                                    <button
                                      className="Approvedbtn resultbtn"
                                      onClick={() => handleShow(item)}
                                    >
                                      Click
                                    </button>
                                  </td>

                                </tr>
                              )
                            })
                          }

                        </tbody>
                      )}
                    </>
                  )}
                </table>
              </ScrollContainer>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className=""
      >
        <Modal.Header closeButton>
          <h2>Race Result </h2>
        </Modal.Header>
        <Modal.Body>
          <Prize data={modaldata} />
        </Modal.Body>
      </Modal>
    </>
  );
};
export default Races;
