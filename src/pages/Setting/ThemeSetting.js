import { useState } from "react";
import { Fragment } from "react";
import { Link, useNavigate } from "react-router-dom";

const ThemeSetting = () => {
  const history = useNavigate();

  const [PrimaryColor , setPrimaryColor] = useState('');
  const [SecondaryColor , setSecondaryColor] = useState('')
  const [HelperColor , setHelperColor] = useState('')

  return (
    <Fragment>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Theme Setting</h4>
              <div>
                <h6
                  style={{
                    marginRight: "100px",
                    alignItems: "center",
                    color: "rgba(0, 0, 0, 0.6)",
                  }}
                ></h6>
              </div>
            </div>
            <>
              <div className="themesetting">
              <p>Select Primary Color</p>
                <div className="ForPrimaryColor">
                  <div className="OnePrimaryColor">
                  </div>
                  <div className="TwoPrimaryColor">

                  </div>
                  <div className="ThreePrimaryColor">

                  </div>
                </div>
                <p>Select Secondary Color</p>
                <div className="ForPrimaryColor">
                  <div className="OnePrimaryColor">
                    
                  </div>
                  <div className="TwoPrimaryColor">

                  </div>
                  <div className="ThreePrimaryColor">

                  </div>
                </div>
                <p>Select Helper Color</p>
                <div className="ForPrimaryColor">
                  <div className="OnePrimaryColor">
                    
                  </div>
                  <div className="TwoPrimaryColor">

                  </div>
                  <div className="ThreePrimaryColor">

                  </div>
                </div>
              </div>
              <div className="ButtonSection" style={{
                marginTop:'100px'
              }}>
                <button
                  type="submit"
                  className="SubmitButton"
                  onClick={() => history(-1)}
                >
                  Back
                </button>
                <button
                  type="submit"
                  className="SubmitButton"
                >
                  Apply
                </button>
              </div>
            </>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default ThemeSetting;
