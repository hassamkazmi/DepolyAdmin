import React, { useState, useEffect } from "react";
import "../../../Components/CSS/forms.css";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import Select from "react-select";
import { fetchracecourse } from "../../../redux/getReducer/getRaceCourseSlice";
import { useSelector, useDispatch } from "react-redux";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { AiOutlineReload } from "react-icons/ai";
import { Modal } from "react-bootstrap";

import TrackConditionPopup from "../../PostTable/TrackCondition";

import Racename from "../../PostTable/Racenameform";
import MeetingTypePopUp from "../../PostTable/MeetingType";
import RaceTypePopup from "../../PostTable/Racetypeform";

import TrackLengthPopup from "../../PostTable/Tracklengthform";
import GroundTypePopup from "../../PostTable/GroundType";
import RaceKindPopup from "../../PostTable/RaceKind";
import RaceCoursePopup from "../../PostTable/RaceCourseForm";
import JockeyPopup from "../../PostTable/JockeyForm";
import SponsorPopup from "../../PostTable/SponsorForm";
import HorseKindPopup from "../../PostTable/Horsekindform";
import { fetchSponsorDropdown } from "../../../redux/getDropDownReducer/getSponsor";
import { fetchMeeting } from "../../../redux/getReducer/getMeeting";
import { fetchRaceTypeDropdown } from "../../../redux/getDropDownReducer/getRaceType";
import { fetchTrackConditionDropdown } from "../../../redux/getDropDownReducer/getTrackConditionDropDown";
import { fetchRaceName } from "../../../redux/getReducer/getRaceName";
import { fetchTrackLength } from "../../../redux/getReducer/getTracklength";
import { fetchRaceKind } from "../../../redux/getReducer/getRaceKind";
import { fetchgroundtype } from "../../../redux/getReducer/getGroundType";

import { fetchcurrency } from "../../../redux/getReducer/getCurrency";
import { fetchHorseKind } from "../../../redux/getReducer/getHorseKind";

const RaceStatuss = [
  { id: "1", value: "Cancelled", label: "Cancel" },
  { id: "2", value: "Due", label: "Due" },
  { id: "2", value: "Live", label: "Live" },
  { id: "2", value: "Completed", label: "End" },
];
const WeatherTypes = [
  {
    id: "1",
    value: "Sunny",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Sunny</p>
        <p>مشمس</p>
      </div>
    ),
  },
  {
    id: "2",
    value: "Cloudy",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>Cloudy</p>
        <p>غائم</p>
      </div>
    ),
  },
];

const NewsForm = () => {
  const history = useNavigate();

  const { state } = useLocation();
  const dispatch = useDispatch();
  const { data: racecourse } = useSelector((state) => state.racecourse);
  const { data: sponsordropdown } = useSelector((state) => state.sponsordropdown);
  const { data: meeting } = useSelector((state) => state.meeting);
  const { data: racetypedropdown } = useSelector((state) => state.racetypedropdown);
  const { data: RaceName } = useSelector((state) => state.RaceName);
  const { data: trackLength } = useSelector((state) => state.trackLength);
  const { data: raceKinds } = useSelector((state) => state.raceKinds);

  const { data: TrackConditiondropdown } = useSelector((state) => state.TrackConditiondropdown);
  const { data: currency } = useSelector((state) => state.currency);
  const { data: HorseKind } = useSelector((state) => state.HorseKind);
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [TotalPrize, setTotalPrize] = useState("");
  const [FirstPrice, setFirstPrice] = useState("");
  const [SecondPrice, setSecondPrice] = useState("");
  const [ThirdPrice, setThirdPrice] = useState("");
  const [FourthPrice, setFourthPrice] = useState("");
  const [FifthPrice, setFifthPrice] = useState("");
  const [SixthPrice, setSixthPrice] = useState("");
  const [showCalculated, setshowCalculated] = useState(false);
  const { fullraceid } = state;
  const [Day, setDay] = useState("");
  const [WeatherType, setWeatherType] = useState("");
  const [Currency, setCurrency] = useState("");
  const [RaceStatus, setRaceStatus] = useState("");
  const [RaceCourse, setRaceCourse] = useState("");
  const [Sponsor, setSponsor] = useState("");
  const [MeetingType, setMeetingType] = useState("");
  const [RaceNameEn, setRaceNameEn] = useState("");
  const [TrackCondition, setTrackCondition] = useState("");
  const [HorsesKind, sethorsesKind] = useState("");
  const [RaceKind, setRaceKind] = useState("");
  const [RaceTyp, setRaceType] = useState("");

  const [showhorseKind, setshowhorseKind] = useState(false);

  const [TrackLength, setTrackLength] = useState("");


  let racecourses =
    racecourse === undefined ? (
      <></>
    ) : (
      racecourse.map(function (item) {
        return {
          id: item._id,
          value: item.TrackNameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.TrackNameEn}</p>
              <p>{item.TrackNameAr}</p>
            </div>
          ),
        };
      })
    );

  let trackconditionTable =
    TrackConditiondropdown === undefined ? (
      <></>
    ) : (
      TrackConditiondropdown.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let currencyoption =
    currency === undefined ? (
      <></>
    ) : (
      currency.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  let Racenameoptions =
    RaceName === undefined ? (
      <></>
    ) : (
      RaceName.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let SponsorForTheRace =
    sponsordropdown === undefined ? (
      <></>
    ) : (
      sponsordropdown.map(function (item) {
        return {
          id: item._id,
          value: item.TitleEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <span
                style={{
                  display: "flex",
                }}
              >
                <img src={item.image} height="30px" width="30px" alt="" />
                <p>{item.TitleEn}</p>
              </span>
              <span
                style={{
                  display: "flex",
                }}
              >
                <img src={item.image} height="30px" width="30px" alt="" />
                <p>{item.TitleAr}</p>
              </span>
            </div>
          ),
        };
      })
    );


  let MeetingTypes =
    meeting === undefined ? (
      <></>
    ) : (
      meeting.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  let horsekindoptions =
    HorseKind === undefined ? (
      <></>
    ) : (
      HorseKind.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let RaceTypes =
    racetypedropdown === undefined ? (
      <></>
    ) : (
      racetypedropdown.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let TrackLenght =
    trackLength === undefined ? (
      <></>
    ) : (
      trackLength.map(function (item) {
        return {
          id: item._id,
          value: item.TrackLength,
          label: item.TrackLength,
        };
      })
    );

  let OprtionRaceKind =
    raceKinds === undefined ? (
      <></>
    ) : (
      raceKinds.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  const [showName, setShowName] = useState(false);
  const [showType, setShowType] = useState(false);
  const [showRaceType, setShowRaceType] = useState(false);
  const [showTrackLength, setShowTrackLength] = useState(false);
  const [showGroundType, setShowGroundType] = useState(false);
  const [showRaceKind, setShowRaceKind] = useState(false);
  const [showRaceCourse, setShowRaceCourse] = useState(false);
  const [showJockey, setShowJockey] = useState(false);
  const [showSponsor, setShowSponsor] = useState(false);
  const [showCondition, setshowCondition] = useState(false);

  const handleCloseName = () => setShowName(false);
  const handleCloseType = () => setShowType(false);
  const handleCloseRaceType = () => setShowRaceType(false);
  const handleCloseTrackLength = () => setShowTrackLength(false);
  const handleCloseGroundType = () => setShowGroundType(false);
  const handleCloseRaceKind = () => setShowRaceKind(false);
  const handleCloseRaceCourse = () => setShowRaceCourse(false);
  const handleCloseJockey = () => setShowJockey(false);
  const handleCloseSponsor = () => setShowSponsor(false);
  const handleCloseTrackCondition = () => setshowCondition(false);
  const handleCloseHorseKind = () => setshowhorseKind(false);

  const handleShowTrackCondition = async () => {
    await setshowCondition(true);
  };

  const handleShowName = async () => {
    await setShowName(true);
  };
  const handleShowHorseKind = async () => {
    await setshowhorseKind(true);
  };
  const handleShowType = async () => {
    await setShowType(true);
  };

  const handleShowRaceType = async () => {
    await setShowRaceType(true);
  };
  const handleShowTrackLength = async () => {
    await setShowTrackLength(true);
  };



  const handleShowRaceKind = async () => {
    await setShowRaceKind(true);
  };

  const handleShowRaceCourse = async () => {
    await setShowRaceCourse(true);
  };

  const [StartTime, setStartTime] = useState('');

  const handleShowSponsor = async () => {
    await setShowSponsor(true);
  };
  const SearchCode = "";
  const SearchTitle = "";
  const SearchAge = "";
  const FetchNew = () => {
    dispatch(fetchracecourse({ SearchCode, SearchTitle, SearchAge }));
    // dispatch(fetchjockey({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchSponsorDropdown());
    dispatch(fetchMeeting({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchRaceTypeDropdown());
    dispatch(fetchRaceName({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchTrackLength({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchRaceKind({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchgroundtype({ SearchCode, SearchTitle, SearchAge }));
    // dispatch(fetchpointTable({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchTrackConditionDropdown());
    dispatch(fetchHorseKind({ SearchCode, SearchTitle, SearchAge }));
    dispatch(fetchcurrency({ SearchCode, SearchTitle, SearchAge }));
  };

  const [state1, setState] = useState({
    Day: "",
    MeetingCode: "",
    DescriptionEn: "",
    DescriptionAr: "",
    WeatherDegree: "",
    RaceStatus: "",
    RaceNumber: "",
    RaceCourse: "",
    HorseKindinRace: "",
    Sponsor: "",
    MeetingType: "",
    RaceName: "",
    RaceTyp: "",
    TrackLength: "",
    RaceKind: "",
    StartTime: "18:59",
    TotalPrize: "",
    WeatherType: "",
    TrackCondition: "",




  });

  console.log(state1.StartTime, 'DSADSAD');

  const [image, setImage] = useState();
  const [preview, setPreview] = useState();
  const totalprize =
    fullraceid.FirstPrice +
    fullraceid.SecondPrice +
    fullraceid.ThirdPrice +
    fullraceid.FourthPrice +
    fullraceid.FifthPrice +
    fullraceid.SixthPrice;
  useEffect(() => {

    if (fullraceid) {
      setState({
        MeetingCode: fullraceid.MeetingCode,
        DescriptionEn: fullraceid.DescriptionEn,
        DescriptionAr: fullraceid.DescriptionAr,
        WeatherDegree: fullraceid.WeatherDegree,
        RaceStatus: fullraceid.RaceStatus,
        WeatherType: fullraceid.WeatherType,
        TrackCondition: fullraceid.TrackConditionData._id,
        Currency: fullraceid.CurrencyData._id,
        RaceCourse: fullraceid.RaceCourseData._id,
        Sponsor: fullraceid.SponsorData._id,
        MeetingType: fullraceid.MeetingTypeData._id,
        RaceName: fullraceid.RaceNameModelData._id,
        RaceKind: fullraceid.RaceKindData._id,
        Day: fullraceid.Day,
        StartTime: fullraceid.StartTime,
        RaceNumber: fullraceid.RaceNumber,
        TotalPrize: totalprize,
        TrackLength: fullraceid.TrackLengthData._id,
        RaceTyp: fullraceid.RaceTypeModelData._id,
        HorseKindinRace: fullraceid.HorseKindinRaceData._id,
      });
      let date123 = new Date(fullraceid.StartTime);
      console.log(date123, "avv");
      let dateformat = `${date123.getHours()}:${date123.getMinutes()}`;
      if (date123.getMinutes() < 10) {
        dateformat = `${date123.getHours()}:${date123.getMinutes()}0`;

      }
      console.log(dateformat, 'date123');

      console.log(dateformat, "date1232");
      setStartTime(dateformat);
      let day123 = new Date(fullraceid.Day);
      let daydate = day123.getDate();
      if (daydate < 10) {
        daydate = `0${day123.getDate()}`;
      }
      let daymonth = day123.getMonth() + 1;
      if (daymonth < 10) {
        daymonth = `0${day123.getMonth() + 1}`;
      }
      let dayformat = `${day123.getFullYear()}-${daymonth}-${daydate}`;
      console.log(dayformat, "dayformat");

      setDay(dayformat);
    } else {
    }
  }, [fullraceid, totalprize]);


  const fileSelected = (event) => {
    const image = event.target.files[0];
    setImage(image, image);
  };
  useEffect(() => {
    dispatch(fetchracecourse());
    FetchNew();
    if (image === undefined) {
      setPreview(fullraceid.image);
      return;
    }
    const objectUrl = URL.createObjectURL(image);
    setPreview(objectUrl);
    return () => URL.revokeObjectURL(objectUrl);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [image]);

  const submit = async (event) => {
    event.preventDefault();

    let Timing = new Date(`${Day} ${StartTime}`);
    let Endtiming = new Date(Timing.getTime() + 1000);
    let UTCTime = Timing.toUTCString();
    let UTCDate = Endtiming.toUTCString();

    try {
      const formData = new FormData();

      formData.append("MeetingCode", state1.MeetingCode);
      formData.append("DescriptionEn", state1.DescriptionEn);
      formData.append("DescriptionAr", state1.DescriptionAr);
      formData.append("WeatherDegree", state1.WeatherDegree);
      formData.append("RaceNumber", state1.RaceNumber);
      formData.append("StartTime", StartTime === "" ? state1.StartTime : StartTime);
      formData.append("Day", Day === "" ? state1.Day : Day);
      formData.append("RaceType", RaceTyp.id === undefined ? state1.RaceTyp : RaceTyp.id);
      formData.append("RaceKind", RaceKind.id === undefined ? state1.RaceKind : RaceKind.id);
      formData.append("MeetingType", MeetingType.id === undefined ? state1.MeetingType : MeetingType.id);
      formData.append("Timing", UTCTime);
      formData.append("Endtiming", UTCDate);
      formData.append("PrizeNumber", isSubscribed === false ? 5 : 6);

      formData.append(
        "WeatherType",
        WeatherType.value === undefined ? state1.WeatherType : WeatherType.value
      );


      formData.append(
        "Currency",
        Currency.id === undefined ? state1.Currency : Currency.id
      );
      formData.append(
        "HorseKindinRace",
        HorsesKind.id === undefined ? state1.HorseKindinRace : HorsesKind.id
      );
      formData.append("totalPrize", state1.TotalPrize);
      formData.append(
        "TrackLength",
        TrackLength.id === undefined ? state1.TrackLength : TrackLength.id
      );
      formData.append(
        "TrackCondition",
        TrackCondition.id === undefined ? state1.TrackCondition : TrackCondition.id
      );


      formData.append(
        "RaceStatus",
        RaceStatus.value === undefined ? state1.RaceStatus : RaceStatus.value
      );
      formData.append(
        "RaceCourse",
        RaceCourse.id === undefined ? state1.RaceCourse : RaceCourse.id
      );
      formData.append(
        "Sponsor",
        Sponsor.id === undefined ? state1.Sponsor : Sponsor.id
      );
      // formData.append(
      //   "TrackLength",
      //   TrackLength.id === undefined ? state1.TrackLength : TrackLength.id
      // );

      formData.append(
        "RaceName",
        RaceNameEn.id === undefined ? state1.RaceName : RaceNameEn.id
      );
      // formData.append("RaceType", RaceTyp.id === undefined ? state1.RaceTyp : RaceTyp.id);

      await axios.put(
        `${window.env.API_URL}/updaterace/${fullraceid._id}`,
        formData
      );
      history("/races");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

  const GoToEditRace = async (event) => {
    event.preventDefault();
    let Timing = new Date(`${Day} ${StartTime}`);
    let Endtiming = new Date(Timing.getTime() + 1000);
    let UTCTime = Timing.toUTCString();
    let UTCDate = Endtiming.toUTCString();
    try {
      const formData = new FormData();
      formData.append("MeetingCode", state1.MeetingCode);
      formData.append("DescriptionEn", state1.DescriptionEn);
      formData.append("DescriptionAr", state1.DescriptionAr);
      formData.append("WeatherDegree", state1.WeatherDegree);
      formData.append("RaceNumber", state1.RaceNumber);
      formData.append("StartTime", StartTime);
      formData.append("Day", Day === "" ? Day : Day);
      formData.append("Timing", UTCTime);
      formData.append("Endtiming", UTCDate);
      formData.append("RaceType", RaceTyp.id === undefined ? state1.RaceTyp : RaceTyp.id);
      formData.append("RaceKind", RaceKind.id === undefined ? state1.RaceKind : RaceKind.id);

      formData.append(
        "WeatherType",
        WeatherType.value === undefined ? state1.WeatherType : WeatherType.value
      );

      formData.append(
        "MeetingType",
        MeetingType.id === undefined ? state1.MeetingType : MeetingType.id
      );
      formData.append(
        "HorseKindinRace",
        HorsesKind.id === undefined ? state1.HorseKindinRace : HorsesKind.id
      );
      formData.append("totalPrize", state1.TotalPrize);
      formData.append(
        "TrackLength",
        TrackLength.id === undefined ? state1.TrackLength : TrackLength.id
      );
      formData.append(
        "TrackCondition",
        state1.TrackCondition
      );


      formData.append(
        "RaceStatus",
        RaceStatus.value === undefined ? state1.RaceStatus : RaceStatus.value
      );
      formData.append(
        "RaceCourse",
        RaceCourse.id === undefined ? state1.RaceCourse : RaceCourse.id
      );
      formData.append(
        "Sponsor",
        Sponsor.id === undefined ? state1.Sponsor : Sponsor.id
      );
      // formData.append(
      //   "TrackLength",
      //   TrackLength.id === undefined ? state1.TrackLength : TrackLength.id
      // );

      formData.append(
        "RaceName",
        RaceNameEn.id === undefined ? state1.RaceName : RaceNameEn.id
      );




      await axios.put(
        `${window.env.API_URL}/updaterace/${fullraceid._id}`,
        formData
      );
      history("/editracehorse", {
        state: {
          RaceId: fullraceid._id,
        },
      });
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }

  };
  const handleChange = (event) => {
    if (event.target.checked) {
      console.log("✅ Checkbox is checked", isSubscribed);
    } else {
      console.log("⛔️ Checkbox is NOT checked", isSubscribed);
    }
    setIsSubscribed((current) => !current);
  };

  const calculatePrize = () => {
    setshowCalculated(true);
    if (!isSubscribed) {
      let data1 = 60 / 100;
      let data2 = 20 / 100;
      let data3 = 11 / 100;
      let data4 = 6 / 100;
      let data5 = 3 / 100;
      setFirstPrice(data1 * state1.TotalPrize);
      setSecondPrice(data2 * state1.TotalPrize);
      setThirdPrice(data3 * state1.TotalPrize);
      setFourthPrice(data4 * state1.TotalPrize);
      setSixthPrice("0");
      setFifthPrice(data5 * state1.TotalPrize);
    } else {
      let data1 = 60 / 100;
      let data2 = 20 / 100;
      let data3 = 10 / 100;
      let data4 = 5 / 100;
      let data5 = 3 / 100;
      let data6 = 2 / 100;
      setFirstPrice(data1 * state1.TotalPrize);
      setSecondPrice(data2 * state1.TotalPrize);
      setThirdPrice(data3 * state1.TotalPrize);
      setFourthPrice(data4 * state1.TotalPrize);
      setSixthPrice(data6 * state1.TotalPrize);
      setFifthPrice(data5 * state1.TotalPrize);
    }
  };
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Race</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <input type='date' data-placeholder="Race Date" onChange={(e) => setDay(e.target.value)}
                    value={Day} className='dateforrace' required />
                  {/* <DatePicker
                    onChange={setDay}
                    value={Day}
                    monthPlaceholder={state1.Day}
                  /> */}
                  {/* <span style={{
                    textAlign: 'center'
                  }}>{state1.Day}</span> */}
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <input
                      type="time"
                      onChange={(e) => setStartTime(e.target.value)}
                      value={StartTime}
                    // required
                    // onChange={(e) =>
                    //   setState({ ...state1, StartTime: e.target.value })
                    // }
                    // value={state1.StartTime}
                    />

                    <span className="spanForm">|</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Race Number"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, RaceNumber: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Race Number"
                        value={state1.RaceNumber}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Meeting</p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {fullraceid.MeetingTypeData &&
                            fullraceid.MeetingTypeData.NameEn}
                        </div>
                      }
                      defaultValue={MeetingType}
                      onChange={setMeetingType}
                      options={MeetingTypes}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowType}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Race Name </p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {fullraceid.RaceNameModelData &&
                            fullraceid.RaceNameModelData.NameEn}
                        </div>
                      }
                      defaultValue={RaceNameEn}
                      onChange={setRaceNameEn}
                      options={Racenameoptions}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowName}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Meeting Code"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, MeetingCode: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.MeetingCode}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Description"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, DescriptionEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.DescriptionEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="وصف"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, DescriptionAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="ملاحظات"
                        value={state1.DescriptionAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Weather Degree"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, WeatherDegree: e.target.value })
                      }
                    >
                      <Form.Control
                        onKeyPress={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        placeholder="Weather Degree"
                        value={state1.WeatherDegree}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Weather Type</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{fullraceid.WeatherType}</div>}
                      defaultValue={WeatherType}
                      onChange={setWeatherType}
                      options={WeatherTypes}
                      isClearable={true}
                      isSearchable={true}
                    />{" "}
                    {/* <span className="spanForm"> |</span> */}
                  </div>

                  {/* <div className="col-sm">
                    <Select
                      placeholder={<div>طقس</div>}
                      className="selectdir"
                      options={WeatherTypesAr}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div> */}
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Race Kind</p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {fullraceid.RaceKindData &&
                            fullraceid.RaceKindData.NameEn}
                        </div>
                      }
                      defaultValue={RaceKind}
                      onChange={setRaceKind}
                      options={OprtionRaceKind}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowRaceKind}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Race Type</p>

                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {fullraceid.RaceTypeModelData &&
                            fullraceid.RaceTypeModelData.NameEn}
                        </div>
                      }

                      defaultValue={RaceTyp}
                      onChange={setRaceType}
                      options={RaceTypes}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowRaceType}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Race Status</p>

                  <div className="col-sm">
                    <Select
                      placeholder={<div>{fullraceid.RaceStatus}</div>}
                      defaultValue={RaceStatus}
                      onChange={setRaceStatus}
                      options={RaceStatuss}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Race Course</p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>{fullraceid.RaceCourseData.TrackNameEn}</div>
                      }
                      defaultValue={RaceCourse}
                      onChange={setRaceCourse}
                      options={racecourses}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowRaceCourse}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Horse Kind</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{fullraceid.HorseKindinRaceData && fullraceid.HorseKindinRaceData.NameEn}</div>}
                      defaultValue={HorsesKind}
                      onChange={sethorsesKind}
                      options={horsekindoptions}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowHorseKind}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Track Length</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{fullraceid.TrackLengthData && fullraceid.TrackLengthData.TrackLength}</div>}
                      defaultValue={TrackLength}
                      onChange={setTrackLength}
                      options={TrackLenght}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowTrackLength}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Track Condition</p>

                  <div className="col-sm">
                    <Select
                      placeholder={<div>{fullraceid.TrackConditionData && fullraceid.TrackConditionData.NameEn}</div>}
                      defaultValue={TrackCondition}
                      onChange={setTrackCondition}
                      options={trackconditionTable}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowTrackCondition}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Currency</p>

                  <div className="col-sm">
                    <Select
                      placeholder={<div>Currency</div>}
                      defaultValue={Currency}
                      onChange={setCurrency}
                      options={currencyoption}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowSponsor}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Sponsor Image</p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div className="sponsorPlaceholder d-flex">
                          <p>{fullraceid.SponsorData.TitleEn}</p>
                          <img src={fullraceid.SponsorData.image} alt="" />
                        </div>
                      }
                      defaultValue={Sponsor}
                      onChange={setSponsor}
                      options={SponsorForTheRace}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowSponsor}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Enter Total Prize"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, TotalPrize: e.target.value })
                      }
                      min="0"
                    >
                      <Form.Control
                        onKeyPress={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        onPaste={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        placeholder="Enter Total Prize"
                        value={state1.TotalPrize}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="ButtonSection ">
                  <div>
                    <div className="ViewCalulatedPrize">
                      <div className="ViewCalulatedPrize1122">
                        <span className="ViewCalulatedPrize111">
                          <input
                            type="checkbox"
                            id="vehicle1"
                            name="checked"
                            value={isSubscribed}
                            onChange={handleChange}
                          />
                          <label for="vehicle1">Six Position</label>
                          <br />
                        </span>
                        <span
                          onClick={calculatePrize}
                          className="ViewCalulatedPrize1"
                        >
                          Calculate Prize
                        </span>
                      </div>

                      {showCalculated ?

                        <table className="calcualtedPrizeTable">
                          <tr>
                            <th>First Prize</th>
                            <th>Second Prize</th>
                            <th>Third Prize</th>
                            <th>Fourth Prize</th>
                            <th>Fifth Prize</th>
                            <th>Six Prize</th>
                          </tr>
                          <tr>
                            <td>{FirstPrice}</td>
                            <td>{SecondPrice}</td>
                            <td>{ThirdPrice}</td>
                            <td>{FourthPrice}</td>
                            <td>{FifthPrice}</td>
                            <td>{SixthPrice}</td>
                          </tr>
                        </table> : <></>}
                    </div>
                  </div>
                  <div>
                    <button type="submit" className="SubmitButton">
                      Update Race
                    </button>
                    <button
                      type="submit"
                      className="SubmitButton"
                      onClick={GoToEditRace}
                    >
                      Update Horse
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <Modal
        show={showName}
        onHide={handleCloseName}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Race Name</h2>
        </Modal.Header>
        <Modal.Body>
          <Racename />
        </Modal.Body>
      </Modal>
      <Modal
        show={showType}
        onHide={handleCloseType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Meeting Type</h2>
        </Modal.Header>
        <Modal.Body>
          <MeetingTypePopUp />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceType}
        onHide={handleCloseRaceType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Race Type</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceTypePopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showTrackLength}
        onHide={handleCloseTrackLength}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Track Length</h2>
        </Modal.Header>
        <Modal.Body>
          <TrackLengthPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceKind}
        onHide={handleCloseRaceKind}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Race Kind</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceKindPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceCourse}
        onHide={handleCloseRaceCourse}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Create Race Course</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceCoursePopup />
        </Modal.Body>
      </Modal>

      <Modal
        show={showJockey}
        onHide={handleCloseJockey}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Create Jockey</h2>
        </Modal.Header>
        <Modal.Body>
          <JockeyPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showSponsor}
        onHide={handleCloseSponsor}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Sponsor</h2>
        </Modal.Header>
        <Modal.Body>
          <SponsorPopup />
        </Modal.Body>
      </Modal>

      <Modal
        show={showGroundType}
        onHide={handleCloseGroundType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Ground Type</h2>
        </Modal.Header>
        <Modal.Body>
          <GroundTypePopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showCondition}
        onHide={handleCloseTrackCondition}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Track Condition</h2>
        </Modal.Header>
        <Modal.Body>
          <TrackConditionPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showhorseKind}
        onHide={handleCloseHorseKind}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Horse Kind</h2>
        </Modal.Header>
        <Modal.Body>
          <HorseKindPopup />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default NewsForm;