import React, { useEffect, useRef } from "react";
import "react-toastify/dist/ReactToastify.css";
import { fetchjockey } from "../../../redux/getReducer/getJockeySlice";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchHorseForRace } from "../../../redux/getDropDownReducer/getHorseRace";
import { toast } from "react-toastify";
import Form from "react-bootstrap/Form";
import Select from "react-select";
import swal from "sweetalert";
import axios from "axios";
import { fetchColorDropdown } from "../../../redux/getDropDownReducer/getColor";
import { ImCross } from "react-icons/im";
import { TiTick } from "react-icons/ti";
import { fetchEditRaceHorse } from "../../../redux/getReducer/getEditHorseRaceSlice";
import { fetchequipmentdropdown } from "../../../redux/getDropDownReducer/getEquipment";
import { Modal, OverlayTrigger, Tooltip } from "react-bootstrap";
import { AiOutlineReload } from "react-icons/ai";
import HorsePopup from "../../PostTable/HorsePopupForm"
import JockeyPopup from "../../PostTable/JockeyPopupForm";
import ColorPopup from "../../PostTable/Color";
import EquipmentPopup from "../../PostTable/Equipment";
import { fetchsilkcolordropdown } from "../../../redux/getDropDownReducer/getOwnerSilkColor";

const AddMoreHorse = () => {
  const { data: jockey } = useSelector((state) => state.jockey);
  const { data: HorseForRace } = useSelector((state) => state.HorseForRace);
  const { data: equipmentdropdown } = useSelector(
    (state) => state.equipmentdropdown
  );
  const { data: colordropdown } = useSelector((state) => state.colordropdown);
  const { data: EditRaceHorse } = useSelector((state) => state.EditRaceHorse);
  const { data: silkcolordropdown } = useSelector((state) => state.silkcolordropdown);

  const [HorseData, SetHorseData] = useState("");

  const [Gate, setGate] = useState("");
  const [EquipmentData, SetEquipmentData] = useState("");
  const [JockeyData, SetJockeyData] = useState("");
  const [CapColor, SetCapColor] = useState("");
  const [HorseStatus, SetHorseStatus] = useState(true);
  const [items, setitems] = useState("");



  const [showHorse, setShowHorse] = useState(false)
  const [showJockey, setshowJockey] = useState(false)
  const [showColor, setshowColor] = useState(false)
  const [showEquipment, setshowEquipment] = useState(false)


  const H1 = EditRaceHorse.map((item) => item.HorseNo);
  const HighNum = Math.max(...H1);

  let IncreseData = HighNum + 1;
  const [HorseNo, setHorseNo] = useState(1);

  const [RaceWeight, setRaceWeight] = useState('');

  // const [HorseNo, setHorseNo] = useState(1);

  let formRef = useRef();
  const [StoreData, setStoreData] = useState([]);

  const history = useNavigate();
  const { state } = useLocation();
  const { RaceId } = state;

  let MapGate = [];
  for (var i = 1; i < 100; i++) {
    MapGate.push({ id: i, value: i, label: i });
  }

  const G1 = StoreData.map((item) => item.GateNo);
  const G2 = EditRaceHorse.map((item) => item.GateNo);

  let Gateoptions = MapGate.map(function (item) {
    return {
      id: item._id,
      value: item.value,
      label: item.value,
      isdisabled:
        G1[0] !== item.value &&
          G1[1] !== item.value &&
          G1[2] !== item.value &&
          G1[3] !== item.value &&
          G1[4] !== item.value &&
          G1[5] !== item.value &&
          G1[6] !== item.value &&
          G1[7] !== item.value &&
          G1[8] !== item.value &&
          G1[9] !== item.value &&
          G1[10] !== item.value &&
          G1[11] !== item.value &&
          G1[12] !== item.value &&
          G1[13] !== item.value &&
          G1[14] !== item.value &&
          G1[15] !== item.value &&
          G1[16] !== item.value &&
          G1[17] !== item.value &&
          G1[18] !== item.value &&
          G1[19] !== item.value &&
          G1[20] !== item.value &&
          G2[0] !== item.value &&
          G2[1] !== item.value &&
          G2[2] !== item.value &&
          G2[3] !== item.value &&
          G2[4] !== item.value &&
          G2[5] !== item.value &&
          G2[6] !== item.value &&
          G2[7] !== item.value &&
          G2[8] !== item.value &&
          G2[9] !== item.value &&
          G2[10] !== item.value &&
          G2[11] !== item.value &&
          G2[12] !== item.value &&
          G2[13] !== item.value &&
          G2[14] !== item.value &&
          G2[15] !== item.value &&
          G2[16] !== item.value &&
          G2[17] !== item.value &&
          G2[18] !== item.value &&
          G2[19] !== item.value &&
          G2[20] !== item.value
          ? false
          : true,
    };
  });

  const A1 = StoreData.map((item) => item.HorseId);
  const A2 = EditRaceHorse.map((item) => item.HorseModelId);

  let horseoptions = HorseForRace.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
      ActiveOwnerid:item.ActiveOwner === null ? <></> : item.ActiveOwner,
      Ownername:
      item.ActiveOwnerData === null ? <>NAN</> : item.ActiveOwnerData.NameEn,
            rating: item.STARS,
      isdisabled:
        A1[0] !== item._id &&
          A1[1] !== item._id &&
          A1[2] !== item._id &&
          A1[3] !== item._id &&
          A1[4] !== item._id &&
          A1[5] !== item._id &&
          A1[6] !== item._id &&
          A1[7] !== item._id &&
          A1[8] !== item._id &&
          A1[9] !== item._id &&
          A1[10] !== item._id &&
          A1[11] !== item._id &&
          A1[12] !== item._id &&
          A1[13] !== item._id &&
          A1[14] !== item._id &&
          A1[15] !== item._id &&
          A1[16] !== item._id &&
          A1[17] !== item._id &&
          A1[18] !== item._id &&
          A1[19] !== item._id &&
          A1[20] !== item._id &&
          A1[21] !== item._id &&
          A2[0] !== item._id &&
          A2[1] !== item._id &&
          A2[2] !== item._id &&
          A2[3] !== item._id &&
          A2[4] !== item._id &&
          A2[5] !== item._id &&
          A2[6] !== item._id &&
          A2[7] !== item._id &&
          A2[8] !== item._id &&
          A2[9] !== item._id &&
          A2[10] !== item._id &&
          A2[11] !== item._id &&
          A2[12] !== item._id &&
          A2[13] !== item._id &&
          A2[14] !== item._id &&
          A2[15] !== item._id &&
          A2[16] !== item._id &&
          A2[17] !== item._id &&
          A2[18] !== item._id &&
          A2[19] !== item._id &&
          A2[20] !== item._id &&
          A2[21] !== item._id
          ? false
          : true,
    };
  });

  

  const J1 = StoreData.map((item) => item.JockeyDataId);
  const J2 = EditRaceHorse.map((item) => item.JockeyOnRace);

  let AllJockey = jockey.map(function (item) {
    return {
      id: item._id,
      value: item._id,
      label: item.NameEn,
      weight: item.JockeyAllowance,
      minweight: item.MiniumumJockeyWeight,
      isdisabled:
        J1[0] !== item._id &&
          J1[1] !== item._id &&
          J1[2] !== item._id &&
          J1[3] !== item._id &&
          J1[4] !== item._id &&
          J1[5] !== item._id &&
          J1[6] !== item._id &&
          J1[7] !== item._id &&
          J1[8] !== item._id &&
          J1[9] !== item._id &&
          J1[10] !== item._id &&
          J1[11] !== item._id &&
          J1[12] !== item._id &&
          J1[13] !== item._id &&
          J1[14] !== item._id &&
          J1[15] !== item._id &&
          J1[16] !== item._id &&
          J1[17] !== item._id &&
          J1[18] !== item._id &&
          J1[19] !== item._id &&
          J1[20] !== item._id &&
          J2[0] !== item._id &&
          J2[1] !== item._id &&
          J2[2] !== item._id &&
          J2[3] !== item._id &&
          J2[4] !== item._id &&
          J2[5] !== item._id &&
          J2[6] !== item._id &&
          J2[7] !== item._id &&
          J2[8] !== item._id &&
          J2[9] !== item._id &&
          J2[10] !== item._id &&
          J2[11] !== item._id &&
          J2[12] !== item._id &&
          J2[13] !== item._id &&
          J2[14] !== item._id &&
          J2[15] !== item._id &&
          J2[16] !== item._id &&
          J2[17] !== item._id &&
          J2[18] !== item._id &&
          J2[19] !== item._id &&
          J2[20] !== item._id
          ? false
          : true,
    };
  });

  let AllEquipment = equipmentdropdown.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
    };
  });

  const dispatch = useDispatch();

  const [Value1, setValue1] = useState("");

  const handleChangeInput = (value) => {
    setValue1(value);
  };

  const slider = document.querySelector(".horizontal-scroll-wrapper");
  let isDown = false;
  let startX;
  let scrollLeft;
  if (slider) {
    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });

    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3;
      slider.scrollLeft = scrollLeft - walk;

    });
  }

  const HorseLength = HorseForRace.length;
  const ItemLength = items.length;
  const DataABC = JSON.parse(localStorage.getItem("mapping"));
  const AOwnerid = HorseData.ActiveOwnerid === '' ? <></> : HorseData.ActiveOwnerid

  useEffect(() => {
    formRef.current?.reset();
    dispatch(fetchHorseForRace({ RaceId, Value1 }));
    dispatch(fetchEditRaceHorse({ RaceId }));
    dispatch(fetchjockey());
    dispatch(fetchequipmentdropdown());
    dispatch(fetchsilkcolordropdown({AOwnerid}));
    localStorage.setItem("lists", JSON.stringify(items));
    localStorage.setItem("mapping", JSON.stringify(StoreData));
  }, [dispatch, items, StoreData, Value1, RaceId, AOwnerid]);

  const removesingleItem = (id) => {
    const updateItems = StoreData.filter((elem, ind) => {
      return ind !== id;
    });
    setStoreData(updateItems);
  };

  let AllsilkColor = silkcolordropdown.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      urlimg:item.OwnerSilkColor,
      label: (
        <img src={item.OwnerSilkColor} height="30px" width="30px" alt="" />

      ),
    };
  });
  const saveItem = (e) => {
    e.preventDefault();

    let HorseEntry = {
      GateNo: Gate === '' ? 100 : Gate.value,
      HorseNo: HorseNo,
      HorseModelId: HorseData.id,
      Equipment: EquipmentData.id,
      JockeyOnRace: JockeyData.id,
      JockeyWeight: JockeyData.weight,
      Rating: HorseData.rating,
      HorseRunningStatus: HorseStatus,
      CapColor: CapColor.id === '' ? null : CapColor.id,
      JockeyRaceWeight: RaceWeight === "" ? undefined : RaceWeight,
    };
    let MappingData = {
      GateNo: Gate.value,
      HorseNo: HorseNo,
      HorseName: HorseData.label,
      EquipmentName: EquipmentData.label,
      OwnerOnRace: HorseData.Ownername,
      JockeyOnRaceName: JockeyData.label,
      JockeyWeight: JockeyData.weight,
      MiniumumJockeyWeight: JockeyData.minweight,
      RaceWeight: RaceWeight,
      Rating: HorseData.rating,
      HorseRunningStatus: HorseStatus,
      CapColor: CapColor.urlimg,
      HorseId: HorseData.id,
      JockeyDataId: JockeyData.id,
    };

    if (HorseLength === ItemLength) {
      toast("No Horse ");
    } 
    else if(JockeyData === '' && HorseStatus === true ){
      toast("Select Jockey Value ");
    } 
    else if (
      HorseData === "" ||
      HorseNo === "" ||
      RaceWeight === ""
    ) {
      toast("Select Values ");
    } else {
      setitems([...items, HorseEntry]);
      setStoreData([...StoreData, MappingData]);
      setHorseNo(HorseNo + 1);
      formRef.current?.reset();
    }
    SetHorseStatus(true)
    setGate("");
    SetHorseData(" ");
    SetCapColor("");
    SetJockeyData("");
    SetEquipmentData("");
    setRaceWeight("");
  };
  const Remove = () => {
    setitems([]);
    setHorseNo(1);
    setStoreData([]);
  };

  const submit = async (event) => {
    event.preventDefault();
    if (ItemLength === 0) {
      toast("Please Add and Save Horse ");
    } else {
      try {
        await axios.post(`${window.env.API_URL}addracehorses/${RaceId}`, {
          HorseEntry: items,
        });
        localStorage.removeItem("lists");
        history("/editracehorse", {
          state: {
            RaceId: RaceId,
          },
        });

        swal({
          title: "Success",
          text: "Data has been added successfully ",
          icon: "success",
          button: "OK",
        });
      } catch (error) {
        const err = error.response.data.message;
        swal({
          title: "Error!",
          text: err,
          icon: "error",
          button: "OK",
        });
      }
    }
  };
  const fetchall = () => {
    dispatch(fetchHorseForRace({ RaceId, Value1 }));
    dispatch(fetchjockey());
    dispatch(fetchequipmentdropdown());
    dispatch(fetchsilkcolordropdown({AOwnerid}));
  }
  const handleshowHorse = async () => {
    await setShowHorse(true)
  }
  const handleshowJockey = async () => {
    await setshowJockey(true)
  }
  const handleshowColor = async () => {
    await setshowColor(true)
  }
  const handleshowEquipment = async () => {
    await setshowEquipment(true)
  }

  const handleCloseHorse = () => setShowHorse(false)

  const handleCloseColor = () => setshowColor(false)

  const handleCloseJockey = () => setshowJockey(false)

  const handleCloseEquipment = () => setshowEquipment(false)


  console.log(HorseStatus,'safdasfasf')
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Add More Horse</h4>
            </div>
            <div className="horizontal-scroll-wrapper squares">
              <div className="myselecthorse">
                <div className="myselecthorsedata">
                  <span>Horse #</span>
                  <span>Gate #</span>
                  <span>Horse</span>
                  <span>Owner Name</span>
                  <span>Jockey</span>

                  <span> Weight</span>

                  <span>Min Weight</span>
                  <span>Race Weight</span>
                  <span>Rating</span>
                  <span>Cap Color</span>
                  <span>Equipment</span>
                  <span>Horse Status</span>
                  <span>Action</span>
                </div>
              </div>
              <div className="myselectdata">
                <hr />
                {!DataABC ? (
                  <></>
                ) : (
                  DataABC.map((item, i) => {
                    return (
                      <div
                        className="myselectiondataaddHorse myselectiondataaddHorse2 mynew"
                        key={i}
                      >
                        <span className="inputAddhorse3">
                          <input  onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }} value={item.HorseNo} min="1" />
                        </span>
                        <span className="inputAddhorse4">
                          <span>
                            <input  onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }} value={item.GateNo} />
                          </span>
                        </span>
                        <span className="InputAddhorse5">
                          <input
                            placeholder="Horse Name"
                            value={item.HorseName}
                            readOnly
                          />
                        </span>
                        <span className="InputAddhorse5">
                          <input
                            placeholder="Owner"
                            value={item.OwnerOnRace}
                            readOnly
                            className="ownerinput"
                          />
                        </span>

                        <span className="InputAddhorse5">
                          <input
                            placeholder="Jockey Name"
                            value={item.JockeyOnRaceName}
                            readOnly
                            className="inputAddhorse"
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            placeholder="Weight"
                            value={item.JockeyWeight}
                            readOnly
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            placeholder="Min inputAddhorse3"
                            value={item.MiniumumJockeyWeight}
                            readOnly
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            placeholder="Race Weight"
                            value={item.RaceWeight}
                            readOnly
                          />
                        </span>
                        <span className="inputAddhorse3">
                          <input
                            type="text"
                            value={item.Rating}
                            readOnly
                            placeholder="Rating"
                          />
                        </span>
                        <span className="inputAddhorse4">
                        <img src={item.CapColor} height="30px" width="30px" alt="" />
                        </span>
                        <span className="inputAddhorse4">
                          <input
                            type="text"
                            value={item.EquipmentName}
                            readOnly
                            placeholder="Equipment"
                          />
                        </span>

                        <Form.Check
                          type="switch"
                          id="custom-switch"
                          // onChange={() => SetHorseStatus(true)}
                          // value={HorseStatus}
                          defaultChecked={item.HorseRunningStatus}
                        />
                        <button
                          className="removebtnhorse"
                          onClick={() => removesingleItem(i)}
                        >
                          <ImCross />
                        </button>
                      </div>
                    );
                  })
                )}
                <form
                  ref={formRef}
                  onSubmit={saveItem}
                  className="myselectiondataaddHorse  myselectiondataaddHorse1 mynew"
                >
                  <span className="inputAddhorse3">
                    <input
                       onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                      onChange={(event) => {
                        const value = Number(event.target.value);
                        setHorseNo(value);
                      }}
                      value={HorseNo}
                      min="1"
                      name="horseNo"
                    />
                  </span>
                  <span className="inputAddhorse4">
                    <Select
                      placeholder="Gate #"
                      className="dropdown multidropdown w-5"
                      onChange={setGate}
                      options={Gateoptions}
                      isSearchable={true}
                      isOptionDisabled={(option) => option.isdisabled}
                      hideSelectedOptions={true}
                      value={Gate}
                    />
                  </span>
                  <span className="InputAddhorse5">

                    <Select
                      id="selectNow"
                      placeholder="Horse"
                      className="dropdown multidropdown"
                      onChange={SetHorseData}
                      options={horseoptions}
                      isOptionDisabled={(option) => option.isdisabled}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      onInputChange={handleChangeInput}
                      value={HorseData}
                      name="HorseValue"
                    />
                    <span className="spanForm spanForm1">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore addmore1" onClick={handleshowHorse}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore addmore1" onClick={fetchall}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </span>
                  <span className="InputAddhorse5">
                    <input
                      placeholder="Owner"
                      value={HorseData.Ownername}
                      className="ownerinput"
                    />
                  </span>
                  <span className="InputAddhorse5">
                    <Select
                      placeholder="Jockey"
                      className="dropdown multidropdown"
                      onChange={SetJockeyData}
                      options={AllJockey}
                      isOptionDisabled={(option) => option.isdisabled}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      value={JockeyData}
                    />
                    <span className="spanForm spanForm1">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore addmore1" onClick={handleshowJockey}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore addmore1" onClick={fetchall}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      placeholder="Weight"
                      value={JockeyData.weight}
                      readOnly
                    />
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      placeholder="Weight"
                      value={JockeyData.minweight}
                      readOnly
                    />
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      placeholder=" Weight"
                      value={RaceWeight}
                      onChange={(e) => setRaceWeight(e.target.value)}
                      onKeyPress={(event) => {
                        if (!/^\d*\.?\d*$/
                        .test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </span>
                  <span className="inputAddhorse3">
                    <input
                      type="text"
                      value={HorseData.rating}
                      readOnly
                      placeholder="Rating"
                    />
                  </span>
                  <span className="inputAddhorse4">
                  <Select
                      placeholder="Color"
                      className="dropdown multidropdown"
                      onChange={SetCapColor}
                      options={AllsilkColor}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      value={CapColor}
                    />
                    <span className="spanForm spanForm1">
                      
                    </span>
                  </span>
                  <span className="inputAddhorse4">
                    <Select
                      placeholder="Equipment"
                      className="dropdown multidropdown"
                      onChange={SetEquipmentData}
                      options={AllEquipment}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      value={EquipmentData}
                    />
                    <span className="spanForm spanForm1">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore addmore1" onClick={handleshowEquipment}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore addmore1" onClick={fetchall}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </span>
                  <Form.Check
                    type="switch"
                    id="custom-switch"
                    onChange={() => SetHorseStatus(!HorseStatus)}
                    value={HorseStatus}
                    checked={HorseStatus}
                  />
                  <button className="savebtnhorse" onClick={saveItem}>
                    <TiTick />
                  </button>
                </form>
              </div>
            </div>
            <div className="sbmtbtndiv">
              <div className="RaceButtonDiv">
                <button className="updateButton" onClick={Remove}>
                  Remove
                </button>

                <button className="SubmitButton" type="submit" onClick={submit}>
                  Add Horses
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={showHorse} onHide={handleCloseHorse}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Horse</Modal.Title>
        </Modal.Header>
        <Modal.Body><HorsePopup /> </Modal.Body>

      </Modal>


      <Modal show={showJockey} onHide={handleCloseJockey}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Jockey</Modal.Title>
        </Modal.Header>
        <Modal.Body><JockeyPopup /> </Modal.Body>

      </Modal>
      <Modal show={showColor} onHide={handleCloseColor}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Color</Modal.Title>
        </Modal.Header>
        <Modal.Body><ColorPopup /> </Modal.Body>

      </Modal>
      <Modal show={showEquipment} onHide={handleCloseEquipment}

        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add New Equipment</Modal.Title>
        </Modal.Header>
        <Modal.Body><EquipmentPopup /> </Modal.Body>

      </Modal>
    </>
  );
};

export default AddMoreHorse;
