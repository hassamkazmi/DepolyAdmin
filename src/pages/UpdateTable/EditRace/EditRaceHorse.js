import React, { useEffect } from "react";
import "react-toastify/dist/ReactToastify.css";
import { fetchjockey } from "../../../redux/getReducer/getJockeySlice";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchHorseForRace } from "../../../redux/getDropDownReducer/getHorseRace";
import { fetchequipment } from "../../../redux/getReducer/getEquipment";
import { fetchEditRaceHorse } from "../../../redux/getReducer/getEditHorseRaceSlice";
import Form from "react-bootstrap/Form";
import Select from "react-select";
import swal from "sweetalert";
import axios from "axios";
import { fetchColorDropdown } from "../../../redux/getDropDownReducer/getColor";
import { fetchsilkcolordropdown } from "../../../redux/getDropDownReducer/getOwnerSilkColor";

import { ImCross } from "react-icons/im";
import { TiTick } from "react-icons/ti";

const EditRaceHorse = () => {
  const [InputData, SetinputData] = useState("");

  const [Gate, setGate] = useState("");
  const [EquipmentData, SetEquipmentData] = useState("");
  const [JockeyData, SetJockeyData] = useState("");

  const [CapColor, SetCapColor] = useState("");
  const [HorseStatus, SetHorseStatus] = useState(true);
  const [items, setitems] = useState([]);
  const [HorseData, SetHorseData] = useState("");

  const { data: EditRaceHorse } = useSelector((state) => state.EditRaceHorse);
  const { data: jockey } = useSelector((state) => state.jockey);
  const { data: HorseForRace } = useSelector((state) => state.HorseForRace);
  const { data: equipment } = useSelector((state) => state.equipment);
  const { data: colordropdown } = useSelector((state) => state.colordropdown);
  const { data: silkcolordropdown } = useSelector((state) => state.silkcolordropdown);

  const [SearchAge, setSearchAge] = useState("");
  const [SearchCode, setSearchCode] = useState("");
  const [SearchTitle, setSearchTitle] = useState("");
  const [SearchNameEn, setSearchNameEn] = useState("");
  const [SearchRating, setSearchRating] = useState("");

  const [RaceWeight, setRaceWeight] = useState("");
  const history = useNavigate();
  const { state } = useLocation();
  const { RaceId } = state;

  const [StoreData, setStoreData] = useState([]);

  const slider = document.querySelector(".horizontal-scroll-wrapper");
  let isDown = false;
  let startX;
  let scrollLeft;
  if (slider) {
    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });

    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3;
      slider.scrollLeft = scrollLeft - walk;
    });
  }

  const H1 = EditRaceHorse.map((item) => item.HorseNo);
  const HighNum = Math.max(...H1);
  let IncreseData = HighNum + 1;
  const [HorseNo, setHorseNo] = useState(1);

  let MapGate = [];
  for (var i = 1; i < 100; i++) {
    MapGate.push({ id: i, value: i, label: i });
  }

  const A1 = EditRaceHorse.map((item) => item.HorseModelId);

  let horseoptions = HorseForRace.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
      ActiveOwnerid:item.ActiveOwner === null ? <></> : item.ActiveOwner,
      Ownername:
      item.ActiveOwnerData === null ? <>NAN</> : item.ActiveOwnerData.NameEn,
      rating: item.STARS,
      isdisabled:
        A1[0] !== item._id &&
          A1[1] !== item._id &&
          A1[2] !== item._id &&
          A1[3] !== item._id &&
          A1[4] !== item._id &&
          A1[5] !== item._id &&
          A1[6] !== item._id &&
          A1[7] !== item._id &&
          A1[8] !== item._id &&
          A1[9] !== item._id &&
          A1[10] !== item._id &&
          A1[11] !== item._id &&
          A1[12] !== item._id &&
          A1[13] !== item._id &&
          A1[14] !== item._id &&
          A1[15] !== item._id &&
          A1[16] !== item._id &&
          A1[17] !== item._id &&
          A1[18] !== item._id &&
          A1[19] !== item._id &&
          A1[20] !== item._id &&
          A1[21] !== item._id
          ? false
          : true,
    };
  });

  const G1 = EditRaceHorse.map((item) => item.GateNo);

  let Gateoptions = MapGate.map(function (item) {
    return {
      id: item._id,
      value: item.value,
      label: item.value,
      isdisabled:
        G1[0] !== item.value &&
          G1[1] !== item.value &&
          G1[2] !== item.value &&
          G1[3] !== item.value &&
          G1[4] !== item.value &&
          G1[5] !== item.value &&
          G1[6] !== item.value &&
          G1[7] !== item.value &&
          G1[8] !== item.value &&
          G1[9] !== item.value &&
          G1[10] !== item.value &&
          G1[11] !== item.value &&
          G1[12] !== item.value &&
          G1[13] !== item.value &&
          G1[14] !== item.value &&
          G1[15] !== item.value &&
          G1[16] !== item.value &&
          G1[17] !== item.value &&
          G1[18] !== item.value &&
          G1[19] !== item.value &&
          G1[20] !== item.value
          ? false
          : true,
    };
  });

  let coloroption = colordropdown.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
    };
  });

  const J1 = EditRaceHorse.map((item) => item.JockeyOnRace);

  let AllJockey = jockey.map(function (item) {
    return {
      id: item._id,
      value: item._id,
      label: item.NameEn,
      weight: item.JockeyAllowance,
      minweight: item.MiniumumJockeyWeight,
      isdisabled:
        J1[0] !== item._id &&
          J1[1] !== item._id &&
          J1[2] !== item._id &&
          J1[3] !== item._id &&
          J1[4] !== item._id &&
          J1[5] !== item._id &&
          J1[6] !== item._id &&
          J1[7] !== item._id &&
          J1[8] !== item._id &&
          J1[9] !== item._id &&
          J1[10] !== item._id &&
          J1[11] !== item._id &&
          J1[12] !== item._id &&
          J1[13] !== item._id &&
          J1[14] !== item._id &&
          J1[15] !== item._id &&
          J1[16] !== item._id &&
          J1[17] !== item._id &&
          J1[18] !== item._id &&
          J1[19] !== item._id &&
          J1[20] !== item._id
          ? false
          : true,
    };
  });

  let AllEquipment = equipment.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      label: item.NameEn,
    };
  });

  const dispatch = useDispatch();
  const AOwnerid = HorseData.ActiveOwnerid === '' ? <></> : HorseData.ActiveOwnerid

  useEffect(() => {
    dispatch(fetchHorseForRace({ RaceId }));
    dispatch(fetchEditRaceHorse({ RaceId }));
    dispatch(fetchsilkcolordropdown({AOwnerid}));
    dispatch(fetchjockey({ SearchRating, SearchNameEn }));
    dispatch(fetchequipment({ SearchTitle, SearchCode, SearchAge }));
    dispatch(fetchColorDropdown());
    setHorseNo(IncreseData);
  }, [
    IncreseData,
    RaceId,
    SearchAge,
    SearchCode,
    SearchNameEn,
    SearchRating,
    SearchTitle,
    AOwnerid,
    dispatch,
  ]);

  let AllsilkColor = silkcolordropdown.map(function (item) {
    return {
      id: item._id,
      value: item.NameEn,
      urlimg:item.OwnerSilkColor,
      label: (
        <img src={item.OwnerSilkColor} height="30px" width="30px" alt="" />

      ),
    };
  });

  useEffect(() => {
    localStorage.setItem("lists", JSON.stringify(items));
    localStorage.setItem("mapping", JSON.stringify(StoreData));
  }, [items, InputData, StoreData]);

  const saveEditItem = async (event, data) => {
    event.preventDefault();
    const formData = new FormData();
    let a = {
      GateNo: undefined,
      HorseNo: undefined,
      HorseModelId: undefined,
      Equipment: undefined,
      JockeyOnRace: undefined,
      JockeyWeight: undefined,
      Rating: undefined,
      HorseRunningStatus: undefined,
      CapColor: undefined,
      JockeyRaceWeight: undefined,
    };
    // eslint-disable-next-line array-callback-return
    EditRaceHorse.map((singleentry) => {
      if (singleentry._id === data) {
        a.GateNo = singleentry.GateNo;
        a.HorseModelId = singleentry.HorseModelId;
        a.Equipment = singleentry.Equipment;
        a.HorseNo = singleentry.HorseNo;
        a.JockeyOnRace = singleentry.JockeyOnRace;
        a.JockeyWeight = singleentry.JockeyWeight;
        a.GateRatingNo = singleentry.GateRatingNo;
        a.HorseRunningStatus = singleentry.HorseRunningStatus;
        a.CapColor = singleentry.CapColor;
        a.JockeyRaceWeight = singleentry.JockeyRaceWeight;
      }
    });
    formData.append("GateNo", Gate.value === undefined ? a.GateNo : Gate.value);
    formData.append("HorseNo", a.HorseNo);
    formData.append(
      "HorseModelId",
      HorseData.id === undefined ? a.HorseModelId : HorseData.id
    );
    formData.append(
      "JockeyRaceWeight",
      RaceWeight === undefined ? a.JockeyRaceWeight : RaceWeight
    );
    if (EquipmentData.id !== undefined) {
      formData.append(
        "Equipment",
        EquipmentData.id === undefined ? a.Equipment : EquipmentData.id
      );

    }
    formData.append(
      "JockeyOnRace",
      JockeyData.id === undefined ? a.JockeyOnRace : JockeyData.id
    );
    formData.append(
      "JockeyWeight",
      JockeyData.weight === undefined ? a.JockeyWeight : JockeyData.weight
    );
    formData.append(
      "Rating",
      JockeyData.Rating === undefined ? 2 : JockeyData.Rating
    );
    formData.append(
      "HorseRunningStatus",
      HorseStatus === undefined ? a.HorseRunningStatus : HorseStatus
    );
    if (CapColor.id !== undefined) {
      formData.append(
        "CapColor",
        CapColor.id === undefined ? a.CapColor : CapColor.id
      );
    }
   
    formData.append("Rowid", data);

    try {
      await axios.put(
        `${window.env.API_URL}EditRaceHorses/${RaceId}`,
        formData
      );
      localStorage.removeItem("lists");
      setGate(1);

      swal({
        title: "Success",
        text: "Data has been added successfully ",
        icon: "success",
        button: "OK",
      });
      dispatch(fetchEditRaceHorse({ RaceId }));
      // window.location.reload();
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

  const handleRemove = async (Id) => {
    try {
      swal({
        title: "Are you sure?",
        text: "do you want to delete this data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then(async (willDelete) => {
        if (willDelete) {
          await axios.delete(`${window.env.API_URL}/DeleteRaceHorse/${Id}`);
          swal("Your data has been deleted Successfully!", {
            icon: "success",
          });
          dispatch(fetchEditRaceHorse({ RaceId }));
        } else {
          swal("Your data is safe!");
        }
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

  const addMore = (e) => {
    history("/editmorehorse", {
      state: {
        RaceId: RaceId,
      },
    });
  };

  console.log(HorseStatus, 'Horse Status');
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Header ">
              <h4>Edit Horse</h4>
              <button className="AddAnother1" onClick={addMore}>
                Add More Horse
              </button>
            </div>

            <div className="horizontal-scroll-wrapper squares">
              <div className="myselecthorse">
                <div className="myselecthorsedata">
                  <span>Horse #</span>
                  <span>Gate #</span>
                  <span>Horse</span>
                  <span>Owner Name</span>
                  <span>Jockey</span>
                  <span> Weight</span>
                  <span>Min Weight</span>
                  <span>Race Weight</span>
                  <span>Rating</span>
                  <span>Cap Color</span>
                  <span>Equipment</span>
                  <span>Horse Status</span>
                  <span>Action</span>
                </div>
              </div>

              <div className="myselectdata myselectiondataaddHorse1 setHeight">
                <>
                  {!EditRaceHorse ? (
                    <></>
                  ) : (
                    EditRaceHorse.map((item, i) => {
                      return (
                        <div className="myselectiondataaddHorse  ">
                          <span className="myedit">
                            <span className="inputAddhorse3">
                              <input
                                 onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                                value={item.HorseNo}
                                min="1"
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse4">
                              <Select
                                placeholder={
                                  item.GateNo === 100 ? (
                                    <>Non Runner</>
                                  ) : (
                                    item.GateNo
                                  )
                                }
                                className="dropdown multidropdown"
                                defaultValue={Gate}
                                onChange={setGate}
                                options={Gateoptions}
                                isOptionDisabled={(option) => option.isdisabled}
                                isSearchable={true}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="InputAddhorse5">
                              <Select
                                placeholder={item.HorseModelIdData1 ? item.HorseModelIdData1.NameEn : <></>}
                                className="dropdown multidropdown"
                                defaultValue={HorseData}
                                onChange={SetHorseData}
                                options={horseoptions}
                                isOptionDisabled={(option) => option.isdisabled}
                                isSearchable={true}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="InputAddhorse5">
                              <input
                                placeholder={item.OwnerOnRaceData1 ? item.OwnerOnRaceData1.NameEn : <></>}
                                value={item.OwnerOnRaceData1 ? item.OwnerOnRaceData1.NameEn : <></>}
                                readOnly
                                style={{ width: "120px" }}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="InputAddhorse5">
                              <Select
                                placeholder={
                                  item.JockeyOnRaceData1 === null ? (
                                    <>Non Runner</>
                                  ) : (
                                    item.JockeyOnRaceData1.NameEn
                                  )
                                }
                                className="dropdown multidropdown"
                                defaultValue={JockeyData}
                                onChange={SetJockeyData}
                                options={AllJockey}
                                isOptionDisabled={(option) => option.isdisabled}
                                isSearchable={true}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse3">
                              <input
                                placeholder={
                                  item.JockeyWeight === null
                                    ? 0
                                    : item.JockeyWeight
                                }
                                value={item.JockeyWeight}
                                readOnly
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse3">
                              <input
                                placeholder={
                                  item.JockeyWeight === null
                                    ? 0
                                    : item.JockeyWeight
                                }
                                value={item.minweight}
                                readOnly
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse3">
                              <input
                                placeholder={item.JockeyRaceWeight}
                                onChange={(e) => setRaceWeight(e.target.value)}
                                onKeyPress={(event) => {
                                  if (!/^\d*\.?\d*$/
                                  .test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse3">
                              <input
                                type={item.Rating}
                                value={item.Rating}
                                readOnly
                                placeholder="Rating"
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse4">
                              <Select
                                placeholder={item.CapColorData1 ? item.CapColorData1.NameEn : <></>}
                                className="dropdown multidropdown"
                                defaultValue={CapColor}
                                onChange={SetCapColor}
                                options={AllsilkColor}
                                isSearchable={true}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <span className="inputAddhorse4">
                              <Select
                                placeholder={
                                  item.EquipmentData1 === null ? (
                                    <>-</>
                                  ) : (
                                    item.EquipmentData1.NameEn
                                  )
                                }
                                className="dropdown multidropdown"
                                defaultValue={EquipmentData}
                                onChange={SetEquipmentData}
                                options={AllEquipment}
                                isSearchable={true}
                              />
                            </span>
                          </span>

                          <span className="myedit">
                            <Form.Check
                              type="switch"
                              id="custom-switch"
                              onChange={() => SetHorseStatus(!item.HorseRunningStatus)}
                              value={HorseStatus}
                              defaultChecked={item.HorseRunningStatus}
                            />
                          </span>
                          <span className="myedit">
                            <button
                              className="savebtnhorse"
                              onClick={(e) => saveEditItem(e, item._id)}
                            >
                              <TiTick />
                            </button>

                            <button
                              className="removebtnhorse"
                              onClick={() => handleRemove(item._id)}
                            >
                              <ImCross />
                            </button>
                          </span>
                        </div>
                      );
                    })
                  )}
                </>

                <div className="sbmtbtndiv"></div>
              </div>
            </div>
            <div className="RaceButtonDiv">
              {/* <button className="updateButton" onClick={submit}>
                      Add
                    </button> */}

              <button
                className="SubmitButton"
                type="submit"
                onClick={() =>
                  history("/editraceverdict", {
                    state: {
                      RaceId: RaceId,
                    },
                  })
                }
              >
                Edit Verdict
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditRaceHorse;
