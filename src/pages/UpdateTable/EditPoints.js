import React, { useState, useEffect, Fragment } from "react";
import "../../Components/CSS/forms.css";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";

const EditPoints = () => {
  const history = useNavigate();
  const { state } = useLocation();

  const { pointid } = state;


  const [state1, setState] = useState({
    Group_Name: '',
    Rank: '',
    Bonus_Point: '',
    Point: '',
    shortCode: '',

  });
  //---------------------------get Data from previous state---------------------------//

  console.log(pointid,'pointid')

  useEffect(() => {
    if (pointid) {
      setState({
        Group_Name: pointid.Group_NameDataOfCompetition ? pointid.Group_NameDataOfCompetition.NameEn : <>N/A</>,
        Rank: pointid.Rank,
        shortCode: pointid.shortCode,
        Bonus_Point: pointid.Bonus_Point,
        Point: pointid.Point,

      });
    } else {
      alert('No Data')
    }
  }, [pointid]);

  //---------------------------Submit---------------------------//

  const submit = async (event) => {
    event.preventDefault();
    try {

      const formData = new FormData();
      formData.append("Group_Name", state1.Group_Name);
      formData.append("Rank", state1.Rank);
      formData.append("Bonus_Point", state1.Bonus_Point);
      formData.append("Point", state1.Point);
      formData.append("shortCode", state1.shortCode);

      await axios.put(`${window.env.API_URL}/updatePointTableSystem/${pointid._id}`, formData);
      history("/viewcompetitionPoint");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {

      swal({
        title: "Error!",
        text: error,
        icon: "error",
        button: "OK",
      });
    }
  };
  return (
    <Fragment>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Create Point Table</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Point Group Name"
                      className="mb-3"
                      name="Name"
                      onChange={(e) =>
                        setState({ ...state1, Group_Name: e.target.value })
                      }

                    >
                      <Form.Control
                        required

                        name="Group_Name"
                        type="text"
                        placeholder="Group_Name"
                        value={state1.Group_Name}

                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Rank"
                      className="mb-3"
                      name="BonusPoints"
                      onChange={(e) =>
                        setState({ ...state1, Rank: e.target.value })
                      }

                    >
                      <Form.Control
                        name="BonusPoints"

                        value={state1.Rank}
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        placeholder="BonusPoints"
                        required

                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label=" Point"
                      className="mb-3"
                      name="Name"
                      onChange={(e) => setState({ ...state1, Point: e.target.value })}
                    >
                      <Form.Control
                        required
                        value={state1.Point}

                        name="Type"
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        placeholder="Name"
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Bonus Point"
                      className="mb-3"
                      name="Name"
                      onChange={(e) => {
                        setState({ ...state, Bonus_Point: e.target.value })
                      }}
                    >
                      <Form.Control


                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        placeholder="Length"
                        value={state1.Bonus_Point}
                        required

                      />
                    </FloatingLabel>
                  </div>
                </div>


                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Short Code"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, shortCode: e.target.value })
                      }

                    >
                      <Form.Control  onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }} placeholder="Description" value={state1.shortCode} />
                    </FloatingLabel>


                  </div>
                </div>

                <div
                  className="ButtonSection "
                  style={{ justifyContent: "end" }}
                >
                  <button Name="submit" className="SubmitButton" >
                    Add Point Table
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div >
    </Fragment >
  )
}

export default EditPoints