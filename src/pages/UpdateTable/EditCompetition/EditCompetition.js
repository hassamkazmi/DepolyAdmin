import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import DatePicker from "react-date-picker";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";

const EditComptetion = () => {
  const history = useNavigate();
  const { state } = useLocation();

  const { competitionid } = state;

  const [StartDate, setStartDate] = useState("");
  const [EndDate, setEndDate] = useState("");

  const [state1, setState] = useState({
    NameAr: "",
    NameEn: "",
    DescEn: "",
    DescAr: "",
    CategoryCount: "",
    CompetitionCategory: "",
    StartDate: "",
    EndDate: " ",
    CompetitionCode: "",
    shortCode: "",
  });

  //----------------------------get Data -------------------------//
  useEffect(() => {
    if (competitionid) {
      setState({
        NameEn: competitionid.NameEn,
        NameAr: competitionid.NameAr,
        DescEn: competitionid.DescEn,
        DescAr: competitionid.DescAr,
        CategoryCount: competitionid.CategoryCount,
        StartDate: competitionid.StartDate,
        CompetitionCode: competitionid.CompetitionCode,
        shortCode: competitionid.shortCode,
        CompetitionCategory: competitionid.CompetitionCategory,
        EndDate: competitionid.EndDate,
      });
      
      let day123 = new Date(competitionid.StartDate);
      let daydate = day123.getDate();
      if (daydate < 10) {
        daydate = `0${day123.getDate()}`;
      }
      let daymonth = day123.getMonth() + 1;
      if (daymonth < 10) {
        daymonth = `0${day123.getMonth() + 1}`;
      }
      let dayformat = `${day123.getFullYear()}-${daymonth}-${daydate}`;
      console.log(dayformat, "dayformat");

      setStartDate(dayformat);


      let day456 = new Date(competitionid.EndDate);
      let daydate1 = day456.getDate();
      if (daydate1 < 10) {
        daydate1 = `0${day456.getDate()}`;
      }
      let daymonth1 = day456.getMonth() + 1;
      if (daymonth1 < 10) {
        daymonth1 = `0${day456.getMonth() + 1}`;
      }
      let dayformat1 = `${day456.getFullYear()}-${daymonth1}-${daydate1}`;
      console.log(dayformat1, "dayformat");

      setEndDate(dayformat1);
    } else {
    }
  }, [competitionid]);
  //----------------------------submit -------------------------//

  const submit = async (event) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("NameEn", state1.NameEn);
      formData.append("NameAr", state1.NameAr);
      formData.append("DescEn", state1.DescEn);
      formData.append("DescAr", state1.DescAr);
      formData.append("CategoryCount", state1.CategoryCount);
      formData.append("CompetitionCategory", state1.CompetitionCategory);
      formData.append("StartDate", StartDate);
      formData.append("EndDate", EndDate);
      formData.append("CompetitionCode", state1.CompetitionCode);
      formData.append("shortCode", state1.shortCode);

      await axios.put(
        `${window.env.API_URL}/updateCompetiton/${competitionid._id}`,
        formData
      );
      // history("/competitionrace", {
      //   state: {
      //     CompetitionId: competitionid._id,
      //   },
      // });
      history("/competitionlisting");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };


  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Competition</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Name"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, NameEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.NameEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, NameAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.NameAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label=" Count"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, CategoryCount: e.target.value })
                      }
                    >
                      <Form.Control
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        min="1"
                        max="12"
                        placeholder="Description"
                        value={state1.CategoryCount}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="تريكونت"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, CategoryCount: e.target.value })
                      }
                    >
                      <Form.Control
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        placeholder="Description"
                        value={state1.CategoryCount}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="dateLabel">Start Date</p>
                  <input type='date' data-placeholder="Start Date" onChange={(e) => setStartDate(e.target.value)}
                    value={StartDate} className='dateforrace' />
                  {/* <DatePicker
                    onChange={setStartDate}
                    value={StartDate}
                    dayPlaceholder=" "
                    monthPlaceholder={state1.StartDate}
                    yearPlaceholder=""
                  /> */}
                </div>
                <div className="row mainrow">
                  <p className="dateLabel">End Date</p>

                  <input type='date' data-placeholder=" End Date" onChange={(e) => setEndDate(e.target.value)}
                    value={EndDate} className='dateforrace' />
                  {/* <DatePicker
                    onChange={setEndDate}
                    value={EndDate}
                    dayPlaceholder=" "
                    monthPlaceholder={state1.EndDate}
                    yearPlaceholder=""
                  /> */}
                  {/* <span className="spanForm"> |</span> */}
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Competition Code"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, CompetitionCode: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.CompetitionCode}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Short Code"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, shortCode: e.target.value })
                      }
                    >
                      <Form.Control
                         onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                        placeholder="Details"
                        value={state1.shortCode}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div
                  className="ButtonSection"
                  style={{ justifyContent: "end" }}
                >
                  <button type="submit" className="SubmitButton">
                    Update
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditComptetion;
