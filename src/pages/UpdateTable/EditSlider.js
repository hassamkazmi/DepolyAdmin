import React, { useState, useEffect } from "react";
import "../../Components/CSS/forms.css";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import { ImCross } from "react-icons/im";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
const EditSlider = () => {
  const history = useNavigate();
  const { state } = useLocation();

  const { sliderid } = state;

  const [image, setImage] = useState();
  const [preview, setPreview] = useState();

  const [state1, setState] = useState({
    TitleEn: "",
    TitleAr: "",
    Url: "",
    image: image,
  });
  //---------------------------get data from previous state---------------------------//
  useEffect(() => {
    if (sliderid) {
      setState({
        TitleEn: sliderid.TitleEn,
        TitleAr: sliderid.TitleAr,
        Url: sliderid.Url,
        image: sliderid.image,
      });
    } else {
      alert("No Data");
    }
  }, [sliderid]);
  //---------------------------Image Preview---------------------------//
  useEffect(() => {
    if (image === undefined) {
      setPreview(sliderid.image);
      return;
    }
    const objectUrl = URL.createObjectURL(image);
    setPreview(objectUrl);
    return () => URL.revokeObjectURL(objectUrl);
  }, [image, sliderid.image]);
  const fileSelected = (event) => {
    const image = event.target.files[0];
    setImage(image, image);
  };
  const handlePreview = () => {
    setImage();
    setPreview();
    document.getElementById("file").value = "";
  };
  //---------------------------submit---------------------------//
  const submit = async (event) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("TitleEn", state1.TitleEn);
      formData.append("TitleAr", state1.TitleAr + " ");
      formData.append("Url", state1.Url);
      formData.append("image", state1.image);

      await axios.put(
        `${window.env.API_URL}/updateSlider/${sliderid._id}`,
        formData
      );
      history("/slider");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Slider</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Title"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, TitleEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.TitleEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, TitleAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.TitleAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="URL"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, Url: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.Url}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="ButtonSection">
                  <div>
                    <label className="Multipleownerlabel">
                      Select Slider image
                    </label>
                    <input
                      type="file"
                      onChange={fileSelected}
                      className="formInput"
                      id="file"
                    />
                    {preview && (
                      <>
                        <ImCross
                          onClick={handlePreview}
                          className="crossIcon"
                        />
                        <img src={preview} className="PreviewImage" alt="" />
                      </>
                    )}
                  </div>
                  <div>
                    <button type="submit" className="SubmitButton">
                      Update
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditSlider;
