import React, { useState, useEffect } from "react";
import "../../Components/CSS/forms.css";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import { fetchgroundtype } from "../../redux/getReducer/getGroundType";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import { Modal } from "react-bootstrap";
import { AiOutlineReload } from "react-icons/ai";
import RaceCoursePopup from "../PostTable/RaceCourseForm";
import GroundTypePopup from "../PostTable/GroundType";

import { ImCross } from "react-icons/im";
import { fetchracecourse } from "../../redux/getReducer/getRaceCourseSlice";

const EditTrack = () => {
  const dispatch = useDispatch();
  const history = useNavigate();
  const { state } = useLocation();

  const [showRaceCourse, setShowRaceCourse] = useState(false);
  const [Racecourse, setRaceCourse] = useState()

  const { data: groundtype } = useSelector((state) => state.groundtype);
  const { data: racecourse } = useSelector((state) => state.racecourse);

  const [showGroundType, setShowGroundType] = useState(false);

  //---------------------------show Popups---------------------------//

  const handleCloseGroundType = () => setShowGroundType(false);
  const handleCloseActiveRaceCourse = () => setShowRaceCourse(false);

  const handleShowGroundType = async () => {
    await setShowGroundType(true);
  };

  const handleShowRaceCourse = async () => {
    await setShowRaceCourse(true);
  };
  //---------------------------options for dropdown---------------------------//

  let groundtypeopt =
    groundtype === undefined ? (
      <></>
    ) : (
      groundtype.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  let racecourses =
    racecourse === undefined ? (
      <></>
    ) : (
      racecourse.map(function (item) {
        return {
          id: item._id,
          value: item.TrackNameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.TrackNameEn}</p>
              <p>{item.TrackNameAr}</p>
            </div>
          ),
        };
      })
    );
  //---------------------------re fetch Api---------------------------//
  const FetchNew = () => {
    dispatch(fetchgroundtype());
  };
  const { trackid } = state;
  const [image, setImage] = useState();
  const [preview, setPreview] = useState();
  const [GroundType, setGroundType] = useState();

  const [state1, setState] = useState({
    TrackLength: "",
    RailPosition: "",
    Racecourse: "",
    GroundType: "",
    image: image,
  });
  //---------------------------get data from previous state---------------------------//
  useEffect(() => {
    if (trackid) {
      setState({
        TrackLength: trackid.TrackLength,
        RailPosition: trackid.RailPosition,
        RaceCourse: trackid.RaceCourse,
        GroundType: trackid.GroundType,
        image: trackid.image,
      });
    } else {
      alert("No Data");
    }
  }, [trackid]);

  const fileSelected = (event) => {
    const image = event.target.files[0];
    setImage(image);
  };
  //---------------------------submit---------------------------//
  const submit = async (event) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("RailPosition", state1.RailPosition);
      formData.append("TrackLength", state1.TrackLength);


      formData.append("RaceCourse", Racecourse.id === undefined ? state1.Racecourse : Racecourse.id)
      formData.append("image", image);

      formData.append(
        "GroundType",
        GroundType.id === undefined ? state1.GroundType : GroundType.id
      );

      await axios.put(
        `${window.env.API_URL}/updateTrackLength/${trackid._id}`,
        formData
      );
      history("/tracklength");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message[0];
      const err1 = error.response.data.message[1];
      const err2 = error.response.data.message[2];
      swal({
        title: "Error!",
        text: err,
        err1,
        err2,
        icon: "error",
        button: "OK",
      });
    }
  };
  const handlePreview = () => {
    fetchracecourse()
    setImage();
    setPreview();
    document.getElementById("file").value = "";
  };
  useEffect(() => {
    if (image === undefined) {
      setPreview(trackid.image);
      return;
    }
    const objectUrl = URL.createObjectURL(image);
    setPreview(objectUrl);
    return () => URL.revokeObjectURL(objectUrl);
  }, [image, trackid.image]);
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Track Length</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Track Length"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, TrackLength: e.target.value })
                      }
                    >
                      <Form.Control
                        onKeyPress={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        placeholder="Description"
                        value={state1.TrackLength}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <p className="selectLabel">Ground Type </p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>{trackid.GroundTypeModelData.NameEn}</div>
                      }
                      defaultValue={GroundType}
                      onChange={setGroundType}
                      options={groundtypeopt}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowGroundType}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Rail Position"
                      className="mb-3 "
                      onChange={(e) =>
                        setState({ ...state1, RailPosition: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.RailPosition}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Race Course </p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{trackid.TrackLengthRaceCourseData ? trackid.TrackLengthRaceCourseData.TrackNameEn : <></>}</div>}
                      defaultValue={Racecourse}
                      onChange={setRaceCourse}
                      options={racecourses}
                      isClearable={true}
                      isSearchable={true}

                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowRaceCourse}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>

                  </div>

                </div>

                <div className="ButtonSection">
                  <div>
                    <label className="Multipleownerlabel">
                      Select Track Gif
                    </label>
                    <input
                      type="file"
                      onChange={fileSelected}
                      className="formInput"
                      id="file"
                    />
                    {preview && (
                      <>
                        <ImCross
                          onClick={handlePreview}
                          className="crossIcon"
                        />
                        <img src={preview} className="PreviewImage" alt="" />
                      </>
                    )}
                  </div>

                  <button type="submit" className="SubmitButton">
                    Update
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={showGroundType}
        onHide={handleCloseGroundType}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Ground Type</h2>
        </Modal.Header>
        <Modal.Body>
          <GroundTypePopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showRaceCourse}
        onHide={handleCloseActiveRaceCourse}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>RaceCourse</h2>
        </Modal.Header>
        <Modal.Body>
          <RaceCoursePopup />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default EditTrack;
