import React, { useState, useEffect } from "react";
import "../../Components/CSS/forms.css";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import Select from "react-select";
import { ImCross } from 'react-icons/im';
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";

//---------------------------options for dropdowns---------------------------//

const Hemisphere = [
  {
    id: "1", value: "Southern Hemisphere", label: (
      <div style={{
        display: 'flex',
        justifyContent: 'space-between'
      }}>
        <p>Southern Hemisphere</p>
        <p>نصف الكرة الجنوبي</p>

      </div>
    ),
  },
  {
    id: "2", value: "Northern Hemisphere", label: (
      <div style={{
        display: 'flex',
        justifyContent: 'space-between'
      }}>
        <p>Northern Hemisphere</p>
        <p>نصف الكرة الشمالي</p>

      </div>
    ),
  },
];
const EditNationality = () => {

  const history = useNavigate();
  const { state } = useLocation();
  const [HemisphereEn, setHemisphereEn] = useState("");

  const { nationalityid } = state;

  const [image, setImage] = useState();
  const [preview, setPreview] = useState();

  const [state1, setState] = useState({
    NameEn: '',
    NameAr: '',
    shortCode: '',
    AltNameEn: '',
    AltNameAr: '',
    AbbrevAr: '',
    AbbrevEn: '',
    HemisphereEn: "",
    Offset: '',
    ValueEn: '',
    ValueAr: '',
    image: image

  });

  const fileSelected = (event) => {
    const image = event.target.files[0];
    setImage(image, image);
  };

  //---------------------------get Data from previous state---------------------------//

  useEffect(() => {
    if (nationalityid) {
      setState({
        NameEn: nationalityid.NameEn,
        NameAr: nationalityid.NameAr,
        shortCode: nationalityid.shortCode,
        AltNameEn: nationalityid.AltNameEn,
        AltNameAr: nationalityid.AltNameAr,
        AbbrevAr: nationalityid.AbbrevAr,
        AbbrevEn: nationalityid.AbbrevEn,
        Offset: nationalityid.Offset,
        ValueEn: nationalityid.ValueEn,
        ValueAr: nationalityid.ValueAr,
        HemisphereEn: nationalityid.HemisphereEn,
        image: nationalityid.image
      });
    } else {
    }
  }, [nationalityid]);
  //---------------------------Image Preview---------------------------//

  useEffect(() => {
    if (image === undefined) {
      setPreview(nationalityid.image)
      return
    }
    const objectUrl = URL.createObjectURL(image)
    setPreview(objectUrl)
    return () => URL.revokeObjectURL(objectUrl)
  }, [image, nationalityid.image])
  const handlePreview = () => {
    setImage()
    setPreview()
    document.getElementById("file").value = ""
  };
  //---------------------------submit---------------------------//

  const submit = async (event) => {
    event.preventDefault();
    try {

      const formData = new FormData();
      formData.append("image", image);
      formData.append("NameEn", state1.NameEn);
      formData.append("NameAr", state1.NameAr);
      formData.append("shortCode", state1.shortCode);
      formData.append("AltNameEn", state1.AltNameEn);
      formData.append("AltNameAr", state1.AltNameAr);
      formData.append("HemisphereEn", (HemisphereEn.value === undefined ? state1.HemisphereEn : HemisphereEn.value));
      formData.append("AbbrevAr", state1.AbbrevAr);
      formData.append("AbbrevEn", state1.AbbrevEn);

      formData.append("Offset", state1.Offset);


      await axios.put(`${window.env.API_URL}/updateNationality/${nationalityid._id}`, formData);
      history("/nationalitylist");
      swal({
        title: "Success!",
        text: "Data has been Updated Successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Nationality</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Name"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, NameEn: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.NameEn} />
                    </FloatingLabel>


                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, NameAr: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.NameAr} />
                    </FloatingLabel>

                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Short Code"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, shortCode: e.target.value })
                      }

                    >
                      <Form.Control  onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }} placeholder="Description" value={state1.shortCode} />
                    </FloatingLabel>

                  </div>

                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Alternate Name"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, AltNameEn: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.AltNameEn} />
                    </FloatingLabel>
                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, AltNameAr: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.AltNameAr} />
                    </FloatingLabel>

                  </div>

                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Abbrevation"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, AbbrevEn: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.AbbrevEn} />
                    </FloatingLabel>
                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, AbbrevAr: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.AbbrevAr} />
                    </FloatingLabel>

                  </div>

                </div>


                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{nationalityid.HemisphereEn}</div>}
                      defaultValue={HemisphereEn}
                      onChange={setHemisphereEn}
                      options={Hemisphere}
                      isClearable={true}
                      isSearchable={true}


                    />



                  </div>



                </div>


                <div className="ButtonSection">
                  <div>
                    <label className="Multipleownerlabel">
                      Select Nationality Flag image
                    </label>
                    <input
                      type="file"
                      onChange={fileSelected}
                      className="formInput fileinputdata"
                      id="file"
                    />
                    {preview && (
                      <>
                        <ImCross onClick={handlePreview} className="crossIcon" />
                        <img src={preview} className="PreviewImage" alt="" />
                      </>
                    )}
                  </div>
                  <button type="submit" className="SubmitButton">
                    Update
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditNationality;
