import React, { useState, useEffect } from "react";
import "../../Components/CSS/forms.css";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useLocation } from "react-router-dom";
import swal from "sweetalert";
import axios from "axios";
import Select from "react-select";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import { fetchTrainerList } from "../../redux/getDropDownReducer/getTrainerList";
import { fetchbreeder } from "../../redux/getReducer/getBreeder";
import { fetchnationality } from "../../redux/getReducer/getNationality";
import { fetchHorseKind } from "../../redux/getReducer/getHorseKind";
import { fetchgenderList } from "../../redux/getDropDownReducer/getGenderList";
import { AiOutlineReload } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import BreederPopup from "../PostTable/Breeder";
import ColorPopup from "../PostTable/Color";
import OwnerPopup from "../PostTable/Owner/OwnerForm";
import TrainerPopup from "../PostTable/PostTrainer";
import GenderPopup from "../PostTable/Gender";
import NationalityPopup from "../PostTable/Nationality";
import HorseKindPopup from "../PostTable/Horsekindform";
import { ImCross } from "react-icons/im";
import Moment from "react-moment";
import DatePicker from "react-date-picker";
import { fetchNationalityList } from "../../redux/getDropDownReducer/getNationalityList";
import { fetchColorDropdown } from "../../redux/getDropDownReducer/getColor";
import { fetchOwnerList } from "../../redux/getDropDownReducer/getOwnerList";
import { fetchBreederList } from "../../redux/getDropDownReducer/getBreederList";
import { fetchHorseKindData } from "../../redux/getDropDownReducer/getHorseKindData";

//---------------------------options for dropdowns---------------------------//

const Gelted = [
  {
    id: "0",
    value: "false",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>false</p>
        <p>Ø®Ø§Ø·Ø¦Ø©</p>
      </div>
    ),
  },
  {
    id: "1",
    value: "true",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>true</p>
        <p>Ø­Ù‚ÙŠÙ‚ÙŠ</p>
      </div>
    ),
  },
];

const HorseStatusAll = [
  {
    id: "0",
    value: "false",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>false</p>
        <p>Ø®Ø§Ø·Ø¦Ø©</p>
      </div>
    ),
  },
  {
    id: "1",
    value: "true",
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <p>true</p>
        <p>Ø­Ù‚ÙŠÙ‚ÙŠ</p>
      </div>
    ),
  },
];

const Foals = [
  { id: "0", value: "1", label: "1" },
  { id: "1", value: "2", label: "2" },
  { id: "2", value: "3", label: "3" },
  { id: "3", value: "4", label: "4" },
  { id: "4", value: "5", label: "5" },
  { id: "5", value: "6", label: "6" },
  { id: "6", value: "7", label: "7" },
  { id: "7", value: "8", label: "8" },
  { id: "8", value: "9", label: "9" },
  { id: "9", value: "10", label: "10" },
];

const EditHorse = () => {
  const dispatch = useDispatch();
  const history = useNavigate();
  const { state } = useLocation();

  const { data: TrainerList } = useSelector((state) => state.TrainerList);
  const { data: OwnerList } = useSelector((state) => state.OwnerList);
  const { data: BreederList } = useSelector((state) => state.BreederList);

  const { data: genderList } = useSelector((state) => state.genderList);
  const { data: NationalityList } = useSelector(
    (state) => state.NationalityList
  );
  const { data: colordropdown } = useSelector((state) => state.colordropdown);
  const { data: HorseKindData } = useSelector((state) => state.HorseKindData);

  const { data: HorseKind } = useSelector((state) => state.HorseKind);

  const { horseid } = state;

  const SearchTitle = ""
  const SearchCode = ""
  const currentPage = 1;

  const [Breeder, setBreeder] = useState("");
  const [ActiveTrainer, setActiveTrainer] = useState("");
  const [ActiveOwner, setActiveOwner] = useState("");
  const [Sex, setSex] = useState("");
  const [NationalityID, setNationalityID] = useState("");

  const [ColorID, setColor] = useState("");
  const [Rds, setRds] = useState("");
  const [isGelded, setisGelded] = useState("");

  const [HorseStatus, setHorseStatus] = useState("");
  const [KindHorse, setKindHorse] = useState("");
  const [Dam, setDam] = useState("");
  const [GSire, setGSire] = useState("");
  const [Sire, setSire] = useState("");
  const [DOB, setDOB] = useState("");
  const [Value1, setValue1] = useState("");
  const [Value2, setValue2] = useState("");
  const [Value4, setValue4] = useState("");
  const [Value3, setValue3] = useState("");

  const [Foal, setFoal] = useState("");
  //---------------------------show popup---------------------------//
  const [showBreeder, setShowBreeder] = useState(false);
  const [showColor, setShowColor] = useState(false);
  const [showGender, setShowGender] = useState(false);
  const [showActiveOwner, setShowActiveOwner] = useState(false);
  const [showActiveTrainer, setShowActiveTrainer] = useState(false);
  const [showActivenationality, setShowActivenationality] = useState(false);
  const [showHorseKind, setShowHorseKind] = useState(false);

  const handleCloseBreeder = () => setShowBreeder(false);
  const handleCloseColor = () => setShowColor(false);
  const handleCloseGender = () => setShowGender(false);
  const handleCloseActiveOwner = () => setShowActiveOwner(false);
  const handleCloseActiveTrainer = () => setShowActiveTrainer(false);
  const handleCloseActivenationality = () => setShowActivenationality(false);
  const handleCloseHorseKind = () => setShowHorseKind(false);

  const handleShowBreeder = async () => {
    await setShowBreeder(true);
  };

  const handleShowColor = async () => {
    await setShowColor(true);
  };

  const handleShowGender = async () => {
    await setShowGender(true);
  };

  const handleShowActiveOwner = async () => {
    await setShowActiveOwner(true);
  };

  const handleShowActiveTrainer = async () => {
    await setShowActiveTrainer(true);
  };

  const handleShowActivenationality = async () => {
    await setShowActivenationality(true);
  };

  const handleShowHorseKind = async () => {
    await setShowHorseKind(true);
  };
  //---------------------------fetch---------------------------//
  const FetchNew = () => {
    dispatch(fetchOwnerList());
    dispatch(fetchColorDropdown());
    dispatch(fetchbreeder());
    dispatch(fetchnationality());
    dispatch(fetchgenderList());
    dispatch(fetchHorseKind());
    dispatch(fetchNationalityList());
    dispatch(fetchHorseKind());
    dispatch(fetchBreederList({ Value3 }));
  };
  //---------------------------code verification---------------------------//
  const VerifyCode = async (event) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("shortCode", state1.shortCode);
      const response = await axios.post(
        `${window.env.API_URL}VerifyShortCode`,
        formData
      );

      const resdata =
        response.data.data === null
          ? "Yes You can use it"
          : `Already exit in ${response.data.data.NameEn}`;

      swal({
        title: "Success!",
        text: resdata,
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

 
  useEffect(() => {
    dispatch(fetchOwnerList({ Value2 }));
  }, [dispatch, Value2]);
  useEffect(() => {
    dispatch(fetchTrainerList({ Value4 }));
  }, [dispatch, Value4]);
  useEffect(() => {
    dispatch(fetchColorDropdown());
    dispatch(fetchBreederList({ Value3 }));
    dispatch(fetchNationalityList());
    dispatch(fetchgenderList());
    dispatch(fetchHorseKind({ SearchTitle, SearchCode, currentPage }));
  }, [dispatch, Value3]);
  //---------------------------options for dropdowns---------------------------//
  let traineroption =
    TrainerList === undefined ? (
      <></>
    ) : (
      TrainerList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  let horseoptions =
    HorseKindData === undefined ? (
      <></>
    ) : (
      HorseKindData.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>
                {item.NameEn}(
                {item.NationalityData ? item.NationalityData.NameEn : <></>})
              </p>
              <p>
                {item.NameAr}(
                {item.NationalityData ? item.NationalityData.NameAr : <></>})
              </p>
            </div>
          ),
        };
      })
    );

  let AllGender =
    genderList === undefined ? (
      <></>
    ) : (
      genderList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let owneroption =
    OwnerList === undefined ? (
      <></>
    ) : (
      OwnerList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let AllBreeder =
    BreederList === undefined ? (
      <></>
    ) : (
      BreederList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  let AllNationality =
    NationalityList === undefined ? (
      <></>
    ) : (
      NationalityList.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  const handleChangeInput = (value) => {
    setValue1(value);
  };

  const handleChangeInput3 = (value) => {
    setValue3(value);
  };

  const handleChangeInput4 = (value) => {
    setValue4(value);
  };

  const handleChangeInput1 = (value) => {
    setValue2(value);
  };
  let AllColor =
    colordropdown === undefined ? (
      <></>
    ) : (
      colordropdown.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  let horsekindoptions =
    HorseKind === undefined ? (
      <></>
    ) : (
      HorseKind.map(function (item) {
        return {
          id: item._id,
          value: item.NameEn,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );

  const today = new Date();

  const [state1, setState] = useState({
    NameEn: "",
    NameAr: "",
    PurchasePrice: "",
    STARS: "",
    RemarksEn: "",
    RemarksAr: "",
    ActiveOwner: "",
    ActiveTrainer: "",
    Breeder: "",
    HorseHorseImage: "",
    Height: "",
    ColorID: "",
    NationalityID: "",
    KindHorse: "",
    CreationId: "",
    Rds: "",
    HorseStatus: "",
    shortCode: "",
  });
  const [HorseImage, setHorseImage] = useState();
  const [preview, setPreview] = useState();

  const fileSelected = (event) => {
    const HorseImage = event.target.files[0];
    setHorseImage(HorseImage, HorseImage);
  };
  useEffect(() => {
    if (HorseImage === undefined) {
      setPreview(horseid.HorseImage);
      return;
    }
    const objectUrl = URL.createObjectURL(HorseImage);
    setPreview(objectUrl);
    return () => URL.revokeObjectURL(objectUrl);
  }, [HorseImage, horseid.HorseImage]);
  const handlePreview = () => {
    setHorseImage();
    setPreview();
    document.getElementById("file").value = "";
  };
  //---------------------------get Data from state---------------------------//
  useEffect(() => {
    if (horseid) {
      setState({
        NameEn: horseid.NameEn,
        NameAr: horseid.NameAr,
        Breeder: horseid.BreederData ? horseid.BreederData._id : null,
        PurchasePrice: horseid.PurchasePrice,
        RemarksEn: horseid.RemarksEn,
        RemarksAr: horseid.RemarksAr,
        HorseImage: horseid.HorseImage,
        isGelded: horseid.isGelded,
        ActiveTrainer: horseid.ActiveTrainerData ? horseid.ActiveTrainerData._id : null,
        ActiveOwner: horseid.ActiveOwnerData ? horseid.ActiveOwnerData._id : null,
        ColorID: horseid.ColorIDData ? horseid.ColorIDData._id : null,
        NationalityID: horseid.NationalityData ? horseid.NationalityData._id : null,
        KindHorse: horseid.KindHorseData ? horseid.KindHorseData._id : null,
        CreationId: horseid.NationalityData ? horseid.NationalityData._id : null,
        HorseStatus: horseid.HorseStatus,
        Rds: horseid.Rds,
        shortCode: horseid.shortCode,
        Foal: horseid.Foal,
        Dam: horseid.DamData,
        Sire: horseid.SireData,
        GSire: horseid.GSireData,

      });
    } else {
    }
  }, [horseid]);
  const KindHorseid = KindHorse === "" ? state1.KindHorse : KindHorse.id;
  useEffect(() => {
    dispatch(fetchHorseKindData({ Value1, KindHorseid }));
  }, [dispatch, Value1, KindHorse, KindHorseid]);
  //---------------------------submit---------------------------//
  const submit = async (event) => {
    event.preventDefault();
    console.log(horseid, 'adsadasd')
    try {
      const formData = new FormData();
      formData.append("Horseimage", HorseImage);
      formData.append("NameEn", state1.NameEn);
      formData.append("NameAr", state1.NameAr + " ");
      formData.append("PurchasePrice", state1.PurchasePrice);
      formData.append("RemarksEn", state1.RemarksEn);
      formData.append("RemarksAr", state1.RemarksAr);

      formData.append(
        "isGelded",
        isGelded.id === undefined ? 'false' : isGelded.id
      );
      formData.append("Rds", Rds.id === undefined ? 'false' : Rds.id);
      formData.append(
        "ActiveTrainer",
        ActiveTrainer.id === undefined ? state1.ActiveTrainer : ActiveTrainer.id
      );

      if(Dam.id !== undefined) {
        formData.append("Dam", Dam.id === undefined ? state1.Dam : Dam.id);
      }
      if(Sire.id !== undefined) {
        formData.append("Sire", Sire.id === undefined ? state1.Sire : Sire.id);
      }
      if(GSire.id !== undefined) {
        formData.append("GSire", GSire.id === undefined ? state1.GSire : GSire.id);
      }
      formData.append(
        "Breeder",
        Breeder.id === undefined ? state1.Breeder : Breeder.id
      );
      if(NationalityID.id !== undefined ){
        formData.append(
          "NationalityID",
          NationalityID.id === undefined ? state1.NationalityID : NationalityID.id
        );
      }

      formData.append(
        "ActiveOwner",
        ActiveOwner.id === undefined ? state1.ActiveOwner : ActiveOwner.id
      );
      formData.append(
        "KindHorse",
        KindHorse.id === undefined ? state1.KindHorse : KindHorse.id
      );
      formData.append(
        "HorseStatus",
        HorseStatus.id === undefined ? state1.HorseStatus : HorseStatus.id
      );
      formData.append(
        "ColorID",
        ColorID.id === undefined ? state1.ColorID : ColorID.id
      );
      // formData.append(
      //   "CreationId",
      //   NationalityID.id === undefined ? state1.NationalityID : NationalityID.id
      // );
      if(NationalityID.id !== undefined ){
        formData.append(
          "CreationId",
          NationalityID.id === undefined ? state1.NationalityID : NationalityID.id
        );
      }
      formData.append(
        "Foal",
        Foal.value === undefined ? state1.Foal : Foal.value
      );
      await axios.put(
        `${window.env.API_URL}/updatehorse/${horseid._id}`,
        formData
      );
      history("/horse");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };
  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Horse</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <p className="selectLabel">Horse Kind </p>

                  <div className="col-sm">
                    <Select
                      placeholder={horseid.KindHorseData ? (
                        horseid.KindHorseData.NameEn
                      ) : (
                        <>-</>
                      )}
                      defaultValue={KindHorse}
                      onChange={setKindHorse}
                      options={horsekindoptions}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowHorseKind}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Horse Code"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, shortCode: e.target.value })
                      }
                      name="Name"
                    >
                      <Form.Control
                        type="text"
                        placeholder=" Horse Code"
                        required
                        value={state1.shortCode}
                      />
                    </FloatingLabel>
                  </div>
                  <div className="col-sm">
                    <p onClick={VerifyCode} className="verifybtn">
                      Verify Code
                    </p>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Name"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, NameEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.NameEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Ø§Ø³Ù…"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, NameAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Ù…Ù„Ø§Ø­Ø¸Ø§Øª"
                        value={state1.NameAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <DatePicker
                      onChange={setDOB}
                      value={DOB}
                      dayPlaceholder=""
                      maxDate={today}
                      monthPlaceholder={horseid.DOB}
                      yearPlaceholder=""
                    />
                    <span className="spanForm"> |</span>
                  </div>
                  <div
                    className="col-sm"
                    style={{
                      display: "flex",
                      marginTop: "20px",
                    }}
                  >
                    <p>Horse Age :</p>
                    {DOB === "" ? (
                      <Moment durationFromNow>{horseid.DOB}</Moment>
                    ) : (
                      <Moment durationFromNow>{DOB}</Moment>
                    )}
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Foal</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{horseid.Foal}</div>}
                      defaultValue={horseid.Foal}
                      onChange={setFoal}
                      options={Foals}
                      className="basic-single"
                      classNamePrefix="select"
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Nationality</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{horseid.NationalityData ? horseid.NationalityData.NameEn : <>-</>}</div>}
                      defaultValue={NationalityID}
                      onChange={setNationalityID}
                      options={AllNationality}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowActivenationality}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Color </p>

                  <div className="col-sm">
                    <Select
                      placeholder={<div>{horseid.ColorIDData === null ? <></> : horseid.ColorIDData.NameEn}</div>}
                      defaultValue={ColorID}
                      onChange={setColor}
                      options={AllColor}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowColor}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Gender</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{horseid.SexModelData ? horseid.SexModelData.NameEn : <>-</>}</div>}
                      defaultValue={Sex}
                      onChange={setSex}
                      options={AllGender}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowGender}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Type to search Dam</div>}
                      onChange={setDam}
                      options={horseoptions}
                      isOptionDisabled={(option) => option.isdisabled}
                      isSearchable={true}
                      hideSelectedOptions={true}
                      onInputChange={handleChangeInput}
                      value={Dam}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Type to search Sire</div>}
                      defaultValue={Sire}
                      onChange={setSire}
                      options={horseoptions}
                      isClearable={true}
                      isSearchable={true}
                      onInputChange={handleChangeInput}
                      value={Sire}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <Select
                      placeholder={<div>Type to search GSire</div>}
                      defaultValue={GSire}
                      onChange={setGSire}
                      options={horseoptions}
                      isClearable={true}
                      isSearchable={true}
                      onInputChange={handleChangeInput}
                      value={GSire}
                    />
                  </div>
                </div>

                <div className="row mainrow">
                  <p className="selectLabel">Owner</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{horseid.ActiveOwnerData ? horseid.ActiveOwnerData.NameEn : <></>}</div>}
                      defaultValue={ActiveOwner}
                      onChange={setActiveOwner}
                      options={owneroption}
                      onInputChange={handleChangeInput1}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowActiveOwner}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Breeder</p>
                  <div className="col-sm">
                    <Select
                      placeholder={<div>{horseid.BreederData ? horseid.BreederData.NameEn : <>-</>}</div>}
                      defaultValue={Breeder}
                      onChange={setBreeder}
                      options={AllBreeder}
                      onInputChange={handleChangeInput3}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span className="addmore" onClick={handleShowBreeder}>
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Trainer</p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>{horseid.ActiveTrainerData ? horseid.ActiveTrainerData.NameEn : <></>}</div>

                      }
                      defaultValue={ActiveTrainer}
                      onChange={setActiveTrainer}
                      options={traineroption}
                      isClearable={true}
                      isSearchable={true}
                      onInputChange={handleChangeInput4}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowActiveTrainer}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>{" "}
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Purchased Price"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, PurchasePrice: e.target.value })
                      }
                    >
                      <Form.Control
                        onKeyPress={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        placeholder="Details"
                        value={state1.PurchasePrice}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <p className="selectLabel">Gelded </p>

                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {horseid.isGelded === false ? "false" : "true"}
                        </div>
                      }
                      defaultValue={isGelded}
                      onChange={setisGelded}
                      options={Gelted}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Rds </p>

                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>{horseid.Rds === false ? "false" : "true"}</div>
                      }
                      defaultValue={Rds}
                      onChange={setRds}
                      options={Gelted}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Horse Status </p>

                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {horseid.HorseStatus === false ? "false" : "true"}
                        </div>
                      }
                      defaultValue={HorseStatus}
                      onChange={setHorseStatus}
                      options={HorseStatusAll}
                      isClearable={true}
                      isSearchable={true}
                    />
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Remarks"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, RemarksEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Details"
                        value={state1.RemarksEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Ù…Ù„Ø§Ø­Ø¸Ø§Øª"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, RemarksAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Ù…Ù„Ø§Ø­Ø¸Ø§Øª"
                        value={state1.RemarksAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="ButtonSection">
                  <div>
                    <label className="Multipleownerlabel">
                      Select Horse image
                    </label>
                    <input
                      type="file"
                      onChange={fileSelected}
                      className="formInput"
                      id="file"
                    />
                    {preview && (
                      <>
                        <ImCross
                          onClick={handlePreview}
                          className="crossIcon"
                        />
                        <img src={preview} className="PreviewImage" alt="" />
                      </>
                    )}
                  </div>
                  <button type="submit" className="SubmitButton">
                    Update
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={showBreeder}
        onHide={handleCloseBreeder}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2> Breeder</h2>
        </Modal.Header>
        <Modal.Body>
          <BreederPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showColor}
        onHide={handleCloseColor}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Color</h2>
        </Modal.Header>
        <Modal.Body>
          <ColorPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showActiveOwner}
        onHide={handleCloseActiveOwner}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Owner</h2>
        </Modal.Header>
        <Modal.Body>
          <OwnerPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showActiveTrainer}
        onHide={handleCloseActiveTrainer}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Trainer</h2>
        </Modal.Header>
        <Modal.Body>
          <TrainerPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showGender}
        onHide={handleCloseGender}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Gender</h2>
        </Modal.Header>
        <Modal.Body>
          <GenderPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showActivenationality}
        onHide={handleCloseActivenationality}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Nationality</h2>
        </Modal.Header>
        <Modal.Body>
          <NationalityPopup />
        </Modal.Body>
      </Modal>
      <Modal
        show={showHorseKind}
        onHide={handleCloseHorseKind}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Horse Kind</h2>
        </Modal.Header>
        <Modal.Body>
          <HorseKindPopup />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default EditHorse;