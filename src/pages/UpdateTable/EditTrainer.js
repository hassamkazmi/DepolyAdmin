import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { ImCross } from "react-icons/im";
import { useNavigate, useLocation } from "react-router-dom";
import { fetchNationalityList } from "../../redux/getDropDownReducer/getNationalityList";
import { useSelector } from "react-redux";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import { AiOutlineReload } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import "../../Components/CSS/forms.css";
import swal from "sweetalert";
import axios from "axios";
import DatePicker from "react-date-picker";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import Select from "react-select";

import NationalityPopup from "../PostTable/Nationality";
const EditTrainer = () => {
  const history = useNavigate();
  const dispatch = useDispatch();
  const { state } = useLocation();

  const { data: NationalityList } = useSelector(
    (state) => state.NationalityList
  );
  const [TrainerLicenseDate, setTrainerLicenseDate] = useState("");
  const [NationalityID, setNationalityID] = useState("");
  const [showActivenationality, setShowActivenationality] = useState(false);
  const handleCloseActivenationality = () => setShowActivenationality(false);

  const handleShowActivenationality = async () => {
    await setShowActivenationality(true);
  };
  const FetchNew = () => {
    dispatch(fetchNationalityList());
  };

  const [DOB, setDOB] = useState("");
  var today = new Date();

  const { trainerid } = state;

  const [state1, setState] = useState({
    NameEn: "",
    NameAr: "",
    TitleEn: "",
    TitleAr: "",
    ShortNameEn: "",
    ShortNameAr: "",
    RemarksEn: "",
    RemarksAr: "",
    DetailEn: "",
    DetailAr: "",
    TrainerLicenseDate: "",
    DOB: "",
    NationalityID: "",
  });

  const [preview, setPreview] = useState();

  const [image, setImage] = useState();

  const fileSelected = (event) => {
    const image = event.target.files[0];
    setImage(image);
  };
  useEffect(() => {
    dispatch(fetchNationalityList());
  }, [dispatch]);
  //---------------------------get value for dropdown---------------------------//
  let AllNationality =
    NationalityList === undefined ? (
      <></>
    ) : (
      NationalityList.map(function (item) {
        return {
          id: item._id,
          value: item._id,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p>{item.NameEn}</p>
              <p>{item.NameAr}</p>
            </div>
          ),
        };
      })
    );
  //---------------------------remove preview---------------------------//
  const handlePreview = () => {
    setImage();
    setPreview();
    document.getElementById("file").value = "";
  };
  //---------------------------get data from previous state---------------------------//

  useEffect(() => {
    if (trainerid) {
      setState({
        NameEn: trainerid.NameEn,
        NameAr: trainerid.NameAr,
        TitleEn: trainerid.TitleEn,
        TitleAr: trainerid.TitleAr,
        ShortNameEn: trainerid.ShortNameEn,
        ShortNameAr: trainerid.ShortNameAr,
        RemarksEn: trainerid.RemarksEn,
        RemarksAr: trainerid.RemarksAr,
        DetailEn: trainerid.DetailEn,
        DetailAr: trainerid.DetailAr,
        TrainerLicenseDate: trainerid.TrainerLicenseDate,
        NationalityID: trainerid.NationalityID,
        DOB: trainerid.DOB,
      });
    } else {
    }
  }, [trainerid]);




  //---------------------------image preview---------------------------//
  useEffect(() => {
    if (image === undefined) {
      setPreview(trainerid.image);
      return;
    }
    const objectUrl = URL.createObjectURL(image);
    setPreview(objectUrl);
    return () => URL.revokeObjectURL(objectUrl);
  }, [image, trainerid.image]);
  //---------------------------submit---------------------------//
  const submit = async (event) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("image", image);
      formData.append("NameEn", state1.NameEn);
      formData.append("NameAr", state1.NameAr);
      formData.append("TitleEn", state1.TitleEn);
      formData.append("TitleAr", state1.TitleAr);
      formData.append("RemarksEn", state1.RemarksEn);
      formData.append("RemarksAr", state1.RemarksAr);
      formData.append("ShortNameEn", state1.ShortNameEn);
      formData.append("ShortNameAr", state1.ShortNameAr);
      formData.append("DetailEn", state1.DetailEn);
      formData.append("DetailAr", state1.DetailAr);
      formData.append("TrainerLicenseDate", TrainerLicenseDate);
      formData.append(
        "NationalityID",
        NationalityID.id === undefined ? state1.NationalityID : NationalityID.id
      );

      formData.append("DOB", DOB);

      await axios.put(
        `${window.env.API_URL}/updatetrainer/${trainerid._id}`,
        formData
      );
      history("/trainer");
      swal({
        title: "Success!",
        text: "Data has been Updated successfully ",
        icon: "success",
        button: "OK",
      });
    } catch (error) {
      const err = error.response.data.message;
      swal({
        title: "Error!",
        text: err,
        icon: "error",
        button: "OK",
      });
    }
  };

  return (
    <>
      <div className="page">
        <div className="rightsidedata">
          <div
            style={{
              marginTop: "30px",
            }}
          >
            <div className="Headers">Edit Trainer</div>
            <div className="form">
              <form onSubmit={submit}>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Name"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, NameEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.NameEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, NameAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.NameAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Short Name"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, ShortNameEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.ShortNameEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="اسم قصير"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, ShortNameAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.ShortNameAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Title"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, TitleEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.TitleEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="لقب"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, TitleAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.TitleAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>
                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Remarks"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, RemarksEn: e.target.value })
                      }

                    >
                      <Form.Control type="text" placeholder="Description" value={state1.RemarksEn} />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="ملاحظات"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, RemarksAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.RemarksAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Detail"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, DetailEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.DetailEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="التفاصيل"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, DetailAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.NameAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="Remarks"
                      className="mb-3"
                      onChange={(e) =>
                        setState({ ...state1, RemarksEn: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.RemarksEn}
                      />
                    </FloatingLabel>

                    <span className="spanForm"> |</span>
                  </div>

                  <div className="col-sm">
                    <FloatingLabel
                      controlId="floatingInput"
                      label="ملاحظات"
                      className="mb-3 floatingInputAr"
                      style={{ direction: "rtl" }}
                      onChange={(e) =>
                        setState({ ...state1, RemarksAr: e.target.value })
                      }
                    >
                      <Form.Control
                        type="text"
                        placeholder="Description"
                        value={state1.RemarksAr}
                      />
                    </FloatingLabel>
                  </div>
                </div>

                <div className="row mainrow">
                  <p className="selectLabel">Trainer license Date</p>
                  <DatePicker
                    onChange={setTrainerLicenseDate}
                    value={TrainerLicenseDate}
                    dayPlaceholder=""
                    maxDate={today}
                    monthPlaceholder={state1.TrainerLicenseDate.split("T")[0]}
                    yearPlaceholder=""
                    className="editDate"
                  />

                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Nationality</p>
                  <div className="col-sm">
                    <Select
                      placeholder={
                        <div>
                          {trainerid.TrainerNationalityData &&
                            trainerid.TrainerNationalityData.NameEn}
                        </div>
                      }
                      defaultValue={NationalityID}
                      onChange={setNationalityID}
                      options={AllNationality}
                      isClearable={true}
                      isSearchable={true}
                    />
                    <span className="spanForm">
                      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}>Add more</Tooltip>}
                      >
                        <span
                          className="addmore"
                          onClick={handleShowActivenationality}
                        >
                          +
                        </span>
                      </OverlayTrigger>
                      <OverlayTrigger
                        overlay={
                          <Tooltip id={`tooltip-top`}>Fetch New</Tooltip>
                        }
                      >
                        <span className="addmore" onClick={FetchNew}>
                          <AiOutlineReload />
                        </span>
                      </OverlayTrigger>
                    </span>
                  </div>
                </div>
                <div className="row mainrow">
                  <p className="selectLabel">Date of Birth</p>
                  <DatePicker
                    onChange={setDOB}
                    value={DOB}
                    dayPlaceholder=""
                    maxDate={today}
                    monthPlaceholder={state1.DOB}
                    yearPlaceholder=""
                    className="editDate"
                  />


                </div>

                <div className="ButtonSection">
                  <div>
                    <label className="Multipleownerlabel">
                      Select Trainer image
                    </label>
                    <input
                      type="file"
                      onChange={fileSelected}
                      className="formInput"
                      id="file"
                    />
                    {preview && (
                      <>
                        <ImCross
                          onClick={handlePreview}
                          className="crossIcon"
                        />
                        <img src={preview} className="PreviewImage" alt="" />
                      </>
                    )}
                  </div>
                  <div>
                    <button type="submit" className="SubmitButton">
                      Update Trainer
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={showActivenationality}
        onHide={handleCloseActivenationality}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h2>Nationality</h2>
        </Modal.Header>
        <Modal.Body>
          <NationalityPopup />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default EditTrainer;
