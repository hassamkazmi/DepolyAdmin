import React from "react";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";

const RaceDetailPopup = (data) => {


  const type = data.data.RaceTypeModelData ? data.data.RaceTypeModelData : "-";

  const racecourse = data.data.RaceCourseData ? data.data.RaceCourseData : "-";
  const kind = data.data.RaceKindData ? data.data.RaceKindData : "-";
  const length = data.data.TrackLengthData ? data.data.TrackLengthData : "-";
  const name = data.data.RaceNameModelData ? data.data.RaceNameModelData : "-";
  const meet = data.data.MeetingTypeData ? data.data.MeetingTypeData : "-";
  const sponsor = data.data.SponsorData ? data.data.SponsorData : "-";
  return (
    <div className="form">
      <div className="modalPreview">
        {/* <img src={data.data.image} className="PreviewImage" alt="" />
         */}
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Race Name"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Title"
              value={name.NameEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="اسم"
            style={{ direction: "rtl", left: "initial", right: 0 }}
            className="mb-3 floatingInputAr"
          >
            <Form.Control
              type="text"
              placeholder="عنوان"
              value={name.NameAr}
              readOnly
              style={{ left: "%" }}
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Description"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Description"
              value={data.data.DescriptionEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="وصف"
            className="mb-3 floatingInputAr"
            style={{ direction: "rtl", left: "initial", right: 0 }}
          >
            <Form.Control
              type="text"
              placeholder="وصف"
              value={data.data.DescriptionAr}
              style={{ left: "%" }}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Race Type "
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={type.NameEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="نوع السباق"
            className="mb-3 floatingInputAr"
            style={{ direction: "rtl", left: "initial", right: 0 }}
          >
            <Form.Control
              type="text"
              placeholder="Description"
              value={type.NameAr}
              style={{ left: "%" }}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Race Course "
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={racecourse.TrackNameEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="نوع السباق"
            className="mb-3 floatingInputAr"
            style={{ direction: "rtl", left: "initial", right: 0 }}
          >
            <Form.Control
              type="text"
              placeholder="Description"
              value={racecourse.TrackNameAr}
              style={{ left: "%" }}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Meeting Type "
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={meet.NameEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="نوع السباق"
            className="mb-3 floatingInputAr"
            style={{ direction: "rtl", left: "initial", right: 0 }}
          >
            <Form.Control
              type="text"
              placeholder="Description"
              value={meet.NameAr}
              style={{ left: "%" }}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Sponsor"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={sponsor.TitleEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="نوع السباق"
            className="mb-3 floatingInputAr"
            style={{ direction: "rtl", left: "initial", right: 0 }}
          >
            <Form.Control
              type="text"
              placeholder="Description"
              value={sponsor.TitleAr}
              style={{ left: "%" }}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Race Kind "
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={kind.NameEn}
              readOnly
            />
          </FloatingLabel>

          <span className="spanForm"> |</span>
        </div>

        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="نوع السباق"
            className="mb-3 floatingInputAr"
            style={{ direction: "rtl", left: "initial", right: 0 }}
          >
            <Form.Control
              type="text"
              placeholder="Description"
              value={kind.NameAr}
              style={{ left: "%" }}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Track Length "
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={length.TrackLength}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>
      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Number Of Horse"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={data.data.RacehorsesData.length}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>
      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel controlId="floatingInput" label="Day" className="mb-3">
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={data.data.Day}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Start Time"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={data.data.StartTime}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>


      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Result Status"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={data.data.ResultStatus}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>

      <div className="row mainrow">
        <div className="col-sm">
          <FloatingLabel
            controlId="floatingInput"
            label="Race Status"
            className="mb-3"
          >
            <Form.Control
              type="text"
              placeholder="Second-Title"
              value={data.data.RaceStatus}
              readOnly
            />
          </FloatingLabel>
        </div>
      </div>
    </div>
  );
};

export default RaceDetailPopup;
