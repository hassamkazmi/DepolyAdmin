import React, { useEffect, Fragment, useState } from "react";
import ReactQuill, { Quill } from "react-quill";
import ImageResize from 'quill-image-resize-module-react';
import { useDispatch, useSelector } from "react-redux";
import { fetchEmailImage, STATUSES } from "../../redux/getReducer/getEmailImagesList";

Quill.register('modules/imageResize', ImageResize);
const modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }, { font: [] }],
    [{ size: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [
      { list: 'ordered' },
      { list: 'bullet' },
      { indent: '-1' },
      { indent: '+1' }
    ],
    ['link', 'image', 'video'],
    ['clean']
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false
  },
  imageResize: {
    parchment: Quill.import('parchment'),
    modules: ['Resize', 'DisplaySize']
  }
};

const formats = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
  'video'
];
const Email = (props) => {


  const dispatch = useDispatch();
  const { data: EmailImage, status } = useSelector((state) => state.EmailImage);

  const [QuillData, setQuillData] = useState('')
  
  useEffect(() => {
    dispatch(fetchEmailImage());
  }, [dispatch]);


  return (
    <div className='parentTemplate'>
      <div className="Headers">{props.data}</div>
      <div className='emailimages'>
        {
          EmailImage.map((item) => {
            return(
              <div className="inneremailimages">
              <img src={item.image} alt="" />
              <p>{item.Title}</p>
              </div>
            )
          })
        }
      </div>
      <div className='templateInputs'>
        <input value={props.data} type="text" />
       
      </div>
      <div className="row">
        <div className="col-sm">
          <ReactQuill
            theme="snow"
            modules={modules}
            formats={formats}
            onChange={setQuillData}
          />

        </div>

      </div>
      <div className="ButtonSection " style={{ justifyContent: "end", marginRight: "-11px" }}>
        <button Name="submit" className="SubmitButton" >
          Submit
        </button>
      </div>
    </div>
  )
}

export default Email