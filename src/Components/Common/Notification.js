import React from 'react'
import {BsBell} from 'react-icons/bs'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';


const Notification = () => {
  return (
    <div>
      <OverlayTrigger
                        overlay={<Tooltip id={`tooltip-top`}> Notification</Tooltip>}
                      >
                        <span
                          className="addmore"
                        >
                          <BsBell style={{
                            width: '25px',
                            height: '25px',
                            color:'#000'
                          }}/>
                        </span>
                  </OverlayTrigger>   

    </div>
  )
}

export default Notification
