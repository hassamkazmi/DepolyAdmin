import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getGenderListSlice = createSlice({
    name: 'genderList',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchgenderList.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchgenderList.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchgenderList.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setgenderList, setStatus } = getGenderListSlice.actions;
export default getGenderListSlice.reducer;

export const fetchgenderList = createAsyncThunk('/SexgetList/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/Sexget?size=200`);
    const genderListData = res.data;
    return genderListData.data;
})