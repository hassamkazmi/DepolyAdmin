import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getTrackConditionDropdownSlice = createSlice({
    name: 'TrackConditiondropdown',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchTrackConditionDropdown.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchTrackConditionDropdown.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchTrackConditionDropdown.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setColor, setStatus } = getTrackConditionDropdownSlice.actions;
export default getTrackConditionDropdownSlice.reducer;

export const fetchTrackConditionDropdown = createAsyncThunk('/TrackConditionDropdown/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/TrackConditionget?size=100`);
    const ColorData = res.data;
    return ColorData.data;
})