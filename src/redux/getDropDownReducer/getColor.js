import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getColorSlice = createSlice({
    name: 'colordropdown',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchColorDropdown.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchColorDropdown.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchColorDropdown.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setColor, setStatus } = getColorSlice.actions;
export default getColorSlice.reducer;

export const fetchColorDropdown = createAsyncThunk('/Color/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/Colorget?size=800`);
    const ColorData = res.data;
    return ColorData.data;
})