import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getpointTableListSlice = createSlice({
    name: 'pointTableList',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchpointTableList.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchpointTableList.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchpointTableList.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setpointTableList, setStatus } = getpointTableListSlice.actions;
export default getpointTableListSlice.reducer;

export const fetchpointTableList = createAsyncThunk('/pointTableListget/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/PointTableSystemget`);
    const pointTableListData = res.data;
    return pointTableListData.data;
})