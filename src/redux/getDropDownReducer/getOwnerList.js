import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getOwnerListSlice = createSlice({
    name: 'OwnerList',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchOwnerList.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchOwnerList.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchOwnerList.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setOwnerList, setStatus } = getOwnerListSlice.actions;
export default getOwnerListSlice.reducer;

export const fetchOwnerList = createAsyncThunk('/OwnerListget/fetch', async ({ Value2 }) => {
    const res = await axios.get(`${window.env.API_URL}/OwnerDropDown?NameEn=${Value2 === undefined ? '' : Value2}&limit=50`);
    const OwnerListData = res.data;
    return OwnerListData.data;
})
