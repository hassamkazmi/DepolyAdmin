import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getGroundListSlice = createSlice({
    name: 'groundtypelist',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchgroundtypelist.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchgroundtypelist.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchgroundtypelist.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setgroundtype, setStatus } = getGroundListSlice.actions;
export default getGroundListSlice.reducer;

export const fetchgroundtypelist = createAsyncThunk('/GroundTypeget/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/GroundTypeget?size=2000`);
    const groundtypelist = res.data;
    return groundtypelist.data;
})