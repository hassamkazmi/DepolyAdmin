import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getFinalPositionDropDownSlice = createSlice({
    name: 'finalpositiondropdown',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchfinalpositiondropdown.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchfinalpositiondropdown.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchfinalpositiondropdown.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setfinaleposition, setStatus } = getFinalPositionDropDownSlice.actions;
export default getFinalPositionDropDownSlice.reducer;

export const fetchfinalpositiondropdown = createAsyncThunk('/FinalPositionget/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/FinalPositionget?size=1000`);
    const colorData = res.data;
    return colorData.data;
})