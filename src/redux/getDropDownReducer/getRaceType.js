import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getRaceKindSlice = createSlice({
    name: 'racetypedropdown',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchRaceTypeDropdown.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchRaceTypeDropdown.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchRaceTypeDropdown.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setRaceType, setStatus } = getRaceKindSlice.actions;
export default getRaceKindSlice.reducer;

export const fetchRaceTypeDropdown = createAsyncThunk('/Raceget/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/RaceTypeget?size=1000`);
    const RaceKindData = res.data;
    return RaceKindData.data;
})