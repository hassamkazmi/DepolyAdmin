import axios from "axios";
import { createAsyncThunk,createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE : 'idle',
    ERROR:'error',
    LOADING: 'loading',
});

const getEmailImageSlice = createSlice({
    name: 'EmailImage',
    initialState: {
        data:[],
        status : STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
        .addCase(fetchEmailImage.pending, (state, action) => {
            state.status = STATUSES.LOADING;
        })
        .addCase(fetchEmailImage.fulfilled, (state, action) => {
            state.data = action.payload;
            state.status = STATUSES.IDLE
        }) 
        .addCase(fetchEmailImage.rejected , (state,action) => {
            state.status = STATUSES.ERROR;
        })
    }
});

export const {setEmailImage , setStatus} = getEmailImageSlice.actions;
export default getEmailImageSlice.reducer;

export const fetchEmailImage = createAsyncThunk('/EmailImageget/fetch', async() => {
    const res = await axios.get(`${window.env.API_URL}/ImagesStorageget`);
    const EmailImageData = res.data;
    return EmailImageData.data;
})