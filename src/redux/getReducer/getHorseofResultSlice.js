import axios from "axios";
import { createAsyncThunk,createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE : 'idle',
    ERROR:'error',
    LOADING: 'loading',
});

const getHorseofResultSlice = createSlice({
    name: 'HorseofResult',
    initialState: {
        data:[],
        status : STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
        .addCase(fetchHorseofResult.pending, (state, action) => {
            state.status = STATUSES.LOADING;
        })
        .addCase(fetchHorseofResult.fulfilled, (state, action) => {
            state.data = action.payload;
            state.status = STATUSES.IDLE
        }) 
        .addCase(fetchHorseofResult.rejected , (state,action) => {
            state.status = STATUSES.ERROR;
        })
    }
});

export const {setHorseofResult , setStatus} = getHorseofResultSlice.actions;
export default getHorseofResultSlice.reducer;

export const fetchHorseofResult = createAsyncThunk('/HorseofResultget/fetch', async({RaceId}) => {
    const res = await axios.get(`${window.env.API_URL}/getracehorses/${RaceId}`);
    const HorseofResultData = res.data;
    return HorseofResultData.data;
})