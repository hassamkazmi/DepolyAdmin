import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getEditVerdictHorseSlice = createSlice({
    name: 'EditVerdictHorse',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchEditVerdictHorse.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchEditVerdictHorse.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchEditVerdictHorse.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setEditVerdictHorse, setStatus } = getEditVerdictHorseSlice.actions;
export default getEditVerdictHorseSlice.reducer;

export const fetchEditVerdictHorse = createAsyncThunk('/GetEditRaceVerdict/fetch', async ({ RaceId }) => {
    const res = await axios.get(`${window.env.API_URL}/GetEditRaceVerdict/${RaceId}`);
    const EditVerdictHorseData = res.data;
    return EditVerdictHorseData.data;
})