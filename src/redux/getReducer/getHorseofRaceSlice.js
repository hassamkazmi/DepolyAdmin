import axios from "axios";
import { createAsyncThunk,createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE : 'idle',
    ERROR:'error',
    LOADING: 'loading',
});

const getHorseofRaceSlice = createSlice({
    name: 'HorseofRace',
    initialState: {
        data:[],
        status : STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
        .addCase(fetchHorseofRace.pending, (state, action) => {
            state.status = STATUSES.LOADING;
        })
        .addCase(fetchHorseofRace.fulfilled, (state, action) => {
            state.data = action.payload;
            state.status = STATUSES.IDLE
        }) 
        .addCase(fetchHorseofRace.rejected , (state,action) => {
            state.status = STATUSES.ERROR;
        })
    }
});

export const {setHorseofRace , setStatus} = getHorseofRaceSlice.actions;
export default getHorseofRaceSlice.reducer;

export const fetchHorseofRace = createAsyncThunk('/HorseofRaceget/fetch', async({SearchTitle,SearchCode,SearchUrl}) => {
    const res = await axios.get(`${window.env.API_URL}/GetHorsesofraces`);
    const HorseofRaceData = res.data;
    return HorseofRaceData.data;
})