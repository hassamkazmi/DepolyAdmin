import axios from "axios";
import { createAsyncThunk,createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE : 'idle',
    ERROR:'error',
    LOADING: 'loading',
});

const getcompetitionracesSlice = createSlice({
    name: 'competitionraces',
    initialState: {
        data:[],
        status : STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
        .addCase(fetchcompetitionraces.pending, (state, action) => {
            state.status = STATUSES.LOADING;
        })
        .addCase(fetchcompetitionraces.fulfilled, (state, action) => {
            state.data = action.payload;
            state.status = STATUSES.IDLE
        }) 
        .addCase(fetchcompetitionraces.rejected , (state,action) => {
            state.status = STATUSES.ERROR;
        })
    }
});

export const {setcompetitionraces , setStatus} = getcompetitionracesSlice.actions;
export default getcompetitionracesSlice.reducer;

export const fetchcompetitionraces = createAsyncThunk('/competitionracesget/fetch', async() => {
    const res = await axios.get(`${window.env.API_URL}/SearchRace`);
    const competitionracesData = res.data;
    return competitionracesData.data;
})