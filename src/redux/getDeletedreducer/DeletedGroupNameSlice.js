import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

const getDeletedGroupNameSlice = createSlice({
    name: 'deletedgroupname',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchdeletedgroupname.pending, (state, action) => {
                state.status = STATUSES.LOADING;
            })
            .addCase(fetchdeletedgroupname.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = STATUSES.IDLE
            })
            .addCase(fetchdeletedgroupname.rejected, (state, action) => {
                state.status = STATUSES.ERROR;
            })
    }
});

export const { setgroupname, setStatus } = getDeletedGroupNameSlice.actions;
export default getDeletedGroupNameSlice.reducer;

export const fetchdeletedgroupname = createAsyncThunk('/PointGroupNamegetdeleted/fetch', async () => {
    const res = await axios.get(`${window.env.API_URL}/PointGroupNamegetdeleted?keyword=&page=`);
    const groupData = res.data;
    return groupData.data;
})


