import React from 'react';
import ReactDOM from 'react-dom/client';
import "../src/Components/CSS/forms.css"
import App from './App';
import 'react-toastify/dist/ReactToastify.css';
import reportWebVitals from './reportWebVitals';

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

// Look ma, no error!

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals();
